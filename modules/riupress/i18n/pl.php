<?php defined('SYSPATH') or die('No direct script access.');

return array(

	// tytuł aplikacji 
	'app_title'					=> 'Riupress',
	'app_subtitle'					=> 'platforma publikacyjna',
	'app_keywords'				=> 'Riupress, programowanie, Riu, Kohana, programista Kohany',
	'app_description'				=> 'Riupress - platforma publikacyjna.',

	// tytuły kontekstów/ról (directory)
	'role_directory_default'			=> 'Kontekst domyślny',
	'role_directory_api'			=> 'API',
	'role_directory_editor'			=> 'Redaktor',
	'role_directory_root'			=> 'Administrator',
	'role_directory_moderator'	=> 'Moderator',
	'role_directory_dev'			=> 'Programista',

	// nazwy kontekstów/ról (directory)
	'role_name_default'			=> 'platforma publikacyjna',
	'role_name_api'				=> 'API',
	'role_name_editor'			=> 'panel redaktora',
	'role_name_root'				=> 'panel administratora',
	'role_name_moderator'		=> 'panel moderatora',
	'role_name_dev'				=> 'panel programisty',

	// profil
	'default_profil_crumb'			=> 'Profil użytkownika',
	'default_profil_title'			=> 'Moje konto',
	'default_profil_login_crumb'		=> 'Logowanie do konta',
	'default_profil_login_title'		=> 'Logowanie do konta',
	'default_profil_register_crumb'	=> 'Rejestracja konta',
	'default_profil_register_title'		=> 'Załóż nowe konto',
	'default_profil_recover_crumb'	=> 'Odzyskaj dostęp do konta',
	'default_profil_recover_title'		=> 'Odzyskaj dostęp do konta',
	'default_profil_index_crumb'	=> 'Mój profil',
	'default_profil_pass_crumb'		=> 'Zmiana hasła',
	'default_profil_avatar_crumb'	=> 'Zmiana avatara',
	'default_profil_index_title'		=> 'Mój profil',
	'default_profil_pass_title'		=> 'Zmiana hasła',
	'default_profil_avatar_title'		=> 'Zmiana avatara',

	// blog
	'default_blog_crumb'			=> 'Blog',
	'default_blog_title'			=> 'Blog',
	'default_blog_search_crumb'	=> 'Szukanie wpisu',
	'default_blog_search_title'		=> 'Szukanie wpisu',

	'default_doc_crumb'			=> 'Dokumentacja',
	'default_doc_title'				=> 'Dokumentacja',

	// walidacja
	'valid_useremail_check'			=> 'Z tego maila korzysta już inny użytkownik.',
	'valid_email_check'			=> 'Z tego maila korzysta już inny użytkownik.',
	'valid_login_check'				=> 'Login <strong>:value</strong> jest zajęty.',
	'valid_not_empty'				=> 'Pole nie może być puste.',
	'valid_not_size'				=> 'Plik jest za duży!',
	'valid_not_url'					=> 'Wpisz poprawny adres url zaczynający się od http://.',
	'valid_not_type_jpg'			=> 'Plik musi być w formacie jpg.',
	'valid_min_length'				=> 'Twój <strong>:field</strong> jest za krótki.',
	'valid_max_length'				=> 'Twój <strong>:field</strong> jest za długi.',
	'valid_alpha'					=> 'Pole <strong>:field</strong> może składać się tylko z liter.',
	'valid_alpha_dash'				=> 'Pole <strong>:field</strong> nie może zawierać polskich znaków.',

	// panel edytora

	'editor_index_crumb'			=> 'Panel redaktorski',
	'editor_index_title'				=> 'Panel redaktora',

	// editor / blog
	'editor_blog_crumb'			=> 'Blog',
	'editor_blog_title'				=> 'Blog',

	'editor_blog_add_crumb'		=> 'Dodaj nowy wpis',
	'editor_blog_add_title'			=> 'Dodawanie nowego wpisu',
	'editor_blog_search_crumb'	=> 'Szukanie wpisu',
	'editor_blog_search_title'		=> 'Szukanie wpisu',
	'editor_blog_edit_crumb'		=> 'Edycja wpisu',
	'editor_blog_edit_title'			=> 'Edycja wpisu',
	'editor_blog_delete_crumb'		=> 'Usuwanie wpisu',
	'editor_blog_delete_title'		=> 'Usuwanie wpisu',
	'editor_blog_deletefile_crumb'		=> 'Usuwanie załącznika wpisu',
	'editor_blog_deletefile_title'		=> 'Usuwanie załącznika wpisu',

	// editor / tag
	'editor_tag_crumb'				=> 'Tagi',
	'editor_tag_title'				=> 'Tagi',

	'editor_tag_add_crumb'			=> 'Dodaj nowy tag',
	'editor_tag_add_title'			=> 'Dodawanie nowego tagu',
	'editor_tag_search_crumb'		=> 'Szukanie tagu',
	'editor_tag_search_title'			=> 'Szukanie tagu',
	'editor_tag_edit_crumb'			=> 'Edycja tagu',
	'editor_tag_edit_title'			=> 'Edycja tagu',
	'editor_tag_delete_crumb'		=> 'Usuwanie tagu',
	'editor_tag_delete_title'			=> 'Usuwanie tagu',

	// editor / tag
	'editor_icon_crumb'			=> 'Ikonki',
	'editor_icon_title'				=> 'Ikonki',

	'editor_icon_add_crumb'		=> 'Dodaj nową ikonkę',
	'editor_icon_add_title'			=> 'Dodawanie nowej ikonki',
	'editor_icon_search_crumb'		=> 'Szukanie ikony',
	'editor_icon_search_title'		=> 'Szukanie ikony',
	'editor_icon_edit_crumb'		=> 'Edycja ikony',
	'editor_icon_edit_title'			=> 'Edycja ikony',
	'editor_icon_delete_crumb'		=> 'Usuwanie ikony',
	'editor_icon_delete_title'		=> 'Usuwanie ikony',

	// panel administratora

	'root_index_crumb'			=> 'Panel administracyjny',
	'root_index_title'				=> 'Panel administratora',

	'root_profil_crumb'			=> 'Profile użytkowników',
	'root_profil_title'				=> 'Profile użytkowników',

	'root_profil_search_crumb'		=> 'Szukanie profilu na podstawie nazwy',
	'root_profil_search_title'		=> 'Szukanie profilu na podstawie nazwy',
	'root_profil_email_crumb'		=> 'Szukanie profilu na podstawie adresu email',
	'root_profil_email_title'			=> 'Szukanie profilu na podstawie adresu email',
	'root_profil_edit_crumb'		=> 'Edycja profilu',
	'root_profil_edit_title'			=> 'Edycja profilu',
	'root_profil_delete_crumb'		=> 'Usuwanie profilu',
	'root_profil_delete_title'			=> 'Usuwanie profilu',

	'root_category_crumb'		=> 'Kategorie',
	'root_category_title'			=> 'Kategorie',

	'root_category_edit_crumb'	=> 'Edycja kategorii',
	'root_category_edit_title'		=> 'Edycja kategorii',
	'root_category_add_crumb'	=> 'Dodanie kategorii',
	'root_category_add_title'		=> 'Dodanie kategorii',
	'root_category_delete_crumb'	=> 'Usuwanie kategorii',
	'root_category_delete_title'	=> 'Usuwanie kategorii',

	'root_settings_crumb'			=> 'Ustawienia',
	'root_settings_title'			=> 'Ustawienia',


	'moderator_comments_crumb'			=> 'Komentarze',
	'moderator_comments_title'				=> 'Komentarze',
	'moderator_comments_edit_crumb'		=> 'Edycja komentarza',
	'moderator_comments_edit_title'			=> 'Edycja komentarza',
	'moderator_comments_delete_crumb'	=> 'Usuwanie komentarza',
	'moderator_comments_delete_title'		=> 'Usuwanie komentarza',


	'root_poll_crumb'			=> 'Ankiety',
	'root_poll_title'			=> 'Ankiety',

	'root_poll_add_crumb'		=> 'Dodaj nową ankietę',
	'root_poll_add_title'			=> 'Dodawanie nowej ankiety',
	'root_poll_search_crumb'	=> 'Szukanie ankiety',
	'root_poll_search_title'		=> 'Szukanie ankiety',
	'root_poll_edit_crumb'		=> 'Edycja ankiety',
	'root_poll_edit_title'			=> 'Edycja ankiety',
	'root_poll_delete_crumb'		=> 'Usuwanie ankiety',
	'root_poll_delete_title'		=> 'Usuwanie ankiety',

	'dev_doc_crumb'			=> 'Dokumentacja',
	'dev_doc_title'			=> 'Dokumentacja',

	'dev_doc_edit_crumb'		=> 'Edycja dokumentacji',
	'dev_doc_edit_title'		=> 'Edycja dokumentacji',
	'dev_doc_add_crumb'		=> 'Dodanie dokumentacji',
	'dev_doc_add_title'		=> 'Dodanie dokumentacji',
	'dev_doc_delete_crumb'	=> 'Usuwanie dokumentacji',
	'dev_doc_delete_title'		=> 'Usuwanie dokumentacji',
	'dev_doc_deletefile_crumb'	=> 'Usuwanie zalącznika dokumentacji',
	'dev_doc_deletefile_title'		=> 'Usuwanie zalącznika dokumentacji',
);

