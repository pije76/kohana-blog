<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(

	'default' => array(
		array('slug'=>'about', 'title'=>'O projekcie', 'class'=>'icon-info-sign'),
		array('slug'=>'blog', 'title'=>'Blog', 'class'=>'icon-bullhorn'),
		array('slug'=>'doc','title'=>'Dokumentacja','class'=>'icon-book'),
		array('slug'=>'profil','title'=>'Profil','class'=>'icon-user')
		),

	'editor' => array(
		array('slug'=>'blog', 'title'=>'Blog', 'class'=>'icon-bullhorn'),
		array('slug'=>'tag', 'title'=>'Tagi', 'class'=>'icon-tags'),
		array('slug'=>'icons','title'=>'Ikonki','class'=>'icon-picture'),
		),

	'root' => array(
		array('slug'=>'poll', 'title'=>'Ankiety', 'class'=>'icon-list-alt'),
		array('slug'=>'profil', 'title'=>'Profile', 'class'=>'icon-user'),
		array('slug'=>'category', 'title'=>'Kategorie', 'class'=>'icon-tags'),
		array('slug'=>'settings','title'=>'Ustawienia','class'=>'icon-wrench'),
		),

	'moderator' => array(
		array('slug'=>'comments', 'title'=>'Komentarze', 'class'=>'icon-comment'),
		),

	'dev' => array(
		array('slug'=>'doc','title'=>'Dokumentacja','class'=>'icon-book'),
		),
);

