<?php defined('SYSPATH') or die('No direct script access.');

class Riupress_Flash {
	public static function msg($errors = false,$flash = false){
		if(!empty($flash)){
				if(is_array($flash)){
					foreach($flash as $f){
					echo '<section class="alert alert-'.$f[0].'">';
					echo '<a class="close" data-dismiss="alert" href="#">×</a>';
					echo $f[1];
					echo '</section>';
					}
					
				}
				else{
					echo '<section class="alert alert-success"><a class="close" data-dismiss="alert" href="#">×</a>'.$flash.'</section>';
				}
		}
		else{
			if(!empty($errors)){
				if(is_array($errors)){
					foreach($errors as $error){
					echo '<section class="alert alert-error">';
					echo '<a class="close" data-dismiss="alert" href="#">×</a>';
					echo $error;
					echo '</section>';
					}
				}
				else{
					echo '<section class="alert alert-error"><a class="close" data-dismiss="alert" href="#">×</a>'.$errors.'</section>';
				}
			}
		}

	}
}
?>

