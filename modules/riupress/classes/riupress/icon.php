<?php defined('SYSPATH') or die('No direct script access.');
 
class Riupress_Icon{

	public static function editorrender($results,$uid)
	{
		echo '<table class="table table-striped">';
		$time = time();
		foreach( $results as $w )
		{
			Icon::renderlist($w,$uid,$time);
		}
		echo '</table>';
	}

	public static function renderlist($r,$uid,$time)
	{

		echo '<tr>'."\n";
		echo '<td class="termtd">'.Icon::icon('small',$r['term'],$r['id']).'<h3>'.Html::anchor('editor/icons/edit/'.$r['id'],$r['term']).'</h3></td>'."\n";
		echo '<td>'.Html::anchor('editor/icons/edit/'.$r['id'], '<i class="icon-edit icon-white"></i> Zmień', array('class'=>'btn btn-success')).' '.Html::anchor('editor/icons/delete/'.$r['id'], '<i class="icon-remove icon-white"></i> Usuń', array('class'=>'btn btn-danger')).'</td>';
		echo '</tr>'."\n";

	}

	public static function icon($size,$alt,$icon,$class='icon')
	{
		if(!empty($icon)){
			return HTML::image('api/file/'.$icon.'/'.$size.'.jpg', array('alt' => $alt, 'class' => $class));
		}
		else{
			return HTML::image('api/file/1/'.$size.'.jpg', array('alt' => $alt, 'class' => $class));
		}
	}

	public static function title($term)
	{
		echo '<td>'.$term.'</td>'."\n";
	}

}
?>
