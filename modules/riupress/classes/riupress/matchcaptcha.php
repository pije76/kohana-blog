<?php defined('SYSPATH') or die('No direct script access.');
 
class Riupress_Matchcaptcha {


	public static function get()
	{
		$sum = Session::instance()->get('captchasum');
		if(empty($sum)){
		Matchcaptcha::generate();
		}
		$val1 = Session::instance()->get('captchaval1');
		$val2 = Session::instance()->get('captchaval2');	
		echo Form::label('captchasum', 'Podaj sumę działania: <strong>'.$val1.' +  '.$val2.'</strong> ');
		echo Form::input('captchasum', FALSE, array('class'=>'span1'), TRUE);
	}

	public static function generate()
	{

		$val1 = rand(1,10);
		$val2 = rand(1,10);
		$sum = $val1+$val2;
		Session::instance()->set('captchaval1', $val1);
		Session::instance()->set('captchaval2', $val2);
		Session::instance()->set('captchasum', $sum);
	}
}
?>
