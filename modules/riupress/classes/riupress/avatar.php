<?php defined('SYSPATH') or die('No direct script access.');
 
class Riupress_Avatar {

 	public static function avatar($user, $name, $is_avatar, $gender, $file, $class=false)
	{
		if($class){
			$avataralt = array('alt' => $name, 'class' => $class);
		}
		else{
			$avataralt = array('alt' => $name);
		}

		if($is_avatar==1){
			return HTML::image('api/file/'.$user.'/'.$file.'.jpg', $avataralt);
		}
		else{
			return HTML::image('api/file/1/'.$file.$gender.'.jpg', $avataralt);
		}
	}
}
?>
