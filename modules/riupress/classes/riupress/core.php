<?php defined('SYSPATH') OR die('No direct access allowed.');

class Riupress_Core {

  	private $_requested, $_token, $_directory, $_controller, $_action, $_factory, $_time, $_key, $_title;
  	private $_role, $_roletitle;
	private $_tokenup,$_setup = FALSE;
	private $_loader = array();
	protected $_session, $_model, $_agent;
	protected $_config = array();
	protected $_roles = array();

	public function __construct($_factory=FALSE) {

		$this->_session = Session::instance();
		$this->_requested = Request::current();
		$this->_config = Kohana::$config->load('core');
		$this->_roles = Kohana::$config->load('roles');
		$this->_model = Model::factory('core_core');
		$this->_agent = Request::$user_agent;
		$this->_time = time();
		if($_factory){
			$user = $this->setconfig();
		}
	}

	public static function factory()
	{
		return new Core($_factory=TRUE);

	}

	protected static $instance;

	public static function instance()
	{
		if ( ! isset(Core::$instance))
		{
			Core::$instance = new Core();
		}

		return Core::$instance;
	}

	private function setconfig(){


	$this->_directory = $this->_requested->directory();

		if(!empty($this->_directory)){

		$this->_role = $this->_roles[$this->_directory]['dirid'];

			switch ($this->_directory)
			{

				case 'default':
				case 'api':
					$this->_title = $this->_config['apptitle'];
				break;

					
				default:
					$this->_title = $this->_config['apptitle'].' - '.$this->_roles[$this->_directory]['dirtitle'];
				break;
			}

		}
		else{

			$this->_role = 1;
			$this->_title = $this->_config['apptitle'];

		}
	
	$this->_controller = $this->_requested->controller();
	$this->_action = $this->_requested->action();
	$this->preload();

	$this->_key = $this->_session->get($this->_config['session_key']);

		if(empty($this->_key)){

			$user = $this->checktoken();

			if($user > 1){

				$sid = $this->createsession($user);
				$this->_setsession($user, TRUE);
				$this->_profileloader($user);

				return $user;

			}
			else{
				$this->createanonimsession();
				$this->_setsession(1);
				$this->_profileloader(1);
				return 1;

			}
		}

		else{

			$uid = $this->_session->get('uid');
			$sid = $this->_session->get('sid');

			if($sid>1){

				$cs = $this->_model->get_session($sid, $this->_time);

				if($cs == 1){
					$this->_upsession($uid, $sid);
					$this->_profileloader($uid);
					return $uid;

				}
				else{
					$this->logout();
				}

			}
			else{
				$this->_profileloader($uid);
			}

		}


	}

	private function preload(){

		$this->_loader = array_merge($this->_loader, array('title'=>$this->_title, 'role'=>$this->_role));
		
	}

	public function param()
	{
		return $this->_loader;
	}

	private function _profileloader($user)
	{

		$profile = Riudb::factory()->get($user)->render();

		if(!empty($profile[$user])){
		$this->_loader = array_merge($this->_loader, array('me'=>$profile[$user]));
		}

	}

	private function checktoken(){

		if ($token = Cookie::get($this->_config['cookie_key'])){
		$t = $this->_gettoken($token);
			if(!empty($t['agent'])){
				if (($t['agent'] == sha1($this->_agent)) AND ($t['expires'] > $this->_time)){

				$this->_tokenUp = TRUE;
				Cookie::set($this->_config['cookie_key'], $t['token'], $this->_config['lifetime']);
				$this->_token = $t['id'];
				return $t['user'];

				}
				else{
					if(!empty($t['id'])){
						$this->_model->del_token($t['id']);
					}
					else{
						$this->_model->del_tokens($this->_time);
					}

				Cookie::delete($this->_config['cookie_key']);
				return FALSE;

				}
			}
			else{
				return FALSE;
			}
		}
		else{
			return FALSE;
		}
	}

	private function _gettoken($token){
		return $this->_model->get_token($token);
	}

	private function _createtoken($user)
	{
		while (TRUE)
		{
			$token = text::random('alnum', 32);
			$tokenid = $this->_gettoken($token);
			if (empty($tokenid))
			{
			$this->_model->add_token($token,$user,$this->_time,$this->_agent,$this->_config['lifetime']);
			return $token;
			}
		}
	}

	private function createsession($user){
	$session = $this->_session->regenerate();
	$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
	$ip = Request::$client_ip;

	$referrer = $this->_requested->referrer();
	if(empty($referrer)){
	$referrer = '';
	}

		$add = $this->_model->add_session($this->_config['expires'],$user,$session,$this->_agent,$this->_requested->detect_uri(),$ip,$host,$this->_time,$referrer);
		$this->_model->up_summary($user);
		$this->_session->set($this->_config['session_key'],$session);
		$this->_session->set('sid',$add);

		return $add;
	}

	private function createanonimsession(){

		$session = $this->_session->regenerate();
		$this->_session->set($this->_config['session_key'],$session);
		$this->_session->set('sid',1);

		return 1;
	}

	private function _setsession($user){
	$this->_session->set('uid',$user);
	$this->_session->set('theme',$this->_config['theme']);
	}

	private function _upsession($uid, $sid){
	$requested = $this->_requested->detect_uri();
	$this->_model->up_session($sid, $this->_time, $requested,$this->_config['expires']);
	}

	public function login($email,$pass,$remember=FALSE)
	{

	$u = $this->_model->get_isuser($email);

	if(!empty($u['pass'])){

		$correct = $pass;
		$passcheck = Pass::instance()->passcheck($correct,$u['pass']);

			if($passcheck == 1){
				$sid = $this->createsession($u['user']);
				$this->_setsession($u['user']);

				if($remember){
				$token = $this->_createtoken($u['user']);
				Cookie::set($this->_config['cookie_key'], $token, $this->_config['lifetime']);
				}
				return TRUE;
			}
			return FALSE;
		}
		else{
			return FALSE;
		}
	}

	public function logout($redirect = FALSE)
	{
		$sid = $this->_session->get('sid');
		$this->_model->del_session($sid);
		$this->_session->destroy();
		Cookie::delete($this->_config['cookie_key']);
		if($redirect){
			$redirect = $redirect;
		}
		else{ 
			$redirect = $this->_requested->referrer(); 
		}
		$this->_requested->redirect($redirect);

	}

	public function __destruct() {
		if($this->_tokenUp){
		$this->_model->up_token($this->_token,$this->_config['lifetime']);
		}
	}
}
