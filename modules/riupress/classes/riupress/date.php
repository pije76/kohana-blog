<?php defined('SYSPATH') or die('No direct script access.');
 
class Riupress_Date extends Kohana_Date {


	// zwracanie czasu unix z dodaniem czasu lokalnego

	public static function sumarytime($time)
	{
	if($time>86400){
	$days = floor($time/86400);
  	$timeh = $time-($days*86400);
	}else{
	$timeh = $time;
	}

	if($timeh>3600){
	$hours = floor($timeh/3600);
	$hoursm = $timeh-($hours*3600);
	}else{
	$hoursm = $timeh;
	}

	if($hoursm>60){
	$minutes = floor($hoursm/60);
	$minutesm = floor($hoursm-($minutes*60));
	}else{
	$minutesm = floor($hoursm);
	}





	if(!empty($days)){ $d ="dni: <strong>".$days."</strong>, "; }else {$d='';}
	if(!empty($hours)){ $h ="godzin: <strong>".$hours."</strong>, "; }else {$h='';}
	if(!empty($minutes)){ $m ="minut:<strong>".$minutes."</strong>, "; }else {$m='';}
	if(!empty($minutesm)){ $s ="sekund: <strong>".$minutesm."</strong>"; }else {$s='';}
	return $d.$h.$m.$s;
	}


	// zwracanie czasu unix z dodaniem czasu lokalnego

	public static function gmt($data)
	{
	$gmt = Kohana::$config->load('core')->gmt;
	return $data+$gmt;
	}
	
	// zwracanie nazwy miesiąca w dopełniaczu

	public static function monthgenitive($data)
	{
	$list = date('n',$data);
	$pl = array( "", "stycznia", "lutego", "marca", "kwietnia", "maja", "czerwca", "lipca", "sierpnia", "września", "października", "listopada", "grudnia");
	return $pl[$list];
	}

	// zwracanie skróconej nazwy miesiąca

	public static function monthshort($data)
	{
	$list = date('n',$data);
	$pl = array( "", "sty", "lut", "mar", "kwi", "maj", "cze", "lip", "sie", "wrz", "paź", "lis", "gru");
	return $pl[$list];
	}
	// zwracanie nazwy dnia tygodnia w mianowniku


	public static function monthpl($month,$year)
	{
	if( !(int) $month){
		$month = date('m');
	}

	$pl = array( "01" => "Styczeń", "02" => "Luty", "03" => "Marzec", "04" => "Kwiecień", "05" => "Maj", "06" => "Czerwiec", "07" => "Lipiec", "08" => "Sierpień", "09" => "Wrzesień", "10" => "Październik", "11" => "Listopad", "12" => "Grudzień");

	return $pl[$month].' - '.$year;
	}

	// zwracanie nazwy dnia tygodnia w mianowniku

	public static function daypl($day)
	{
	$pl = array("Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela");
	return $pl[$day];
	}
	// zwracanie nazwy dnia tygodnia w mianowniku

	public static function weekdaynominative($data)
	{
	$list = date('N',$data);
	$pl = array( "", "poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota", "niedziela");
	return $pl[$list];
	}

	// zwracanie nazwy dnia tygodnia w dopełniaczu
	
	public static function weekdaygenitive($data)
	{
	$list = date('N',$data);
	$pl = array( "", "poniedziałku", "wtorku", "środy", "czwartku", "piątku", "soboty", "niedzieli");
	return $pl[$list];
	}
	
	// zwracanie date w formacie dzień tygodnia, DD miesiąca YYYY 

	public static function fullgenitive($data)
	{
	$data = Date::gmt($data);
	
	$day = Date::weekdaygenitive($data);
	$month = Date::monthgenitive($data);
	$mm = date('j',$data);
	$rrrr = date('Y',$data);
	return $day.', '.$mm.' '.$month.' '.$rrrr;
	}

	public static function fullgenitivee($data)
	{
	$data = Date::gmt($data);
	
	$day = Date::weekdaygenitive($data);
	$month = Date::monthgenitive($data);
	$mm = date('j',$data);
	$rrrr = date('Y',$data);
	return $day.', '.$mm.' '.$month.' '.$rrrr;
	}

	public static function fullnominative($data)
	{
	$data = Date::gmt($data);
	$day = Date::weekdaynominative($data);
	$month = Date::monthgenitive($data);
	$mm = date('j',$data);
	$rrrr = date('Y',$data);
	return $day.', '.$mm.' '.$month.' '.$rrrr;
	}
	public static function fullnominativetime($data)
	{
	$data = Date::gmt($data);
	$day = Date::weekdaynominative($data);
	$month = Date::monthgenitive($data);
	$mm = date('j',$data);
	$rrrr = date('Y',$data);
	$data1 = gmstrftime ("%R",$data);
	return $day.', '.$mm.' '.$month.' '.$rrrr.', '.$data1;
	}
	// zwracanie date w formacie dzień tygodnia, DD miesiąca YYYY 

	public static function shortdaydate($data)
	{
	$data = Date::gmt($data);
	$month = Date::monthshort($data);
	$mm = date('j',$data);

	return $mm.' '.$month;
	}
	
	// zwykła data
	
	public static function polish($data)
	{
	$gmt = Kohana::$config->load('core')->gmt;
	$data1 = gmstrftime ("%d-%m-%Y, %R",$data+$gmt);
	return $data1;	
	}
	
	
	// przeszłość

	public static function distanceOfTimeInWords($fromTime, $toTime = 0) {
	$distanceInSeconds = round(abs($toTime - $fromTime));
	$distanceInMinutes = round($distanceInSeconds / 60);
	$gmt = Kohana::$config->load('core')->gmt;;
	$data = gmstrftime ("%R",$fromTime+$gmt);

		if ( $distanceInMinutes <= 1 ) {
		return 'przed chwilą';
		}
		if ( $distanceInMinutes < 5 ) {
		return $distanceInMinutes . ' minuty temu';
		}
		if ( $distanceInMinutes < 45 ) {
		return $distanceInMinutes . ' minut temu';
		}
		if ( $distanceInMinutes < 90 ) {
		return 'około godziny temu';
		}
		if ( $distanceInMinutes < 1440 ) {
		return 'około ' . round(floatval($distanceInMinutes) / 60.0) . ' godzin temu';
		}
		if ( $distanceInMinutes < 2880 ) {
		return 'wczoraj o '.$data;
		}
		if ( $distanceInMinutes < 43200 ) {
		return 'około ' . round(floatval($distanceInMinutes) / 1440) . ' dni temu';
		}
		if ( $distanceInMinutes < 86400 ) {
		return 'miesiąc temu';
		}
		if ( $distanceInMinutes < 525600 ) {
		return round(floatval($distanceInMinutes) / 43200) . ' miesięcy temu';
		}
		if ( $distanceInMinutes < 1051199 ) {
		return 'ponad rok temu';
		}
		
		return 'ponad ' . round(floatval($distanceInMinutes) / 525600) . ' lat(a) temu';
	}
	
	// przyszłość
		
	public static function distanceto($fromTime, $toTime = 0) {
	$distanceInSeconds = round(abs($toTime - $fromTime));
	$distanceInMinutes = round($distanceInSeconds / 60);
	$gmt = Kohana::$config->load('core')->gmt;;
	$data = gmstrftime ("%R",$fromTime+$gmt);
	
		if ( $toTime > $fromTime ) {
		return 'dziś / trwa teraz';
		}
		if ( $distanceInMinutes < 10 ) {
		return 'za chwilę';
		}
		if ( $distanceInMinutes < 45 ) {
		return 'za '. $distanceInMinutes . ' minut';
		}
		if ( $distanceInMinutes < 90 ) {
		return 'za około godzinę';
		}
		if ( $distanceInMinutes < 1440 ) {
		return 'za około ' . round(floatval($distanceInMinutes) / 60.0) . ' godzin';
		}
		if ( $distanceInMinutes < 2880 ) {
		return 'jutro o '.$data;
		}
		if ( $distanceInMinutes < 43200 ) {
		return 'za ' . round(floatval($distanceInMinutes) / 1440) . ' dni';
		}
		if ( $distanceInMinutes < 86400 ) {
		return 'za miesiąc';
		}
		if ( $distanceInMinutes < 525600 ) {
		return 'za '. round(floatval($distanceInMinutes) / 43200) . ' miesięcy';
		}
		if ( $distanceInMinutes < 1051199 ) {
		return 'za prawie rok';
		}
		
		return 'za ' . round(floatval($distanceInMinutes) / 525600) . ' lat';
	}
	
	// formatowanie z daty YYYYMMDD
	
	// ----- do unix
	
	public static function formatymdtounix($date)
	{
	return mktime(0, 0, 0, substr($date, 4, 2), substr($date, 6, 2), substr($date, 0, 4));
	}
	
	
	// ----- do formatu czytelnego
	
	public static function formatymdtohuman($date)
	{
	$data = Date::formatymdtounix($date);
	return Date::fullgenitive($data);
	}
	
	// ----- krótka do formatu czytelnego
	
	public static function shortdaydatetohuman($date)
	{
	$data = Date::formatymdtounix($date);
	return Date::shortdaydate($data);
	}
	
	// ----- ilość dnia między dwoma datami 
	
	public static function distancebetweendays($from, $for)
	{
	$first = Date::formatymdtounix($for);
	$second = Date::formatymdtounix($from);
	return ($first-$second)/86400;
	}
	
	// ----- zwracanie daty dla równania YYYYMMDD  + ilość dni
	
	public static function distanceplusdays($from, $days)
	{
	$unixdate = Date::formatymdtounix($from);
	$date = ($days*86400)+$unixdate;
	return date('Ymd',$date);
	}

	// ----- zwracanie daty dla różnicy YYYY-MM-DD  - ilość dni
	
	public static function distanceminus($from, $days)
	{
	$date = $from-($days*86400);
	return Date::fullgenitive($date);
	}

	// ----- ilość lat od teraźniejszości do określonej daty - wiek użytkownika
	
	public static function yearsbetweendays($from)
	{

	$data = explode("-", $from); 

    	$rok = $data[0]; 
    	$mies = $data[1]; 
    	$dzien = $data[2];  

	$teraz = date('Y-m-d');
	$teraz = explode("-", $teraz); 

    	$terazrok = $teraz[0]; 
    	$terazmies = $teraz[1]; 
    	$terazdzien = $teraz[2]; 

	$rok_teraz = date('Y'); 
	$wiek = $terazrok - $rok; 

	if($terazmies>=$mies AND $terazdzien>=$dzien){
	$wiek = $wiek;
	} 
	else {	
	$wiek--;
	} 
	return $wiek;
	}
}
 
?>
