<?php defined('SYSPATH') OR die('No direct access allowed.');

class Riupress_Arr extends Kohana_Arr {

	public static function select($array)
	{
		
	    $new_array = array();

	    foreach ($array as $a)
	    {

		    $new_array[$a['id']] = $a['name'];
	    }

	    return $new_array;
	}
}
