<?php defined('SYSPATH') OR die('No direct access allowed.');

class Riupress_Pass {

	protected static $instance;

	private $PasswordHash;

	public function __construct()
	{
		if ( ! class_exists('PasswordHash', FALSE))
			{
				require Kohana::find_file('vendor', 'PasswordHash');
			}
			$this->PasswordHash = new PasswordHash(8, FALSE);
	}
	public static function instance()
	{
		if ( ! isset(Pass::$instance))
		{
			Pass::$instance = new Pass();
		}
		return Pass::$instance;
	}

	public function passnew($correct){
	return $this->PasswordHash->HashPassword($correct);
	}

	public function passcheck($orginal, $correct){
	return $this->PasswordHash->CheckPassword($orginal, $correct);
	}
}
?>

