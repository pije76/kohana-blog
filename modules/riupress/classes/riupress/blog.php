<?php defined('SYSPATH') or die('No direct script access.');
 
class Riupress_Blog {

	public static function editorrender($results,$uid)
	{
		echo '<table class="table table-striped">';
		$time = time();
		foreach( $results as $w )
		{
			Blog::renderlist($w,$uid,$time);
		}
		echo '</table>';
	}

	public static function renderlist($r,$uid,$time)
	{

		echo '<tr>'."\n";
		echo '<td class="termtd"><h4>'.Html::anchor('editor/blog/edit/'.$r['id'],$r['term']).'</h4><span style="font-size:0.8em;">'.Date::fullnominativetime($r['publish']).'</span></td>'."\n";
		echo '<td>'.Html::anchor('editor/blog/edit/'.$r['id'], '<i class="icon-edit icon-white"></i> Zmień', array('class'=>'btn btn-success')).' '.Html::anchor('editor/blog/delete/'.$r['id'], '<i class="icon-remove icon-white"></i> Usuń', array('class'=>'btn btn-danger')).'</td>';
		echo '</tr>'."\n";

	}

	public static function icon($size,$alt,$icon,$class='icon')
	{
		if(!empty($icon)){
			return HTML::image('api/file/'.$icon.'/'.$size.'.jpg', array('alt' => $alt, 'class' => $class));
		}
		else{
			return HTML::image('api/file/1/icon/'.$size.'.jpg', array('alt' => $alt, 'class' => $class));
		}
	}

	public static function title($term)
	{
		echo '<td>'.$term.'</td>'."\n";
	}


	public static function meta($text) {

		if ( preg_match('[break]', $text) ) {
			$list = explode('[break]', $text);

			if(!empty($list[1])){
		      	$text = $list[0];
			}
		}

		return $text;
	}

	public static function full($text,$page = 1) {

		if ( preg_match('[break]', $text) ) {
			$list = explode('[break]', $text);
			$counter = count($list);
			if(!empty($list[$page])){
			$count = $counter-1;
				if($page==='1'){
			      	echo $list[0];
			      	echo $list[$page];
				}
				else{
			      	echo $list[$page];
				}
			}
			else{
			$count = 1;
			echo $text;
			}
		}
		else{
			$count = 1;
			echo $text;
		}

		if($count > 1){
		$pagination = Pagination::factory(array(
				'current_page'   => array('source' => 'route', 'key' => 'page'),
				'total_items'    => $count,
				'view'           => 'pagination/article',
				'items_per_page' => 1,
			));

		echo $pagination->render();
		}
	}

	public static function tags($tags){
		echo '<section class="ratings">';
		if(!empty($tags)){

			echo '<span class="pull-left">Wpis przypisany do tagów:</span>';
			echo '<ul class="relations">';
		foreach($tags as $t){
			echo '<li>'.Html::anchor('tag/'.$t['slug'], $t['term'], array('title'=>$t['term'],'class'=>'label label-info')).'</li>';
		}
			echo '</ul>';

		}
		else{
		echo '<span class="pull-left">Wpis nie został przypisany do żadnych tagów.</span>';
		}
		echo '</section>';
	}

	public static function relations($relations){
		if(!empty($relations)){
		echo '<section class="ratings">';
			echo '<span class="pull-left">Wpisy podobne lub powiązane:</span>';
			echo '<ul class="relations">';
		foreach($relations as $r){
			echo '<li>'.Html::anchor($r['id'], $r['term'], array('title'=>$r['term'],'class'=>'label')).'</li>';
		}
			echo '</ul>';
		echo '</section>';
		}
	}
	public static function rating($link,$uid,$id,$sum,$ratings){

		$count = count($ratings);
		if(!empty($count)){
		$width = ($sum/$count);
		}
		else{
		$width = 0;
		}
		if(!empty($count) AND !empty($sum)){
		$allsum = $count*5;
		$percent = ($sum*100)/$allsum;
		}
		else{
		$percent = 0;
		$allsum = 0;
		}
		$percent = round($percent, 0);
		$width = round($width, 2);
		echo '<section class="ratings pull-right">';
		if($width>0){
		echo '<span id="prating" class="pull-right">Średnio oceniony na '.$width.', wszytkich głosów: '.$count.'</span>';
		}
		else{
		echo '<span id="prating"  class="pull-right">Ten wpis jeszcze nie został oceniony.</span>';
		}
		echo '<ul class="rating"><li class="current" style="width: '.$percent.'%;">średnia glosów: '.$width.'</li>';
		if(!in_array($uid,$ratings) AND $uid>1){
		echo '<li class="nocurrent">'.Html::anchor($link, 'Przeciętne', array('title'=>'Przeciętne', 'id' => $id.'-1', 'class'=>'rate1 ratme')).'</li>
		<li class="nocurrent">'.Html::anchor($link, 'Może być', array('title'=>'Może być', 'id' => $id.'-2', 'class'=>'rate2 ratme')).'</li>
		<li class="nocurrent">'.Html::anchor($link, 'Dobre', array('title'=>'Dobre', 'id' => $id.'-3', 'class'=>'rate3 ratme')).'</li>
		<li class="nocurrent">'.Html::anchor($link, 'Bardzo dobre', array('title'=>'Bardzo dobre', 'id' => $id.'-4', 'class'=>'rate4 ratme')).'</li>
		<li class="nocurrent">'.Html::anchor($link, 'Kocham to!', array('title'=>'Kocham to!', 'id' => $id.'-5', 'class'=>'rate5 ratme')).'</li>';
		echo '</ul>';
		}
		else{
		echo '</ul>';
		}
		echo '</section>';

	}

		public static function torss($text) {

		$in = array('/\<p\>(.*?)\<\/p\>/ms');
		$out = array('\1');
		$text = preg_replace($in, $out, $text);

		if ( preg_match('[break]', $text) ) {
			$list = explode('[break]', $text);

			if(!empty($list[1])){
		      	$text = $list[0];
			}
		}

		return $text;
		}
}
?>
