<?php defined('SYSPATH') OR die('No direct access allowed.');

class Riupress_Profil {

	protected $_model;
	protected $_config = array();

	public function __construct() {

		$this->_config = Kohana::$config->load('core');
		$this->_model = Model::factory('profil');

	}

	/*
	* Tworzenie fabryki
	*/

	public static function factory()
	{
		return new Profil();

	}

	/*
	* Instancja obiektu bez wywołania metod fabryki.
	*/

	protected static $instance;

	public static function instance()
	{
		if ( ! isset(Profil::$instance))
		{
			Profil::$instance = new Profil();
		}

		return Profil::$instance;
	}


	/*
	* Wysyłanie komunikatów dotyczących profilu.
	*/


	private function _sendmail($subject, $email, $post)
	{

		$mailer = Sender::connect();
		$message = Swift_Message::newInstance()
			->setSubject($subject)
			->setFrom(array($this->_config->mail => $this->_config->admin))
			->setTo(array($email))
			->setBody($post, 'text/html');
		$mailer->send($message);

	}



	/*
	* Tworzenie użytkownika
	* Profil::instance()->create(params)
	* Tworzenie nowego uzytkownika wiąże się z podaniem i sprawdzaniem
	* szeregu danych. Metoda zwraca określony wynik:
	* - 1 - konto utworzono i wysłano email
	* - 2 - podaj poprawnie wszystkie dane
	*/


	public function create($name, $email, $pass, $gender, $is_active = 2, $members = array(),$send = TRUE)
	{

		$add = $this->_model->add_fullprofile($name,$email,$pass,$gender,$is_active,$members);

		if($add AND $send){
			Session::instance()->set('flash', array(array('success','Gratuluję! Konto zostało założone. Sprawdź pocztę i zaloguj się .')));
			//$this->register($add);

		}
		else{
			Session::instance()->set('flash', array(array('error','Niestety, konto nie zostało założone .')));
		}
	}

	/*
	* Wysłanie potwierdzenia rejestracji.
	*/


	private function register($param)
	{

		$activatepath = url::base()."profil/activate/".$param['id']."/".$param['key']."/";
		$subject = 'Potwierdznie rejestracji profilu '.$param['name'].'';
		$admin_name = $this->_config->admin;
		$admin_mail = $this->_config->mail;
		$post = Theme::factory('default/profil/mail/register')
				->bind('name', $param['name'])
				->bind('admin_name', $admin_name)
				->bind('admin_mail', $admin_mail)
				->bind('path', $activatepath);

		$this->_sendmail($subject, $param['email'], $post);

	}

	/*
	* Aktywacja konta
	*/

	public function activate($uid, $key)
	{
	
		$profile = Riudb::factory()->get($uid)->render();

		if(!empty($profile[$uid]['is_active']) AND $profile[$uid]['is_active']===2){
			$count = $this->_model->is_active($uid, $key);
			if(!empty($count)){
				$is_active = 1;
				$data = array('is_active'=>$is_active);
				Riudb::factory()->id($uid)->save($data);
				$this->_model->up_active($uid);
				Session::instance()->set('flash',array(array('success','Konto zostało aktywowane.')));
				return TRUE;
			}
			else{
				Session::instance()->set('flash',array(array('warning','Klucz aktywacyjny jest nieprawidłowy, lub nie pasuje do użytkownika.')));
				return FALSE;
			}
		}
		else{
			Session::instance()->set('flash',array(array('warning','Nie ma takiego użytkownika, '.Debug::vars($profile[$uid]).'lub to konto jest już aktywne.')));
			return FALSE;
		}
	}

	/*
	* Przypomnienie hasła / Odzyskanie konta
	*/

	public function getrecover($email)
	{

		$user = $this->_model->get_email_user($email);

		if($user['user']){

			$count = $this->_model->get_recover_count($user['user']);
			if($count > 2){
				Session::instance()->set('flash', array(array('warning','Na to konto wysłano wielokrotnie link do zmiany hasła. Należy z nich skorzystać lub odczekać przynajmniej 24 h przed korzystaniem z następnej próby ustawienia nowego hasła .')));
				return FALSE;
			}
			else{
	    			$add = $this->_model->add_recover($user);
				//$this->recover($add);
				Session::instance()->set('flash', array(array('success','Na podany adres email został wysłany link umożliwiający stworzenie nowego hasła.')));
				return TRUE;
			}
		}
		else{
			Session::instance()->set('flash', array(array('warning','Ten to konto nie zostało jeszcze aktywowane, lub jest zablokowane.')));
			return FALSE;
		}

	}


	/*
	* Wysłanie linka do zmiany hasła.
	*/


	private function recover($param)
	{

		$activatepath = url::base()."profil/recover/".$param['id']."/".$param['key']."/";
		$subject = 'Link do zmiany hasła dla '.$param['name'].'';
		$admin_name = $this->_config->admin;
		$admin_mail = $this->_config->mail;
		$post = Theme::factory('default/profil/mail/recover')
				->bind('name', $param['name'])
				->bind('admin_name', $admin_name)
				->bind('admin_mail', $admin_mail)
				->bind('path', $activatepath);

		$this->_sendmail($subject, $param['email'], $post);

	}
	
}
