<?php defined('SYSPATH') OR die('No direct access allowed.');

class Riupress_HTML extends Kohana_HTML {

	/**
	 * Convert HTML entities to special characters.  This function is the 
	 * opposite of htmlspecialchars().
	 *
	 *     echo HTML::charsdecode($username);
	 *
	 * @param   string   convert convert
	 * @param   boolean  encode existing entities
	 * @return  string
	 */
	public static function charsdecode($value, $double_encode = TRUE)
	{
		return htmlspecialchars_decode( (string) $value, ENT_QUOTES);
	}
}

