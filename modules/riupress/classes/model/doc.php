<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Doc extends Model_Core {

	public function get_docs()
	{
	return DB::select('id','record', 'parent', 'depth', 'order', 'term', 'slug')
			->from('docs')
			->order_by('depth','ASC')
			->order_by('order','ASC')
			->execute()
			->as_array();
	}
	public function get_docid($depth,$order,$parent = false)
	{
		$query = DB::select('record')->from('docs')->where('depth','=',$depth)->and_where('order','=',$order);

		if($parent){
			$query->and_where('parent','=',$parent);
		}

		$query = $query->execute()->current();

		if(!empty($query)){
			return $query['record'];
		}
	}

	public function get_next($publish)
	{
	return DB::select()->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.status','=',1)->and_where('publishes.publish','>',$publish)
			->group_by('publishes.record')->order_by('publishes.publish', 'ASC')->limit(1)->offset(0)
			->execute()->current();
	}
	public function get_prev($publish)
	{
	return DB::select()->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.status','=',1)->and_where('publishes.publish','<',$publish)
			->group_by('publishes.record')->order_by('publishes.publish', 'DESC')->limit(1)->offset(0)
			->execute()->current();
	}

}

