<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Dev_Doc extends Model_Core {

 	public function json_doc()
	{
	return array('id'=>'', 'parent'=>'', 'module'=>'', 'term'=>'', 'slug'=>'', 'depth'=>'', 'order'=>'', 'post'=>'', 'files'=>array());
	}

	public function add_doc($record,$parent,$depth,$term,$slug)
	{
		if($record===$parent){
		$count = $this->count_docs();
		}
		else{
		$count = $this->count_docs($parent);
		}

		$count++;
		DB::insert('docs', array('record', 'parent', 'depth', 'order','term','slug'))->values(array($record,$parent,$depth,$count,$term,$slug))->execute();
		return $count; 
	}

	public function count_docs($parent = false)
	{
		$query = DB::select(DB::expr('COUNT(*) AS count'))->from('docs');
		if($parent){
			$query->where('parent','=',$parent)->and_where('depth','=',1);
		}
		else{
			$query->where('depth','=',0);
		}
		$count = $query->execute()->get('count');
		return $count ;
	}

	public function get_all()
	{
	return DB::select()
			->from('docs')
			->order_by('depth','ASC')
			->order_by('order','ASC')
			->execute()
			->as_array();
	}

	public function get_parents($parent=false)
	{

		$query = DB::select(array('record','id'), array('term','name'))->from('docs')->where('depth','=',0);
		if($parent){
			$query->and_where('record','!=',$parent);
		}
		return $query->execute()->as_array();
	}

	public function count_catsubslug($record,$parent,$depth,$slug)
	{

		return DB::select(DB::expr('COUNT(*) AS count'))->from('docs')
			->where('parent','=',$parent)->and_where('depth','=',$depth)->and_where('slug','=',$slug)->and_where('record','!=',$record)
			->execute()->get('count');
	}

	public function count_catslug($record,$depth,$slug)
	{

		return DB::select(DB::expr('COUNT(*) AS count'))->from('docs')
			->where('depth','=',$depth)->and_where('slug','=',$slug)->and_where('record','!=',$record)
			->execute()->get('count');
	}

	public function up_doc($id,$parent,$depth,$order,$term,$slug,$post)
	{
		$query = DB::update('docs')->set(array('parent'=>$parent,'depth'=>$depth,'order'=>$order,'term'=>$term,'slug'=>$slug,'post'=>$post))->where('record','=',$id)->execute();
	}

	public function up_doccount($id,$count)
	{
		$query = DB::update('docs')->set(array('count'=>$count))->where('record','=',$id)->execute();
	}


	public function up_subs($module,$parent=false)
	{

		$query = DB::select('record')->from('docs');
		if($parent){
			$query->where('parent','=',$parent)->and_where('depth','=',1);
		}
		else{
			$query->where('depth','=',0);
		}
		$result = $query->execute()->as_array();
		$i = 1;

		foreach($result as $q){
			DB::update('docs')->set(array('order'=>$i))->where('record','=',$q['record'])->execute();
			$array = array('order'=>$i);
			Riudb::factory()->id($q['record'])->save($array);
			$i++;
		}

	}

	public function get_postterm($id)
	{
	return DB::select()->from('docs')
			->where('record','=',$id)->execute()->current();
	}
}

