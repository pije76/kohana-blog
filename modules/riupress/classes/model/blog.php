<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Blog extends Model_Core {

	public function captcha_check($captchasum)
	{
		$sum = Session::instance()->get('captchasum');

		if($sum == $captchasum){
			$bool = TRUE;
		}
		else{
			$bool = FALSE;
		}
		return (bool) $bool;
	}

	public function allRecords($offset,$records)
	{
	$records = DB::select('record')->from('publishes')->where('status','=',1)->and_where('publish','<',DB::expr('UNIX_TIMESTAMP()'))
			->group_by('record')->order_by('publish', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
	$ar = array();
	foreach($records as $r){
		$ar[] = $r['record'];
	}
	return $ar;
	}


	public function countallRecords()
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('publishes')
			->execute()->get('count');
	}

	public function searchallRecords($search,$offset,$records)
	{
	$records =  DB::select('publishes.record')->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.status','=',1)->and_where('terms.term','LIKE','%'.$search.'%')->and_where('publishes.publish','<',DB::expr('UNIX_TIMESTAMP()'))
			->group_by('publishes.record')->order_by('publishes.publish', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
	$ar = array();
	foreach($records as $r){
		$ar[] = $r['record'];
	}
	return $ar;
	}


	public function searchcountallRecords($search)
	{
	return DB::select(DB::expr('COUNT(DISTINCT publishes.record) AS count'))->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.status','=',1)->and_where('terms.term','LIKE','%'.$search.'%')->and_where('publishes.publish','<',DB::expr('UNIX_TIMESTAMP()'))
			->execute()->get('count');
	}

	public function allTags($tag,$offset,$records)
	{
	$records =  DB::select('publishes.record')->from('publishes')->join('relations')->on('publishes.record','=','relations.record')
			->where('publishes.status','=',1)->and_where('relations.relation','=',$tag)->and_where('relations.module','=',2)->and_where('publishes.publish','<',DB::expr('UNIX_TIMESTAMP()'))
			->group_by('publishes.record')->order_by('publishes.publish', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
	$ar = array();
	foreach($records as $r){
		$ar[] = $r['record'];
	}
	return $ar;
	}


	public function countallTags($tag)
	{
	return DB::select(DB::expr('COUNT(DISTINCT publishes.record) AS count'))->from('publishes')->join('relations')->on('publishes.record','=','relations.record')
				->where('publishes.status','=',1)->and_where('relations.relation','=',$tag)->and_where('relations.module','=',2)->and_where('publishes.publish','<',DB::expr('UNIX_TIMESTAMP()'))
			->execute()->get('count');
	}

	public function allCats($cat,$offset,$records)
	{
	$records =  DB::select('publishes.record')->from('publishes')->join('relations')->on('publishes.record','=','relations.record')
			->where('publishes.status','=',1)->and_where('relations.relation','=',$cat)->and_where('relations.module','=',3)->and_where('publishes.publish','<',DB::expr('UNIX_TIMESTAMP()'))
			->group_by('publishes.record')->order_by('publishes.publish', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
	$ar = array();
	foreach($records as $r){
		$ar[] = $r['record'];
	}
	return $ar;
	}


	public function countallCats($cat)
	{
	return DB::select(DB::expr('COUNT(DISTINCT publishes.record) AS count'))->from('publishes')->join('relations')->on('publishes.record','=','relations.record')
				->where('publishes.status','=',1)->and_where('relations.relation','=',$cat)->and_where('relations.module','=',3)->and_where('publishes.publish','<',DB::expr('UNIX_TIMESTAMP()'))
			->execute()->get('count');
	}
	public function get_categories($module)
	{
	return DB::select()
			->from('categories')
			->where('module','=',$module)
			->and_where('count','>',0)
			->order_by('depth','ASC')
			->order_by('order','ASC')
			->execute()
			->as_array();
	}
	public function get_tagid($tag)
	{
		$query = DB::select()->from('tags')->where('slug','=',$tag)->execute()->current();
		if(!empty($query)){
			return $query['record'];
		}
	}

	public function get_next($publish)
	{
	return DB::select()->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.status','=',1)->and_where('publishes.publish','>',$publish)
			->group_by('publishes.record')->order_by('publishes.publish', 'ASC')->limit(1)->offset(0)
			->execute()->current();
	}
	public function get_prev($publish)
	{
	return DB::select()->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.status','=',1)->and_where('publishes.publish','<',$publish)
			->group_by('publishes.record')->order_by('publishes.publish', 'DESC')->limit(1)->offset(0)
			->execute()->current();
	}

	public function get_poll()
	{
		$time = time();
		$record = DB::select('record')->from('polls')->where('status','=',1)->and_where('publish','<',$time)->and_where('expiry','>',$time)
				->order_by('publish', 'DESC')->limit(1)->execute()->current();
		if(!empty($record['record'])){
			$rec = Riudb::factory()->get($record['record'])->render();
			return $rec[$record['record']];
		}
	}
}

