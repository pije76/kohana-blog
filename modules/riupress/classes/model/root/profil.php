<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Root_Profil extends Model {


	public function allprofiles($offset,$records)
	{
	$records = DB::select()->from('users')->join('profiles')->on('users.user','=','profiles.user')->limit($records)->offset($offset)->execute()->as_array();
	return $records;
	}

	public function countprofiles()
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('users')->execute()->get('count');
	}

	public function searchallprofiles($search,$offset,$onpage)
	{
	return DB::select()->from('users')->join('profiles')->on('users.user','=','profiles.user')->where('profiles.name','LIKE','%'.$search.'%')->limit($onpage)->offset($offset)->execute()->as_array();
	}

	public function searchcountprofiles($search)
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('users')->join('profiles')->on('users.user','=','profiles.user')->where('profiles.name','LIKE','%'.$search.'%')->execute()->get('count');
	}

	public function emailallprofiles($search,$offset,$onpage)
	{
	return DB::select()->from('users')->join('profiles')->on('users.user','=','profiles.user')->where('users.email','LIKE','%'.$search.'%')->limit($onpage)->offset($offset)->execute()->as_array();
	}

	public function emailcountprofiles($search)
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('users')->join('profiles')->on('users.user','=','profiles.user')->where('users.email','LIKE','%'.$search.'%')->execute()->get('count');
	}

	public function idprofile($id)
	{
	return DB::select()->from('users')->join('profiles')->on('users.user','=','profiles.user')->where('users.user','=',$id)->execute()->current();
	}

	public function saveidprofil($id,$is_active,$is_ban)
	{
		DB::update('profiles')->set(array('is_active'=>$is_active,'is_ban'=>$is_ban))->where('user','=',$id)->execute();
	}

	public function isusermember($user,$role)
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('members')
				->where('user','=',$user)->and_where('role','=',$role)
				->execute()->get('count');
	}

	public function addrolemember($record,$role)
	{
	return DB::insert('members', array('user','role'))
		->values(array($record,$role))
		->execute();
	}

	public function allmembers($record)
	{
	return DB::select('members.id', 'members.user',array('roles.slug','role'))
			->from('members')->join('roles')->on('members.role','=','roles.id')
			->where('members.user','=',$record)->and_where('members.role','>',2)->execute()->as_array();

	}

	public function allmemberroles($mroles)
	{
	return DB::select('id', array('slug','name'))->from('roles')->where('id','>',2)->where('id','NOT IN',$mroles)->execute()->as_array();

	}

	public function memberusers($id)
	{
	return DB::select(array(DB::expr('GROUP_CONCAT(role)'),'roles'))
			->from('members')
			->where('user','=',$id)
			->and_where('role','>',2)
			->group_by('user')
			->execute()->current();
	}
	public function deletemember($id)
	{
	return DB::delete('members')->where('id','=',$id)->execute();
	}

}

