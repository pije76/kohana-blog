<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Root_Category extends Model_Core {

 	public function json_category()
	{
	return array('id'=>'', 'parent'=>'', 'module'=>'', 'term'=>'', 'slug'=>'', 'depth'=>'', 'order'=>'', 'count'=>'0');
	}

	public function add_category($record,$parent,$depth,$module,$term,$slug)
	{
		if($record===$parent){
		$count = $this->count_categories($module);
		}
		else{
		$count = $this->count_categories($module,$parent);
		}

		$count++;
		DB::insert('categories', array('record', 'parent', 'depth', 'order','module','term','slug'))->values(array($record,$parent,$depth,$count,$module,$term,$slug))->execute();
		return $count; 
	}

	public function count_categories($module,$parent = false)
	{
		$query = DB::select(DB::expr('COUNT(*) AS count'))->from('categories')->where('module','=',$module);
		if($parent){
			$query->and_where('parent','=',$parent)->and_where('depth','=',1);
		}
		else{
			$query->and_where('depth','=',0);
		}
		$count = $query->execute()->get('count');
		return $count ;
	}

	public function get_all($module)
	{
	return DB::select()
			->from('categories')
			->where('module','=',$module)
			->order_by('depth','ASC')
			->order_by('order','ASC')
			->execute()
			->as_array();
	}

	public function get_parents($module,$parent=false)
	{

		$query = DB::select(array('record','id'), array('term','name'))->from('categories')->and_where('module','=',$module)->and_where('depth','=',0);
		if($parent){
			$query->and_where('record','!=',$parent);
		}
		return $query->execute()->as_array();
	}

	public function count_catsubslug($module,$record,$parent,$depth,$slug)
	{

		return DB::select(DB::expr('COUNT(*) AS count'))->from('categories')
			->where('module','=',$module)->and_where('parent','=',$parent)->and_where('depth','=',$depth)->and_where('slug','=',$slug)->and_where('record','!=',$record)
			->execute()->get('count');
	}

	public function count_catslug($module,$record,$depth,$slug)
	{

		return DB::select(DB::expr('COUNT(*) AS count'))->from('categories')
			->where('module','=',$module)->and_where('depth','=',$depth)->and_where('slug','=',$slug)->and_where('record','!=',$record)
			->execute()->get('count');
	}

	public function up_category($id,$parent,$depth,$order,$term,$slug)
	{
		$query = DB::update('categories')->set(array('parent'=>$parent,'depth'=>$depth,'order'=>$order,'term'=>$term,'slug'=>$slug))->where('record','=',$id)->execute();
	}

	public function up_categorycount($id,$count)
	{
		$query = DB::update('categories')->set(array('count'=>$count))->where('record','=',$id)->execute();
	}

	public function count_categoryplus($record,$relation)
	{
		$query = DB::update('relations')->set(array('relation'=>$relation))->where('record','=',$record)->where('relation','=',$relation)->and_where('module','=',3)->execute();
		if(!$query){
			DB::insert('relations', array('record', 'relation', 'module'))->values(array($record,$relation,3))->execute();
		}

	}
	public function count_categoryminus($record,$relation)
	{
		$query = DB::update('relations')->set(array('relation'=>$relation))->where('record','=',$record)->where('relation','=',$relation)->and_where('module','=',3)->execute();
		if(!$query){
			return DB::delete('relations')->where('record','=',$record)->where('relation','=',$relation)->and_where('module','=',3)->execute();
		}

	}
	public function count_categorycount($relation)
	{
		$count = DB::select(DB::expr('COUNT(DISTINCT relations.record) AS count'))->from('categories')->join('relations')->on('categories.record','=','relations.relation')
			->where('categories.record','=',$relation)->and_where('relations.module','=',3)
			->execute()->get('count');

		$this->up_categorycount($relation,$count);
		return $count;

	}
	public function up_subs($module,$parent=false)
	{

		$query = DB::select('record')->from('categories')->and_where('module','=',$module);
		if($parent){
			$query->and_where('parent','=',$parent)->and_where('depth','=',1);
		}
		else{
			$query->and_where('depth','=',0);
		}
		$result = $query->execute()->as_array();
		$i = 1;

		foreach($result as $q){
			DB::update('categories')->set(array('order'=>$i))->where('record','=',$q['record'])->execute();
			$array = array('order'=>$i);
			Riudb::factory()->id($q['record'])->save($array);
			$i++;
		}

	}
}

