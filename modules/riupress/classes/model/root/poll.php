<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Root_Poll extends Model_Core {

	// ---- dodanie usera ----

	public function json_poll()
	{
		return array('id'=>'',  'user'=>'', 'publish'=>'', 'expiry'=>'', 'status'=>'', 'term'=>'', 'answers'=>array(), 'votes'=>array(),  'totalvotes'=>0);
	}

	public function allRecords($offset,$records)
	{
	$records = DB::select('record')->from('polls')
			->group_by('record')->order_by('id', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
	$ar = array();
	foreach($records as $r){
		$ar[] = $r['record'];
	}
	return $ar;
	}


	public function countallRecords()
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('polls')
			->execute()->get('count');
	}

	public function searchallRecords($search,$offset,$records)
	{
	$records =  DB::select('record')->from('polls')
			->where('term','LIKE','%'.$search.'%')
			->group_by('record')->order_by('id', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
	$ar = array();
	foreach($records as $r){
		$ar[] = $r['record'];
	}
	return $ar;
	}


	public function searchcountallRecords($search)
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('polls')
			->where('term','LIKE','%'.$search.'%')
			->execute()->get('count');
	}

	public function add_answer($record,$answer,$votes,$count)
	{

		list($insert_id) = DB::insert('poll_answers', array('poll', 'answer', 'votes', 'order'))->values(array($record,$answer,$votes,$count))->execute();
		return $insert_id;
	}

	public function count_answers($poll)
	{
		$count = DB::select(DB::expr('COUNT(*) AS count'))->from('poll_answers')->where('poll','=',$poll)->execute()->get('count');
		return $count;
	}

	public function delete_answer($id,$poll)
	{
		return DB::delete('poll_answers')->where('id','=',$id)->and_where('poll','=',$poll)->execute();
	}

	public function up_answers($poll)
	{

		$query = DB::select()->from('poll_answers')->where('poll','=',$poll)->order_by('order','ASC')->execute()->as_array();
		$i = 1;
		$array = array();
		foreach($query as $q){
			DB::update('poll_answers')->set(array('order'=>$i))->where('id','=',$q['id'])->execute();
			$array[] = array('id'=>$q['id'],'answer'=>$q['answer'],'votes'=>$q['votes'],'order'=>$i);
			$i++;
		}
		$data = array('answers'=>$array);
			Riudb::factory()->id($poll)->save($data);
	}

	public function up_votes($poll)
	{

		$query = DB::select('user')->from('poll_votes')->where('poll','=',$poll)->order_by('id','ASC')->execute()->as_array();
		$i = 1;
		$array = array();
		foreach($query as $q){
			$array[] = $q['user'];
		}
		$totalvotes = count($array);
		$data = array('votes'=>$array, 'totalvotes'=>$totalvotes);
		Riudb::factory()->id($poll)->save($data);
	}

	public function up_answer($id,$answer)
	{
		return DB::update('poll_answers')->set(array('answer'=>$answer))->where('id','=',$id)->execute();
	}

	public function up_poll($id,$term,$publish,$expiry,$status)
	{
		return DB::update('polls')->set(array('term'=>$term,'publish'=>$publish,'expiry'=>$expiry,'status'=>$status))->where('record','=',$id)->execute();
	}

	public function add_vote($poll,$answer,$user)
	{
		DB::insert('poll_votes', array('poll', 'user', 'answer'))->values(array($poll,$user,$answer))->execute();
		$count = DB::select(DB::expr('COUNT(*) AS count'))->from('poll_votes')->where('poll','=',$poll)->and_where('answer','=',$answer)->execute()->get('count');
		DB::update('poll_answers')->set(array('votes'=>$count))->where('id','=',$answer)->execute();
	}
}

