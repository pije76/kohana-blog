<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Editor_Tag extends Model_Core {

	public function json_tag()
	{
		return array('id'=>'', 'module'=>'', 'term'=>'', 'slug'=>'');
	}

	public function tag_relations($id)
	{
		return DB::select(array('relations.relation','id'), 'tags.term')
				->from('tags')->join('relations')->on('tags.record','=','relations.relation')
				->where('relations.record','=',$id)->and_where('relations.module','=',2)
				->group_by('relations.relation')->order_by('relations.id','ASC')
				->execute()->as_array();
	}

	public function tag_token($user,$token)
	{
		return DB::select(array('record', 'id'), array('term','name'))
				->from('tags')
				->where('term','LIKE','%'.$token.'%')
				->group_by('id')->limit(15)
				->execute()->as_array();
	}

	public function get_all($uid,$offset,$records,$search)
	{
		$results = DB::select(array('relations.relation','id'))
				->from('relations')->join('tags')->on('relations.relation','=','tags.record')
				->where('relations.record','=',$uid);

		if($search){
			$results->and_where('tags.term','LIKE','%'.$search.'%');
		}
		
		$results = $results->group_by('relations.relation')->order_by('relations.id', 'DESC')->limit($records)->offset($offset)
				->execute()->as_array();

		return $this->get_array($results);
	}

	public function count_all($uid,$search)
	{
		$results = DB::select(DB::expr('COUNT(DISTINCT relations.relation) AS count'))
				->from('relations')->join('tags')->on('relations.relation','=','tags.record')
				->where('relations.record','=',$uid);

		if($search){
			$results->and_where('tags.term','LIKE','%'.$search.'%');
		}

		$results = $results->execute()->get('count');
		return $results;
	}

	public function is_slug($slug)
	{
		return DB::select('record')->from('tags')->where('slug','=',$slug)->execute()->current();
	}

	public function up_tag($id,$term,$slug)
	{
		return DB::update('tags')->set(array('term'=>$term, 'slug'=>$slug))->where('record','=',$id)->execute();
	}
}

