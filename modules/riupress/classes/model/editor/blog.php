<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Editor_Blog extends Model_Core {

	// ---- dodanie usera ----

	public function json_blog()
	{
		return array('id'=>'', 'module'=>'4',  'user'=>'', 'publish'=>'', 'updated'=>'', 'status'=>'', 'term'=>'', 'post'=>'', 'icon'=>'', 'is_comment'=>'', 'comments'=>array(), 'count_comment'=>'',  'cats'=>array(), 'tags'=>array(), 'relations'=>array(), 'files'=>array(), 'sum_rating'=>'0','ratings'=>array());;
	}

	public function get_all($uid,$offset,$records,$search)
	{
		$results =  DB::select(array('publishes.record','id'))
			->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.user','=',$uid);

		if($search){
			$results->and_where('terms.term','LIKE','%'.$search.'%');
		}

		$results = $results->group_by('publishes.record')->order_by('publishes.id', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();

		return $this->get_array($results);

	}


	public function count_all($uid,$search)
	{
		$results =  DB::select(DB::expr('COUNT(DISTINCT publishes.record) AS count'))->from('publishes')->join('terms')->on('publishes.record','=','terms.record')
			->where('publishes.user','=',$uid);

		if($search){
			$results->and_where('terms.term','LIKE','%'.$search.'%');
		}

		$results = $results->execute()->get('count');
		return $results;

	}

	public function get_postterm($id)
	{
		return DB::select()
				->from('posts')->join('terms')->on('posts.record','=','terms.record')
				->where('posts.record','=',$id)
				->execute()->current();
	}

	public function token_relations($id)
	{
		return DB::select(array('relations.relation','id'), 'terms.term')
				->from('terms')->join('relations')->on('terms.record','=','relations.relation')
				->where('relations.record','=',$id)->and_where('relations.module','=',4)
				->group_by('relations.relation')->order_by('relations.id','ASC')
				->execute()->as_array();
	}

	public function rel_token($token,$id)
	{
		return DB::select(array('record', 'id'), array('term','name'))
				->from('terms')
				->where('term','LIKE','%'.$token.'%')->and_where('record','!=',$id)
				->group_by('id')->limit(15)
				->execute()->as_array();
	}

	public function get_cats($module)
	{
		return DB::select()
				->from('categories')
				->where('module','=',$module)
				->order_by('depth','ASC')->order_by('order','ASC')
				->execute()->as_array();
	}

}
