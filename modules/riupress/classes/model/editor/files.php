<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Editor_Files extends Model_Core {

 	public function json_file()
	{
	return array('id'=>'', 'parent'=>'', 'module'=>'', 'user'=>'', 'file'=>'', 'mime'=>'', 'width'=>'', 'height'=>'', 'size'=>'');
	}

	public function attachment_add($record, $user, $file, $mime, $width, $height, $size)
	{
	DB::insert('files', array('record', 'user', 'file', 'mime', 'width', 'height', 'size'))
		->values(array($record, $user, $file, $mime, $width, $height, $size))->execute();
	}

	public function attach($id, $record)
	{
	DB::insert('record_files', array('record', 'file'))->values(array($id, $record))->execute();
	}

	public function attachments($id)
	{
	$records = DB::select('file')->from('record_files')->where('record','=',$id)->execute()->as_array();

	$ar = array();
	foreach($records as $r){
		$ar[] = $r['file'];
	}
	return $ar;
	}

/*
* Pobranie wszystkich plików podłączonych pod rekord
*/	
	public function attachment_all($id)
	{
	return DB::select()
			->from('files')
			->where('record','=',$id)
			->execute()
			->as_array();
	}

/*
* Pobieranie pojedynczego zdjecia
*/

	public function attachment_get($id)
	{
	return DB::select()
			->from('files')
			->where('id','=',$id)
			->execute()
			->as_array();
	}

/*
* Usunięcie relacji dla np.: kategorii.
*/	
	public function attachment_del($id)
	{
	return DB::delete('files')
			->where('id','=',$id)
			->execute();
	}

}

