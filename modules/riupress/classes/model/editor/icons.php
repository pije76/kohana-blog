<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Editor_Icons extends Model_Core {

	public function json_icon()
	{
		return array('id'=>'', 'module'=>'', 'term'=>'');
	}

	public function add_icon($record, $term)
	{
		return DB::insert('icons', array('record', 'term'))->values(array($record, $term))->execute();
	}

	public function get_icons($search)
	{
		return DB::select(array('record','rel'), 'term')
			->from('icons')
			->where('term','LIKE','%'.$search.'%')
			->order_by('record','ASC')->limit(30)
			->execute()->as_array();
	}

	public function get_all($uid,$offset,$records,$search)
	{
		$results = DB::select(array('relations.relation','id'))
			->from('relations')->join('icons')->on('relations.relation','=','icons.record')
			->where('relations.record','=',$uid);

		if($search){
			$results->and_where('icons.term','LIKE','%'.$search.'%');
		}
	
		$results = $results->group_by('relations.relation')->order_by('relations.id', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
		return $this->get_array($results);
	}

	public function count_all($uid,$search)
	{
		$results = DB::select(DB::expr('COUNT(*) AS count'))
			->from('relations')->join('icons')->on('relations.relation','=','icons.record')
			->where('relations.record','=',$uid);

		if($search){
			$results->and_where('icons.term','LIKE','%'.$search.'%');
		}
		$results = $results->execute()->get('count');
		return $results;
	}

	public function up_icon($id,$term)
	{
		return DB::update('icons')->set(array('term'=>$term))->where('record','=',$id)->execute();
	}

}

