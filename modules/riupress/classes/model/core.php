<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Core extends Model {

	public function add($module)
	{
		list($insert_id) = DB::insert('records', array('module'))->values(array($module))->execute();		
		return $insert_id;
	}

	public function get_array($records)
	{
		$array = array();
		foreach($records as $r){
			$array[] = $r['id'];
		}
		return $array;
	}

	public function delete($id)
	{
		return DB::delete('records')->where('id','=',$id)->execute();
	}

	public function delete_module($id,$module)
	{
		return DB::delete('records')->where('id','=',$id)->and_where('module','=',$module)->execute();
	}

	public function add_publishes($record, $status, $user, $publish)
	{
		return DB::insert('publishes', array('record', 'status', 'user',  'publish', 'updated'))
			->values(array($record, $status, $user, $publish, $publish))->execute();
	}

	public function add_terms($record, $term)
	{
		return DB::insert('terms', array('record', 'term'))->values(array($record, $term))->execute();
	}

	public function add_posts($record,$post)
	{
		return DB::insert('posts', array('record', 'post'))->values(array($record, $post))->execute();
	}


	public function add_tags($record, $term, $slug)
	{
		DB::insert('tags', array('record', 'term', 'slug'))->values(array($record, $term, $slug))->execute();
	}

	public function get_relations($record, $module)
	{
		$records = DB::select('relation')->from('relations')->where('record','=',$record)->and_where('module','=',$module)->execute()->as_array();
		return $this->get_array($records);
	}

	public function add_relations($record, $relation,$module)
	{
		DB::insert('relations', array('record', 'relation','module'))->values(array($record, $relation,$module))->execute();
	}

	public function count_relations($record, $relation,$module)
	{
		return DB::select(DB::expr('COUNT(*) AS count'))
				->from('relations')->where('record','=',$record)->and_where('relation','=',$relation)->and_where('module','=',$module)->execute()->get('count');
	}

	public function del_relations($record, $relation,$module)
	{
		return DB::delete('relations')->where('record','=',$record)->and_where('relation','=',$relation)->and_where('module','=',$module)->execute();
	}

	public function up_relations($record, $relation,$module)
	{
		return DB::update('relations')->set(array('relation'=>$relation))->where('record','=',$record)->and_where('module','=',$module)->execute();
	}

}

