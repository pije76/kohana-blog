<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Comments extends Model_Core {

	public function json_comment()
	{
		return array('id'=>'', 'parent'=>'', 'module'=>'', 'user'=>'', 'publish'=>'',  'status'=>'', 'ip'=>'', 'agent'=>'', 'nick'=>'', 'post'=>'');
	}


	public function add_comments($record, $parent, $user, $publish, $status, $ip, $agent, $nick, $post)
	{
		DB::insert('comments', array('record', 'parent', 'user', 'publish', 'status', 'ip', 'agent', 'nick', 'post'))
			->values(array($record, $parent, $user, $publish, $status, $ip, $agent, $nick, $post))->execute();
	}

	public function get_comments($parent,$status)
	{

	$records = DB::select('record')->from('comments')->where('parent','=',$parent)->and_where('status','=',$status)->order_by('publish', 'ASC')->execute()->as_array();

		$ar = array();
		foreach($records as $r){
			$ar[] = $r['record'];
		}
		return $ar;
	}

	public function get_comment($record)
	{

	return DB::select()->from('comments')->where('record','=',$record)->execute()->current();

	}

	public function up_comment($record,$status,$nick,$post)
	{
		DB::update('comments')->set(array('status'=>$status, 'nick'=>$nick, 'post'=>$post))->where('record','=',$record)->execute();
	}

	public function up_commentcount($parent)
	{

			$records = DB::select('record')->from('comments')->where('parent','=',$parent)->and_where('status','=',1)->order_by('publish', 'ASC')->execute()->as_array();

			$ar = array();
			foreach($records as $r){
				$ar[] = $r['record'];
			}
			$count = count($ar);

			if($count > 0){
				$true = DB::update('count_comments')->set(array('count'=>$count))->where('record','=',$parent)->execute();
				if(!$true){
				DB::insert('count_comments', array('record', 'is_comment', 'count'))->values(array($parent, 1, $count))->execute();
				}
			}
			else{
				DB::delete('count_comments')->where('record','=',$parent)->execute();
			}

			$parentup = array('count_comment'=>$count,'comments'=>$ar);
			Riudb::factory()->id($parent)->save($parentup);
	}

	public function allRecords($offset,$records)
	{
	$records = DB::select('record')->from('comments')
			->group_by('record')->order_by('id', 'DESC')->limit($records)->offset($offset)
			->execute()->as_array();
	$ar = array();
	foreach($records as $r){
		$ar[] = $r['record'];
	}
	return $ar;
	}


	public function countallRecords()
	{
	return DB::select(DB::expr('COUNT(*) AS count'))->from('comments')
			->execute()->get('count');
	}

}
