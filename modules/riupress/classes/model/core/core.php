<?php defined('SYSPATH') OR die('No direct access allowed.');
class Model_Core_Core extends Model {

	public static function get_session($sid, $time)
	{
		return (bool) DB::select(DB::expr('COUNT(*) AS count'))
			->from('sessions')->where('id','=',$sid)->and_where('expires','>',$time)->execute()->get('count');
	}

	public function del_token($id)
	{
		return DB::delete('tokens')->where('id','=',$id)->execute();
	}

	public function del_tokens($time)
	{
		return DB::delete('tokens')->where('expires','<',$time)->execute();
	}

	public function add_token($token,$user,$time,$agent,$lifetime)
	{
		$agento = Request::$user_agent;
		$agent = sha1($agento);
		$expires = $time + $lifetime;
		list($insert_id) = DB::insert('tokens', array('user', 'agent', 'token', 'created', 'expires'))
			->values(array($user, $agent, $token, $time, $expires))->execute();
				
		return $insert_id;
	}


	public function get_token($token)
	{
		return DB::select()->from('tokens')->where('token','=',$token)->execute()->current();
	}

	public function add_session($expires,$user,$session,$agent,$requested,$ip,$host,$time,$referrer)
	{

		$expires = $time + $expires;
		list($insert_id) = DB::insert('sessions', array('user', 'created', 'expires', 'timestamp', 'ip', 'session', 'agent', 'requested', 'referrer', 'host'))
		->values(array($user, $time, $expires, $time, $ip, $session, $agent, $requested, $referrer, $host))
		->execute();
				
		return $insert_id;
	}
	public function up_session($sid, $time, $requested,$expires)
	{

		$expires = $time + $expires;
		DB::update('sessions')->set(array('expires'=>$expires, 'timestamp'=>$time, 'requested'=>$requested))->where('id','=',$sid)->execute();

	}

	public function get_isuser($user)
	{
		return DB::select('users.user', 'users.pass')->from('users')->join('profiles')->on('users.user','=','profiles.user')
			->where('users.email','=',$user)->and_where('profiles.is_active','=','1')->and_where('profiles.is_ban','=','2')
			->execute()->current();

	}

	public function up_summary($id)
	{
		DB::update('profiles')->set(array('summary_login'=>DB::expr('summary_login+1')))->where('user','=',$id)->execute();
	}

	public function up_token($id,$lifetime)
	{
		$expires = time() + $lifetime;
		return DB::update('tokens')->set(array('expires'=>$expires))->where('id','=',$id)->execute();
	}

	public function del_session($session)
	{
		return DB::delete('sessions')->where('id','=',$session)->execute();
	}

	public function add_member($user)
	{
		return DB::insert('members', array('record', 'role'))->values(array($user, '2'))->execute();
	}

	public function get_members($id)
	{
		return DB::select(array(DB::expr('GROUP_CONCAT(role)'),'roles'))->from('members')->where('record','=',$id)->execute()->current();
	}


}

