<?php defined('SYSPATH') OR die('No direct access allowed.');

class Model_Profil extends Model_Core {

	// ---- dodanie usera ----

	public function json_user()
	{
		return array('id'=>'', 'joined'=>'', 'is_active'=>'', 'is_ban'=>'', 'is_avatar'=>'', 'gender'=>'', 'name'=>'', 'members'=>array());
	}

	// ---- dodanie usera ----

	public function add_user($record,$email,$pass,$key)
	{
		DB::insert('users', array('user','email','pass','active_key'))->values(array($record,$email,$pass,$key))->execute();
	}

	// ---- dodanie profilu ----

	public function add_profil($uid,$is_active,$gender,$name)
	{
		DB::insert('profiles', array('user','joined','is_active','gender','name'))->values(array($uid,DB::expr('UNIX_TIMESTAMP()'),$is_active,$gender,$name))->execute();
	}

	// ---- dodanie roli ----

	public function add_memeber($uid,$role)
	{
		DB::insert('members', array('user','role'))->values(array($uid,$role))->execute();
	}


	// ---- dodanie pełnego profilu ----

	public function add_fullprofile($name,$email,$pass,$gender,$is_active,$members)
	{
		$module = 1;
		$id = $this->add($module);
		$this->add_profil($id,$is_active,$gender,$name);
		$time = time();
		// Tworzenie klucza aktywacyjnego.
		$key = md5($this->get_newpass(10));
		$password = Pass::instance()->passnew($pass);
		$this->add_user($id,$email,$password,$key);
		foreach($members as $m){
		$this->add_memeber($id,$m);
		}
		Riudb::factory()->id($id)->add(TRUE, $this->json_user());
		$data = array('id'=>$id, 'joined'=>$time, 'is_active'=>$is_active, 'is_ban'=>'2', 'is_avatar'=>'2', 'gender'=>$gender, 'name'=>$name, 'members'=>$members);
		Riudb::factory()->id($id)->save($data);
		return array('id'=>$id, 'name'=>$name, 'email'=>$email, 'key'=>$key);

	}
	// ---- generowanie nowego hasła ----

	public function get_newpass($int)
	{
		return $this->random_string($int);
	}

	public function random_string($length){
		$string = md5(time());
		$string = substr($string,0,$length);
		return($string);
	}

	// ---- aktualizacja profil ----

	public function up_profil($uid,$gender,$name)
	{
		DB::update('profiles')->set(array('gender'=>$gender,'name'=>$name))->where('user','=',$uid)->execute();
	}

	// ---- sprawdzanie poprawności hasła ----

	public static function pass_check($oldpass)
	{
		$user = Session::instance()->get('uid');
		$correct = html::chars($oldpass);

		$result = DB::select('pass')->from('users')->where('id','=',$user)->execute()->current();

		$password = $result['pass'];
		$passcheck = Pass::instance()->passcheck($correct,$password);

		return (bool) $passcheck;

	}

	// ---- aktualizacja avatar ----

	public function up_avatar($uid,$avatar)
	{
		DB::update('profiles')->set(array('is_avatar'=>$avatar))->where('user','=',$uid)->execute();
	}

	// ---- aktualizacja haslo ----

	public function up_haslo($uid,$pass)
	{
		DB::update('users')->set(array('pass'=>$pass))->where('id','=',$uid)->execute();
		return DB::select()->from('users')->where('id','=',$uid)->execute()->current();
	}


	// ---- sprawdzenie czy konto zostało aktywoane ----

	public function is_active($uid,$key)
	{
		return DB::select(DB::expr('COUNT(*) AS count'))->from('users')->where('user','=',$uid)->and_where('active_key','=',$key)->execute()->get('count');
	}

	// ---- aktywacja konta ----

	public function up_active($uid)
	{
		return DB::update('profiles')->set(array('is_active'=>'1'))->where('user','=',$uid)->execute();
	}

	// ---- sprawdzenie emaila ----

	public static function email_check($email)
	{
		return !(bool) DB::select(DB::expr('COUNT(*) AS count'))
				->from('users')
				->where('email','=',$email)
				->execute()
				->get('count');
	}

	// ---- dodanie linku do nowego hasła ----

	public function add_recover($user)
	{

		$host = gethostbyaddr($_SERVER['REMOTE_ADDR']);
		$agent = Request::$user_agent;
		$ip = Request::$client_ip;
		$key = md5($this->get_newpass(10));

		DB::insert('recovers', array('user','key','create','update','ip','agent','host','status'))
				->values(array($user['user'],$key,DB::expr('UNIX_TIMESTAMP()'),DB::expr('UNIX_TIMESTAMP()'),$ip,$agent,$host,2))->execute();

		return array('id'=>$user['user'], 'name'=>$user['name'], 'email'=>$user['email'], 'key'=>$key);

	}

	// ---- pobranie danych o użytkowniku na podstawie email ----

	public function get_recover_count($uid)
	{
		return DB::select(DB::expr('COUNT(*) AS count'))->from('recovers')->where('user','=',$uid)
				->and_where('status','=','2')->and_where('create','>',DB::expr('UNIX_TIMESTAMP()-86400'))->execute()->get('count');

	}

	// ---- pobranie danych o użytkowniku na podstawie email ----

	public function get_email_user($email)
	{
		return DB::select('users.user','users.email','profiles.name')->from('users')->join('profiles')->on('users.user','=','profiles.user')
				->where('users.email','=',$email)->and_where('profiles.is_active','=','1')->and_where('profiles.is_ban','=','2')
				->execute()->current();

	}

	// ---- pobranie danych do odzyskania hasła ----

	public function get_recover($uid, $key)
	{
		return DB::select('id','user','status','create')->from('recovers')->where('user','=',$uid)->and_where('key','=',$key)->execute()->current();

	}

	// ---- sprawdzenie emaila ----

	public static function get_recover_mail($email)
	{
	return (bool) DB::select(DB::expr('COUNT(*) AS count'))
			->from('users')
			->where('email','=',$email)
			->execute()
			->get('count');
	}

	// ---- oznacznie linku do zmiany hasła jako przeczytanego ----

	public function up_recover($id)
	{
		return DB::update('recovers')->set(array('update'=>DB::expr('UNIX_TIMESTAMP()'),'status'=>'1'))->where('id','=',$id)->execute();
	}
}
