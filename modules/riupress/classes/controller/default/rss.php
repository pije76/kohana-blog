<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Default_Rss extends Controller {

	public $model,$title;

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('blog');
			$this->title = $this->loader['title'].' - '.__('default_blog_title');
		}
	}
	// listowanie rekordów usera

	public function action_index()
	{

    	header('Content-Type: text/xml; charset=UTF-8', TRUE);
	$records = $this->model->allRecords(0,10);
	$results = Riudb::factory()->get($records)->join('user')->render();

	$rsstitle = $this->title;
	$rssdesc = __('app_description');
	$rsslink = url::base();

	echo View::factory('default/blog/rss')
                    ->bind('rsstitle',$rsstitle)
                    ->bind('rsslink',$rssdesc)
                    ->bind('rssdesc',$rsslink)
                    ->bind('results',$results);
	}

	public function action_tag(){
   	header('Content-Type: text/xml; charset=UTF-8', TRUE);
	$tag = Request::current()->param('tag');
	$id = $this->model->get_tagid($tag);

		if(!empty($tag)){
		$record = Riudb::factory()->get($id)->render();
		$rsstitle = $this->title.' - Tag: '.$record[$id]['term'];
		$rssdesc = __('app_description');
		$rsslink = url::base().'tag/'.$record[$id]['slug'];

		$records = $this->model->allTags($id,0,10);
		$results = Riudb::factory()->get($records)->join('user')->render();
		echo View::factory('default/blog/rss')
		            ->bind('rsstitle',$rsstitle)
		            ->bind('rsslink',$rssdesc)
		            ->bind('rssdesc',$rsslink)
		            ->bind('results',$results);
		}
		else{
		$this->request->redirect('blog');
		}
	}

	public function action_category(){
	$s = Request::current()->param('subcategory');
	$c = Request::current()->param('category');

	$category = DB::select()->from('categories')->where('depth','=',0)->and_where('slug','=',$c)->execute()->current();
	$cat = $category['record'];
	$rsstitle = $this->title.' - '.$category['term'];
	$rsslink = url::base().'blog/'.$category['slug'];

		if(!empty($s)){
		$subcategory = DB::select()->from('categories')->where('depth','=',1)->and_where('parent','=',$cat )->and_where('slug','=',$s)->execute()->current();
		$rsstitle = $rsstitle.' - '.$subcategory['term'];
		$rsslink = url::base().'blog/'.$category['slug'].'/'.$subcategory['slug'];
		$cat = $subcategory['record'];
		}

	$rssdesc = __('app_description');
		
		$records = $this->model->allCats($cat,0,10);
		$results = Riudb::factory()->get($records)->join('user')->render();
		echo View::factory('default/blog/rss')
		            ->bind('rsstitle',$rsstitle)
		            ->bind('rsslink',$rssdesc)
		            ->bind('rssdesc',$rsslink)
		            ->bind('results',$results);

	}
}

