<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Default_Profil extends Controller_Template_Default  {

	public $sidebar;
	public $sidebar_login;

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('profil');
			$this->title = $this->loader['title'].' - '.__('default_profil_title');
			$this->sidebar = View::factory('default/profil/sidebar');
			$this->sidebar_login = View::factory('default/profil/sidebar_login');
		}
	}

	public function action_index()
	{
		$this->_islogin(true);
		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->add('profil',__('default_profil_index_crumb'))->render();

		if($_POST){
		$this->model->add_fullprofile('Katarzyna','kasia@kasia.pl','123123',2,1,array());
		$post = Validation::factory($_POST)
			->rule('name', 'not_empty')
			->rule('name', 'min_length', array(':value', '3'))
			->rule('name', 'max_length', array(':value', '128'));

			if ($post->check()){

				$name = html::chars($_POST['name']);
				$gender = $_POST['gender'];
				$this->model->up_profil($this->uid,$gender,$name);
				Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane')));

				$save = array('gender'=>$gender,'name'=>$name);
				Riudb::factory()->id($this->uid)->save($save);
				$this->request->redirect('profil');
			}
			else{
				$this->errors = $post->errors('profil');
			}
		}

		$this->template->content = View::factory('default/profil/index')
				->bind('sidebar', $this->sidebar_login)
				->bind('errors', $this->errors)
				->bind('flash', $this->flash)
				->bind('data',$this->loader);
	}

	public function action_pass()
	{
		$this->_islogin(true);
		$this->template->title = $this->title.' - '.__('default_profil_pass_title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil/pass',__('default_profil_pass_crumb'))->render();
		$this->sidebar = View::factory('default/profil/sidebar_login');

		if($_POST){

		$post = Validation::factory($_POST)->rule('oldpass', 'not_empty')
			->rule('oldpass', 'min_length', array(':value', '6'))
			->rule('oldpass', 'max_length', array(':value', '32'))
			->rule('oldpass', array($this->model, 'pass_check'))
			->rule('pass', 'not_empty')
			->rule('pass', 'min_length', array(':value', '6'))
			->rule('pass', 'max_length', array(':value', '32'));

			if ($post->check()){
				$pass = html::chars($_POST['pass']);
				$newpassword = Pass::instance()->passnew($pass);

				$result = $this->model->up_haslo($this->uid,$newpassword);
				Session::instance()->set('flash',array(array('success','Hasło zostało zmienione')));
				$this->request->redirect('profil/pass');
			}
			else{
				$this->errors = $post->errors('profil');
			}

		}


		$this->template->content = View::factory('default/profil/pass')
				->bind('sidebar', $this->sidebar_login)
				->bind('errors', $this->errors)
				->bind('flash', $this->flash);


	}


	public function action_avatar()
	{
		$this->_islogin(true);
	
		$default = Request::current()->param('id');
		$this->template->title = $this->title.' - '.__('default_profil_avatar_title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil/avatar',__('default_profil_avatar_crumb'))->render();
		$path = Riudb::factory()->id($this->uid)->getpath();

		// usunięcie dotychczasowego avatara i ustawienie domyślnego
		if($default){
			if($this->loader['me']['is_avatar']==='1' AND $default==='1'){

				if (is_file($path.'/normal.jpg') === TRUE) {
					unlink($path.'/normal.jpg');
				}
				if (is_file($path.'/large.jpg') === TRUE) {
					unlink($path.'/large.jpg');
				}
				if (is_file($path.'/medium.jpg') === TRUE) {
					unlink($path.'/medium.jpg');
				}
				if (is_file($path.'/small.jpg') === TRUE) {
					unlink($path.'/small.jpg');
				}
				if (is_file($path.'/mini.jpg') === TRUE) {
					unlink($path.'/mini.jpg');
				}
				if (is_file($path.'/favicon.jpg') === TRUE) {
					unlink($path.'/favicon.jpg');
				}

				$this->model->up_avatar($this->uid,'0');
				$save = array('is_avatar'=>'0');
				Riudb::factory()->id($this->uid)->save($save);
				Session::instance()->set('flash',array(array('success','Domyślny avatar został ustawiony.')));

			}
			else{
				Session::instance()->set('flash',array(array('success','Zdjęcie zostało usunięte.')));
				if (is_file($path.'/tmp.jpg') === TRUE) {
					unlink($path.'/tmp.jpg');
				}
			}

		
			$this->request->redirect('profil/avatar');
		}
		else{

			if($_POST){

			$post = new Validation(array_merge($_POST, $_FILES));
			$post->rule('file', 'Upload::not_empty')
				->rule('file', 'Upload::type', array(':value',array('jpg')))
				->rule('file', 'Upload::size', array(':value','100KiB'));

				if ($post->check()){

				$filename = Upload::save($_FILES['file'],$filename = 'tmp.jpg',$path,0777);
				$icon = basename($filename);
				$this->gd_data = getimagesize($path.'/tmp.jpg');
				$this->gd_image_out = FALSE;			
				$width = $this->gd_data[0];
				$height = $this->gd_data[1];

					if($width < 150 OR $height < 150){
	
						if (is_file($path.'/tmp.jpg') === TRUE) {
							unlink($path.'/tmp.jpg');
						}

						$this->errors = array('Minimalny rozmiar zdjecia to 150x150 px');
					}
					else{

					$filename = $path.'/tmp.jpg';

					$this->model->up_avatar($this->uid,'1');

					$save = array('is_avatar'=>'1');
					Riudb::factory()->id($this->uid)->save($save);

					Image::factory($filename)
						->resize(150, 150, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'large.jpg','85');
					Image::factory($filename)
						->resize(100, 100, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'medium.jpg','85');
					Image::factory($filename)
						->resize(50, 50, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'small.jpg','85');
					Image::factory($filename)
						->resize(25, 25, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'mini.jpg','85');
					Image::factory($filename)
						->resize(16, 16, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'favicon.jpg','85');

					unlink($path.'/tmp.jpg');
					Session::instance()->set('flash',array(array('success','Avatar został ustawiony.')));
					$this->request->redirect('profil/avatar');

					}
	
				}
				else{
					$this->errors = $post->errors('profil');
				}
			}
			
		}

		$this->template->content = View::factory('default/profil/avatar')
				->bind('sidebar', $this->sidebar_login)
				->bind('data', $this->loader)
				->bind('errors', $this->errors)
				->bind('flash', $this->flash);
	}

	public function action_login()
	{

		$this->_islogin(false);
		$this->template->breadcrumb = $this->breadcrumb->add('profil/login',__('default_profil_login_crumb'),__('default_profil_login_title'))->render();
		$this->template->title = $this->title.' - '.__('default_profil_login_title');
	

		if($_POST){
		$post = Validation::factory($_POST)
			->rule('email', 'not_empty')
			->rule('email', 'min_length', array(':value', '7'))
			->rule('email', 'max_length', array(':value', '128'))
			->rule('email', 'email')
			->rule('pass', 'not_empty')
			->rule('pass', 'min_length', array(':value', '6'))
			->rule('pass', 'max_length', array(':value', '32'));

			if ($post->check())
			{
				$email = html::chars($_POST['email']);
				$pass = html::chars($_POST['pass']);

					if(isset($_POST['token'])){
					$token = 1;
					}
					else{
					$token = false;
					}

					if(Core::instance()->login($email,$pass,$token)){
						$referrer = Request::current()->referrer();
						Request::current()->redirect($referrer);
					}
					else{
						$this->flash = array(array('warning','Podane dane są niepoprawne'));
					}
			}
			else{
				$this->errors = $post->errors('profil');
			}
		}

		$this->template->content = View::factory('default/profil/login')
				->bind('sidebar', $this->sidebar)
				->bind('errors', $this->errors)
				->bind('flash', $this->flash);

	}


	public function action_register()
	{
		$this->_islogin(false);
		$this->template->title = $this->title.' - '.__('default_profil_register_title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil/register',__('default_profil_register_crumb'),__('default_profil_register_title'))->render();
		$this->sidebar = View::factory('default/profil/sidebar');


		if($_POST){

		$post = Validation::factory($_POST)
			->rule('name', 'not_empty')
			->rule('name', 'min_length', array(':value', '5'))
			->rule('name', 'max_length', array(':value', '32'))
			->rule('pass', 'not_empty')
			->rule('pass', 'min_length', array(':value', '6'))
			->rule('pass', 'max_length', array(':value', '32'))
			->rule('email', 'not_empty')
			->rule('email', 'email')
			->rule('email', array($this->model, 'email_check'));

			if ($post->check())
			{
				$email = $_POST['email'];
				$gender = $_POST['gender'];
				$pass = html::chars($_POST['pass']);
				$name = html::chars($_POST['name']);
				Profil::instance()->create($name, $email, $pass, $gender);
				Request::current()->redirect('profil/login');

			}
			else{
				$this->errors = $post->errors('profil');
			}
		}


		$this->template->content = View::factory('default/profil/register')
				->bind('sidebar', $this->sidebar)
				->bind('errors', $this->errors)
				->bind('flash', $this->flash);
		

	}


	public function action_recover()
	{
		$key = Request::current()->param('key');
		$uid = Request::current()->param('id');
		$this->_islogin(false);
		$this->template->title = $this->title.' - '.__('default_profil_recover_title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil/recover',__('default_profil_recover_crumb'),__('default_profil_recover_title'))->render();

		if(!empty($key) AND !empty($uid)){
			$recover = $this->model->get_recover($uid, $key);
			if(!empty($recover)){
				$create = time()-86400;
				if($recover['status']==='1' OR $recover['create']<$create){
            				Session::instance()->set('flash', array(array('warning','Ten adres został już użyty lub minął czas na jego wykorzystanie. Wygeneruj nowy.')));
            				Request::current()->redirect('profil/recover');
				}
				else{
					$data = Riudb::factory()->get($recover['user'])->render();
					if($_POST){

					$post = Validation::factory($_POST)
						->rule('pass', 'not_empty')
						->rule('pass', 'min_length', array(':value', '6'))
						->rule('pass', 'max_length', array(':value', '32'));

						if ($post->check()){
							$pass = html::chars($_POST['pass']);
							$newpassword = Pass::instance()->passnew($pass);

							$this->model->up_haslo($recover['user'],$newpassword);
							$this->model->up_recover($recover['id']);
							Session::instance()->set('flash',array(array('success','Hasło zostało zmienione')));
							$this->request->redirect('profil/login');
						}
						else{
							$this->errors = $post->errors('profil');
						}

					}


					$this->template->content = View::factory('default/profil/recoverpass')
							->bind('data', $data[$recover['user']])
							->bind('sidebar', $this->sidebar)
							->bind('errors', $this->errors)
							->bind('flash', $this->flash);
				}
			}
			else{
            				Session::instance()->set('flash', array(array('warning','Ten adres do ustawienia nowego hasła jest nieprawidłowy. Wygeneruj nowy.')));
            				Request::current()->redirect('profil/recover');
			}
		}
		else{

		if($_POST){
		$post = Validation::factory($_POST)->rule('email', 'not_empty')
			->rule('email', 'email')
            		->rule('email', array($this->model, 'get_recover_mail'));

			if ($post->check())
            		{
            			$email = $_POST['email'];
				Profil::instance()->getrecover($email);
				Request::current()->redirect('profil/recover');
				
           		}
			else{
				$this->errors = $post->errors('profil');
			}

		}

		$this->template->content = View::factory('default/profil/recover')
				->bind('sidebar', $this->sidebar)
				->bind('errors', $this->errors)
				->bind('flash', $this->flash);
		}

	}



	public function action_activate()
	{
		$key = Request::current()->param('key');
		$uid = Request::current()->param('id');
		$this->_islogin(false);
		Profil::instance()->activate($uid, $key);
		Request::current()->redirect('profil/login');
	}

	public function action_logout()
	{
		$this->_islogin(true);
		Core::instance()->logout('profil/login');
	}

}
