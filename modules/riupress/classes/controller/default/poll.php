<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Default_Poll extends Controller_Template_Default {

	public function action_vote(){

	$referrer = Request::current()->referrer();
	if($_POST AND $this->uid>1){
		$post = new Validation($_POST);
		$post = Validation::factory($_POST)
			->rule('answer', 'not_empty');

		if ($post->check() AND !empty($this->id)){
			$model = Model::factory('root_poll');
			$answer = html::chars($_POST['answer']);
			$model->add_vote($this->id,$answer,$this->uid);
			$model->up_answers($this->id);
			$model->up_votes($this->id);
		}
	}
	$this->request->redirect($referrer);
	}
}
