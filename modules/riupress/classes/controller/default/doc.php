<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Default_Doc extends Controller_Template_Default {

	public $sidebar;
	public $docs;
	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('doc');
			$this->docs = $this->model->get_docs();
			$this->title = $this->loader['title'].' - '.__('default_doc_title');
			$this->sidebar = View::factory('default/doc/sidebar')->bind('docs',$this->docs)->bind('uid',$this->uid);
			$this->breadcrumb->add('doc',__('default_doc_crumb'));
		}
	}

	public function action_index(){
	$this->template->title = $this->title;
	$this->template->breadcrumb = $this->breadcrumb->render();
	$this->template->content = Theme::factory("default/doc/index")
			->bind('docs',$this->docs)
			->bind('sidebar',$this->sidebar);

	}

	public function action_doc(){
	$param = Request::current()->param('param');
	$page= Request::current()->param('page');


	$params = explode('.',$param);
	$doc = $params[0];
	$subdoc = $params[1];

	$id = $this->model->get_docid(0,$doc);

	if(!empty($id)){
		$record = Riudb::factory()->get($id)->render();
		$this->breadcrumb->add($record[$id]['order'],$record[$id]['term']);
		$this->title = $this->title.' - '.$record[$id]['term'];
		$data= $record[$id];
		$sub = false;
		if(!empty($subdoc)){
			$subid = $this->model->get_docid(1,$subdoc,$id);
			$subrecord = Riudb::factory()->get($subid)->render();
			$this->breadcrumb->add($record[$id]['order'].'.'.$subrecord[$subid]['order'],$subrecord[$subid]['term'],$subrecord[$subid]['term'],2);
			$this->title = $this->title.' - '.$subrecord[$subid]['term'];
			$sub = $subrecord[$subid];
		}
		if($page > 1){
		$this->title = $this->title.' - strona '.$page;
		$this->breadcrumb->add($page,'Strona '.$page);
		}
		$this->sidebar->bind('active',$record[$id]['id']);
		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$this->template->content = Theme::factory("default/doc/id")
				->bind('docs',$this->docs)
				->bind('page',$page)
				->bind('record',$data)
				->bind('subrecord',$sub)
				->bind('sidebar',$this->sidebar);
	}
	else{
			$this->request->redirect('doc');
	}
	}
}
