<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Default_Index extends Controller_Template_Default {

	public function action_index()
	{

	$this->template->title = $this->title;
	$this->template->breadcrumb = '';
	$this->template->content = Theme::factory('index');
		$this->_rss('',$this->title);
	}
	public function action_icons()
	{

	$this->template->title = $this->title;
	$this->template->breadcrumb = '';
	$this->template->content = Theme::factory('icons');

	}

}
