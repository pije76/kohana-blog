<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Default_Blog extends Controller_Template_Default {

	public $sidebar;

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('blog');
			$cats = $this->model->get_categories(4);
			$this->title = $this->loader['title'].' - '.__('default_blog_title');
			$poll = $this->model->get_poll();
			$this->sidebar = View::factory('default/blog/sidebar')->bind('cats',$cats)->bind('poll',$poll)->bind('uid',$this->uid);
			$this->breadcrumb->add('blog',__('default_blog_crumb'));
		}
	}

	public function action_index(){
	$this->template->title = $this->title;
	$this->template->breadcrumb = $this->breadcrumb->render();
	$list = $this->_listRecords(9);
	$this->template->content = Theme::factory("default/blog/index")
			->bind('list',$list)
			->bind('sidebar',$this->sidebar);

		$this->_rss('',$this->title);
	}

	public function action_tag(){
		$tag = Request::current()->param('tag');
		$page= Request::current()->param('page');
		$id = $this->model->get_tagid($tag);
		if(!empty($tag)){
		$record = Riudb::factory()->get($id)->render();
		$this->title = $this->title.' - Tag: '.$record[$id]['term'];
		$this->breadcrumb->add('tag/'.$record[$id]['slug'],'Tag: '.$record[$id]['term'],'Tag: '.$record[$id]['term'],0);
		if($page>1){
		$this->title = $this->title.' - Strona '.$page;
		$this->breadcrumb->add($page,'Strona '.$page);
		}
		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$list = $this->_listTags(9,$id);
		$this->template->content = Theme::factory("default/blog/index")
				->bind('tag',$record[$id]['term'])
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
		$this->_rss('tag/'.$record[$id]['slug'],$this->title);
		}
		else{
		$this->request->redirect('blog');
		}

	}

	public function action_category()
	{
	$s = Request::current()->param('subcategory');
	$c = Request::current()->param('category');

	$category = DB::select()->from('categories')->where('depth','=',0)->and_where('slug','=',$c)->execute()->current();
	$cat = $category['record'];

	//$cats = $this->categories();
	$this->breadcrumb->add($category['slug'],$category['term']);
	$this->title = $this->title.' - '.$category['term'];
		$subcat = false;
		if(!empty($s)){

		$subcategory = DB::select()->from('categories')->where('depth','=',1)->and_where('parent','=',$cat )->and_where('slug','=',$s)->execute()->current();

		$this->title = $this->title.' - '.$subcategory['term'];
		$this->breadcrumb->add($subcategory['slug'],$subcategory['term']);
		$cat = $subcategory['record'];
		$subcat = $subcategory['term'];
		$this->_rss($category['slug'].DIRECTORY_SEPARATOR.$subcategory['slug'],$this->title);
		}
		else{
		$this->_rss($category['slug'],$this->title);
		}

		if(!empty($this->page) AND $this->page>1){
		$page = 'Strona '.$this->page;
		$this->title = $this->title.' - '.$page;
		$this->breadcrumb->add($this->page,$page);

		}


	$this->template->breadcrumb = $this->breadcrumb->render();
	$this->template->title = $this->title;
	$list = $this->_listCats(9,$cat);
	$this->template->content = Theme::factory("default/blog/index")
			->bind('cat',$category['term'])
			->bind('subcat',$subcat)
			->bind('list',$list)
			->bind('sidebar',$this->sidebar);
	}
	public function action_id(){
		$id = Request::current()->param('id');
		$page= Request::current()->param('page');

		if ($this->request->is_ajax())  
		{
		$this->auto_render = FALSE;
		if(!empty($_POST) AND !empty($_POST['id'])){
		$arr = $this->ajaxid();
			$this->response->headers('cache-control', 'no-cache, must-revalidate');
			$this->response->headers('content-type', 'application/json');
			$this->response->body(json_encode($arr));
		}
		else{
			$arr = array('Brak identyfikatora');
			$this->response->headers('cache-control', 'no-cache, must-revalidate');
			$this->response->headers('content-type', 'application/json');
			$this->response->body(json_encode($arr));
		}

		}
		else{
		$record = Riudb::factory()->get($id)->join('files')->join('tags')->join('relations')->render();
		if($record[$id]['module']===4){
		$this->title = $this->title.' - '.$record[$id]['term'];
		$this->breadcrumb->add($record[$id]['id'],$record[$id]['term'],$record[$id]['term'],0);
		$next = $this->model->get_next($record[$id]['publish']);
		$prev = $this->model->get_prev($record[$id]['publish']);
		if($page > 1){
		$this->title = $this->title.' - strona '.$page;
		$this->breadcrumb->add($page,'Strona '.$page);
		}

		if($_POST){
		$post = new Validation($_POST);
		$post = Validation::factory($_POST)
			->rule('post', 'not_empty')
			->rule('nick', 'not_empty')
			->rule('captchasum', array($this->model, 'captcha_check'));

		if ($post->check() AND !empty($this->id)){
			$model = Model::factory('comments');
			$publish = time();
			$ip = Request::$client_ip;
			$agent = Request::$user_agent;

			$module = '7';
			$record = $model->add($module);
			$parent = $this->id;
			$user = $this->uid;
			$post = html::chars($_POST['post']);
			$postnick = html::chars($_POST['nick']);
			$post_json = Markdown::parse($_POST['post']);
			if($this->uid>1){
			$status = '1';
			$nick = $this->loader['me']['name'];
			}
			else{
			Matchcaptcha::generate();
			$status = '2';
			$nick = $postnick;
			}

			$model->add_comments($record, $parent, $user, $publish, $status, $ip, $agent, $nick, $post);
	
			$pre = $model->json_comment();
			Riudb::factory()->id($record)->add(FALSE, $pre);
			$array = array('id'=>$record, 'parent'=>$parent, 'module'=>$module, 'user'=>$user, 'publish'=>$publish, 'status'=>$status, 'nick'=>$nick, 'post'=>$post_json, 'ip'=>$ip, 'agent'=>$agent);
			Riudb::factory()->id($record)->save($array);

			$newcomments = $model->get_comments($parent ,1);
			$count = count($newcomments);

			if($count > 0){
				$true = DB::update('count_comments')->set(array('count'=>$count))->where('record','=',$parent)->execute();
				if(!$true){
				DB::insert('count_comments', array('record', 'is_comment', 'count'))->values(array($parent, 1, $count))->execute();
				}
			}
			else{
				DB::delete('count_comments')->where('record','=',$parent)->execute();
			}

			$parentup = array('count_comment'=>$count);
			Riudb::factory()->id($parent)->save($parentup);


			DB::update('count_comments')->set(array('count'=>$count))->where('record','=',$parent)->execute();
			$recordup = array('comments'=>$newcomments, 'count_comment'=>$count);
			Riudb::factory()->id($parent)->save($recordup);

			Session::instance()->set('flash',array(array('success','Komentarz został dodany.')));
			$referrer = Request::current()->referrer();
			$this->request->redirect($referrer.'#'.$record);
		}
		else{
			$this->errors = $post->errors('comments');
		}

		}
		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();

		$this->template->content = Theme::factory("default/blog/id")
			->bind('record',$this->loader['id'])
			->bind('next',$next)
			->bind('prev',$prev)
			->bind('page',$page)
			->bind('uid',$this->uid)
			->bind('errors',$this->errors)
			->bind('flash',$this->flash)
			->bind('sidebar',$this->sidebar);
		}
		else{
		$this->request->redirect('');
		}
		}
	}
	public function action_search()
	{

		$this->template->title = $this->title.' - '.__('default_blog_search_title');
		$this->template->breadcrumb = $this->breadcrumb->add('search',__('default_blog_search_crumb'))->render();
		if($_POST){
			$post = Validation::factory($_POST)->rule('search', 'not_empty');
			if ($post->check()){
			$search = html::chars($_POST['search']);
			Session::instance()->set('search',$search);
			}
			else{
			$this->request->redirect('blog');
			}
		}

		$search = Session::instance()->get('search');
		if(!empty($search)){
			$list = $this->_listSearch(9,$search);
			$this->template->content = View::factory("default/blog/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
		}
		else{
			$this->request->redirect('blog');
		}

			
	}

	public function _listTags($onpage = 10,$tag){
	$offset = ($this->page*$onpage)-$onpage;
	$records = $this->model->allTags($tag,$offset,$onpage);
	$count = $this->model->countallTags($tag);
	$results = Riudb::factory()->get($records)->render();
	return $this->_pagination('default/blog/list', $results, $count, $onpage);
	}

	public function _listCats($onpage = 10,$cat){
	$offset = ($this->page*$onpage)-$onpage;
	$records = $this->model->allCats($cat,$offset,$onpage);
	$count = $this->model->countallCats($cat);
	$results = Riudb::factory()->get($records)->render();
	return $this->_pagination('default/blog/list', $results, $count, $onpage);
	}

	public function _listRecords($onpage = 10){
	$offset = ($this->page*$onpage)-$onpage;
	$records = $this->model->allRecords($offset,$onpage);
	$count = $this->model->countallRecords();
	$results = Riudb::factory()->get($records)->render();
	return $this->_pagination('default/blog/list', $results, $count, $onpage);
	}

	public function _listSearch($onpage = 10,$search){
	$offset = ($this->page*$onpage)-$onpage;
	$records = $this->model->searchallRecords($search,$offset,$onpage);
	$count = $this->model->searchcountallRecords($search);
	$results = Riudb::factory()->get($records)->render();
	return $this->_pagination('default/blog/list', $results, $count, $onpage);
	}

	public function ajaxid()
	{
	$uid = Session::instance()->get('uid');
	$parent = Request::current()->param('id');
	$post = new Validation($_POST);
	$post = Validation::factory($_POST)->rule('id', 'not_empty');

	if ($post->check() AND !empty($parent)){

	$tablica = explode('-',$_POST['id']);
	$record = $tablica[0];
	$status = $tablica[1];
	$recorddata = Riudb::factory()->get($record)->render();

	$ratings = $recorddata[$record]['ratings'];
	$sum = $recorddata[$record]['sum_rating'];

	$ratings[] = $uid;
	$sum = $sum+$status;
		
	DB::insert('ratings', array('record', 'user', 'rating'))->values(array($record, $uid, $status))->execute();
			$count = count($ratings);

			$average = ($sum/$count);
			$average = round($average, 2);

			if($count > 0){
				$true = DB::update('count_ratings')->set(array('count'=>$count,'sum'=>$sum,'average'=>$average))
					->where('record','=',$record)->execute();
				if(!$true){
				DB::insert('count_ratings', array('record', 'count', 'sum', 'average'))->values(array($record, $count, $sum, $average))->execute();
				}
			}
			else{
				DB::delete('count_ratings')->where('record','=',$record)->execute();
			}

			$recordup = array('ratings'=>$ratings, 'sum_rating'=>$sum);
			Riudb::factory()->id($record)->save($recordup);

			$json = array('success' => 1, 'width' => $average, 'sum' => $sum, 'count' => $count, 'idrecord' => $record);
			return $json;

	}
	}
}
