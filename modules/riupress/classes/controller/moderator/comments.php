<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Moderator_Comments extends Controller_Root_Root{

	// Listowanie wszystkich userów.
	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('comments');
			$this->breadcrumb->add('blog',__('moderator_comments_crumb'));
			$this->title = $this->title.' - '.__('moderator_comments_title');

			$styles = array(
				$this->media->uri(array('file' => 'css/editor.css'))  => 'screen',
			);

			$scripts = array(
				$this->media->uri(array('file' => 'js/backend/comments.js')),
			);

			$this->template->scripts = array_merge( $scripts, $this->template->scripts);
			$this->template->styles = array_merge( $styles, $this->template->styles);

		}
	}

	public function action_index(){

		if ($this->request->is_ajax())  
		{
			$this->auto_render = FALSE;
			if(!empty($_POST) AND !empty($_POST['rel'])){

			$model = Model::factory('comments');
			$uid = Session::instance()->get('uid');

			$post = new Validation($_POST);
			$post = Validation::factory($_POST)->rule('rel', 'not_empty');
			if ($post->check()){
			$tablica = explode('-',$_POST['rel']);
			$record = $tablica[0];
			$status = $tablica[1];
			$parent = $tablica[2];
			
			switch ($status) {
			    case 1:
					$statustext = 'aktywny';
					$style = 'label label-success';
				break;
			    case 2:
					$statustext = 'nowy / niezatwierdzony';
					$style = 'label label-warning';
				break;
			    case 3:
					$statustext = 'zbanownay';
					$style = 'label label-important';
				break;
			}


			DB::update('comments')->set(array('status'=>$status))->where('record','=',$record)->execute();
			$model->up_commentcount($parent);

			$array = array('status'=>$status);
			Riudb::factory()->id($record)->save($array);

			$json = array('success' => 1, 'id' => $record, 'style' => $style, 'status' => $statustext);
				$this->response->headers('cache-control', 'no-cache, must-revalidate');
				$this->response->headers('content-type', 'application/json');
				$this->response->body(json_encode($json));
			}
			}
		}
		else{

		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$list = $this->_listRecords(9);
		$this->template->content = View::factory("moderator/comments/index")
				->bind('list',$list);
		}
	}

	
	public function _listRecords($onpage = 10){
	$offset = ($this->page*$onpage)-$onpage;
	$records = $this->model->allRecords($offset,$onpage);
	$count = $this->model->countallRecords();
	$results = Riudb::factory()->get($records)->join('parent')->join('user')->render();
	return $this->_pagination('moderator/comments/list', $results, $count, $onpage);
	}

	public function action_delete()
	{
		$id = Request::current()->param('id');
		$param = Request::current()->param('param');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('moderator_comments_delete_title').': '.$record[$id]['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('moderator_comments_delete_crumb').': '.$record[$id]['id'])->render();
		if($param==='del'){

			// właściwe usuwanie rekordu
			DB::delete('records')->where('id','=',$id)->and_where('module','=',7)->execute();
			Riudb::factory()->id($id)->delfile()->deldir();
			$this->model->up_commentcount($record[$id]['parent']);
			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{
			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('moderator/comments/delete')->bind('record', $record[$id])->bind('user', $this->uid)->bind('referrer', $referrer);
		}
	}


	public function action_edit()
	{
		$id = Request::current()->param('id');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('moderator_comments_edit_title').': '.$record[$id]['id'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('moderator_comments_edit_crumb').': '.$record[$id]['id'])->render();

		if($_POST){
			$post = Validation::factory($_POST)->rule('post', 'not_empty');
			if ($post->check()){

			$post = html::chars($_POST['post']);
			$post_json = Markdown::parse($_POST['post']);
			$status = html::chars($_POST['status']);
			
			$this->model->up_comment($id,$status,$record[$id]['nick'],$post);
			$this->model->up_commentcount($record[$id]['parent']);
			$array = array('status'=>$status,  'post'=>$post_json);
					Riudb::factory()->id($id)->save($array);
			Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
			$this->request->redirect($this->request->referrer());
			}
			else{
			$this->errors = $post->errors('root');
			}
		}

		$comment = $this->model->get_comment($id);
		$this->template->content = View::factory('moderator/comments/edit')
					->bind('comment', $comment)->bind('user', $this->uid)->bind('errors',$this->errors)->bind('flash',$this->flash);
		

	}
}
