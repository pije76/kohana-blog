<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Template_Default extends Controller_Template {

	// zmienne wykorzystywane w systemie

	// szablon
	public $template;

	// media: css, js, 
	public $media;

	// modele
	public $model;

	// identyfikator użytkownika
	public $uid;

	// identyfikator rekordu
	public $id;

	// parametr pomocniczy
	public $param;

	// nazwa aktywnego theme
	public $theme;

	// strona stronnicowania wyników
	public $page;

	// tytuł dokumentu html
	public $title;

	// okruszki
	public $breadcrumb;

	// wiadomości flash
	public $flash = array();

	// wiadomości z formularzy
	public $errors = array();

	// bierzący katalog aplikacji
	public $directory; 

	// bieżący kontroler
	public $controller; 

	// role userów
	public $roles;

	// menu zależne od kontekstu
	public $menus;

	public function before()
	{

		if ( ! $this->request->is_ajax() )
		{
			parent::before();

			if ($this->auto_render)
			{

				$this->roles = Kohana::$config->load('roles');
				$this->menus = Kohana::$config->load('menu');
				$request = Request::current();
				$this->directory = $request->directory();
				$this->controller = $request->controller();	
				$this->page = $request->param('page');
				$this->id = $request->param('id');
				$this->param = $request->param('param');

				if(empty($this->page)){
				$this->page = 1;
				}

				if(empty($this->loader['me']['id'])){
					if($this->controller !== 'setup'){
						$request->redirect('setup');
					}
				}
				else{
					if($this->controller === 'setup'){
						$request->redirect('');
					}
				}

				$session = Session::instance();
				$this->uid = $session->get('uid');
				$this->theme = $session->get('theme');
				$this->flash = $session->get_once('flash');

				$this->media = Route::get('api/index/media');
				$this->themes = Route::get('api/index/themes');

				$this->template = Theme::factory('template/index');

				$this->title = $this->loader['title'];
				$this->template->title = '';
				$this->template->metas = array();
				$this->template->links = array();
				$this->template->styles = array();
				$this->template->scripts = array();
				$this->template->codes = array();

				$this->breadcrumb = Breadcrumb::factory();

				$subtitle = $this->roles[$this->directory]['dirsubtitle'];
				$this->template->header = Theme::factory('template/header')
					->bind('data', $this->loader)->bind('roles', $this->roles)->bind('menu', $this->menus[$this->directory])->bind('subtitle', $subtitle);
				$this->template->breadcrumb = '';
				$this->template->leftbar = '';
				$this->template->content = '';
				$this->template->sidebar = '';
				$this->template->footer = Theme::factory('template/footer')->bind('data', $this->loader);

				if(!empty($this->id)){
					$newdata = Riudb::factory()->get($this->id)
						->join('user')->join('comments','user')
						->join('relations')->join('tags')->join('cats')->render();

					if(!empty($newdata[$this->id])){
						$this->loader = array_merge($this->loader, array('id'=>$newdata[$this->id]));
					}
					else{
						$this->request->redirect('');
					}
				}
			}
		}
	}

	public function after()
	{
	if ( ! $this->request->is_ajax() )
	{
		if ($this->auto_render)
		{

			$metas = array(
				'Content-Type' => array(
					'http-equiv'	=> 'Content-Type',
					'content'	=> 'text/html; charset=utf-8',
				),
				'robots' => array(
					'name'		=> 'robots',
					'content'	=> 'ALL',
				),
				'generator' => array(
					'name'		=> 'generator',
					'content'	=> 'Riupress',
				),
				'viewport' => array(
					'name'		=> 'viewport',
					'content'	=> 'width=device-width,initial-scale=1',
				),
				'keywords' => array(
					'name'		=> 'keywords',
					'content'	=> __('app_keywords'),
				),
				'description' => array(
					'name'		=> 'description',
					'content'	=> __('app_description'),
				),
			);

			$links = array(
				'shortcut icon' => array(
					'rel'	=> 'shortcut icon',
					'href'	=> url::base().$this->themes->uri(array('file' => $this->theme.'/ico/favicon.ico')),
				),
				'shortcut icon2' => array(
					'rel'	=> 'shortcut icon',
					'href'	=> url::base().$this->themes->uri(array('file' => $this->theme.'/ico/favicon.ico')),
					'type'	=> 'image/x-icon',
				),
				'index' => array(
					'rel'	=> 'index',
					'href'	=> url::base(),
					'title'	=> $this->title,
				),
				'canonical' => array(
					'rel'	=> 'canonical',
					'href'	=> url::base().Request::current()->uri(),
					'title'	=> $this->title,
				),
			);

			if($this->loader['role']==='1'){
			$files = Kohana::load(THEMEPATH.$this->theme.'/media.php');
			}
			else{
			$files = Kohana::load(THEMEPATH.$this->theme.'/backend.php');
			}
			$styles = Theme::set_themestyle($files['styles'],$this->themes,$this->theme);
			$scripts = Theme::set_themescript($files['scripts'],$this->themes,$this->theme);


			$this->template->metas = array_merge( $metas, $this->template->metas);
			$this->template->links = array_merge( $links, $this->template->links);

			$this->template->scripts = array_merge( $scripts, $this->template->scripts);
			$this->template->styles = array_merge( $styles, $this->template->styles);

		}
		parent::after();

		$this->response->body($this->template);

		}
	}

	public function _pagination($view, $results, $count, $onpage, $paginationview = 'pagination/default'){

		$pagination = Pagination::factory(array(
				'current_page'   => array('source' => 'route', 'key' => 'page'),
				'total_items'    => $count,
				'view'           => $paginationview,
				'items_per_page' => $onpage,
			));

		$page_links = $pagination->render();

		$list = Theme::factory($view)
				->bind('uid', $this->uid)
				->bind('results',$results)
				->bind('count', $count)
				->bind('pagination', $page_links)->render();

		return $list;
	}

	public function _rss($slug, $title){

			$links = array(
				'alternate' => array(
					'rel'	=> 'alternate',
					'href'	=> url::base().'rss/'.$slug,
					'title'	=> $title,
					'type'	=> 'application/rss+xml',
				),
			);
			$this->template->links = array_merge( $this->template->links, $links );
	}


	public function _islogin($islogin)
	{
		$user = Session::instance()->get('uid');

		if($user > 1){
			if($islogin){
				return true;
			}
			else{
				$this->request->redirect('');
			}	
		}
		else{
			if($islogin){
				$this->request->redirect('profil/login');
			}
			else{
				return true;
			}

		}
	}

	public function _ispermission(){

		if(!in_array($this->loader['role'],$this->loader['me']['members'])){
			$this->request->redirect('');
		}
	}

}

