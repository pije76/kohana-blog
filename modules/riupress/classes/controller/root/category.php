<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Root_Category extends Controller_Root_Root {

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();

			$this->model = Model::factory('root_category');
			$this->breadcrumb->add('profil',__('root_category_crumb'));
			$this->title = $this->title.' - '.__('root_category_title');
			$this->sidebar = View::factory('root/category/sidebar');
			$styles = array(
				$this->media->uri(array('file' => 'css/editor.css'))  => 'screen',
			);

			$this->template->styles = array_merge( $styles, $this->template->styles);
		}
	}

	public function action_index(){

		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$param = Request::current()->param('param');

		if(!empty($param)){

		$tablica = explode('-', $param);
		$depth = $tablica[0];
		$record = $tablica[1];
		$order = $tablica[2];
		$neworder = $order+1;
		if($depth>0){
		$upnew = DB::select('record','parent')->from('categories')->where('record','=',$record)->execute()->current();
		$downnew = DB::select('record')->from('categories')->where('depth','=',$depth)->and_where('parent','=',$upnew['parent'])->and_where('order','=',$order)->execute()->current();
		}
		else{
		$upnew = DB::select('record')->from('categories')->where('record','=',$record)->execute()->current();
		$downnew = DB::select('record')->from('categories')->where('depth','=',$depth)->and_where('order','=',$order)->execute()->current();
		}
		DB::update('categories')->set(array('order'=>$order))->where('record','=',$upnew['record'])->execute();
		DB::update('categories')->set(array('order'=>$neworder))->where('record','=',$downnew['record'])->execute();


		$data = array('order'=>$order);
		Riudb::factory()->id($upnew['record'])->save($data);

		$data = array('order'=>$neworder);
		Riudb::factory()->id($downnew['record'])->save($data);

		$this->request->redirect('root/category');

		}

		$cats = $this->model->get_all(4);
		$list = View::factory("root/category/list")->bind('cats',$cats)->bind('param',$depth);
		$this->template->content = View::factory("root/category/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
	}

	public function action_add(){
	
		$this->template->title = $this->title.' - '.__('root_category_add_title');
		$this->template->breadcrumb = $this->breadcrumb->add('search',__('root_category_add_crumb'))->render();

		if($_POST){
			$post = Validation::factory($_POST)->rule('term', 'not_empty')
			->rule('term', 'min_length', array(':value', '3'))
			->rule('term', 'max_length', array(':value', '32'));
			if ($post->check()){

				$term = html::chars($_POST['term']);
				$slug = url::title($term, $separator = '-', $ascii_only = TRUE);
				$cat = html::chars($_POST['cat']);

				$module = 3;
			
				$record = $this->model->add($module);

				if($cat>1){
					$parent = $cat;
					$depth = 1;
				}
				else{
					$parent = $record;
					$depth = 0;
				}

				$count = $this->model->add_category($record,$parent,$depth,4,$term,$slug);


				$pre = $this->model->json_category();

			Riudb::factory()->id($record)->add(FALSE, $pre);
			$array = array('id'=>$record, 'parent'=>$parent, 'module'=>$module, 'term'=>$term, 'slug'=>$slug, 'depth'=>$depth, 'order'=>$count);
			Riudb::factory()->id($record)->save($array);


			Session::instance()->set('flash',array(array('success','Kategoria została dodana.')));
			$this->request->redirect('root/category/edit/'.$record);
			}
		}
		$parents = $this->model->get_parents(4);
		$this->template->content = View::factory('root/category/add')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('parents', $parents)->bind('sidebar',$this->sidebar);
		

	}

	public function action_edit(){

		$id = Request::current()->param('id');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('root_category_edit_title').': '.$record[$id]['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_category_edit_crumb').': '.$record[$id]['term'])->render();
		$module = 4;
		if($_POST){

			$post = Validation::factory($_POST)->rule('term', 'not_empty')
				->rule('term', 'min_length', array(':value', '2'))
				->rule('term', 'max_length', array(':value', '64'));

			if ($post->check()){
				$term = html::chars($_POST['term']);
				$slug = url::title($term, $separator = '-', $ascii_only = TRUE);
				$cat = html::chars($_POST['cat']);

				if($cat>1){

					// sprawdznie czy inne subkategoria z `cat` ma już taką nazwę
					$countc = $this->model->count_catsubslug($module,$id,$cat,1,$slug);

					// zmiana na subkategorie
			
					if($cat === $record[$id]['parent']){
					// kategoria główna nie zostaje zmieniona

						if(!empty($countc)){
							 // bład = inn subkategoria już tak sie nazywa
							$this->errors = array('Inna subkategoria już tak sie nazywa.');
						}
						else{
							// nazwa zostaje zmieniona
							$this->model->up_category($id,$record[$id]['parent'],$record[$id]['depth'],$record[$id]['order'],$term,$slug);
							$array = array('term'=>$term, 'slug'=>$slug);
							Riudb::factory()->id($id)->save($array);
							Session::instance()->set('flash',array(array('success','Nazwa kategorii została zmieniona.')));
							$this->request->redirect('root/category/edit/'.$id);
						}
					}
					else{
					// kategoria główna jest zmieniona
						if(!empty($countc)){
							 // bład = inn subkategoria już tak sie nazywa
							$this->errors = array('Zmiany nie zostały zapisane, ponieważ inna subkategoria wybranej kategorii już tak sie nazywa.');
						}
						else{
							// nazwa i kategoria nadrzędna zostaje zmieniona
						
							$order = $this->model->count_categories($module,$cat);
							$order++;
							$depth = 1;
							$this->model->up_category($id,$cat,$depth,$order,$term,$slug);
							$array = array('parent'=>$cat, 'term'=>$term, 'slug'=>$slug, 'order'=>$order, 'depth'=>$depth);
							Riudb::factory()->id($id)->save($array);
							if($record[$id]['depth'] ==='1'){
							$this->model->up_subs($module,$record[$id]['parent']);
							}
							else{
							$this->model->up_subs($module);
							}
							Session::instance()->set('flash',array(array('success','Nazwa i kategoria nadrzędna kategorii zostały zmienione.')));
							$this->request->redirect('root/category/edit/'.$id);
						
						}
					}
			
				}
				else{
				// zmiana na kategorie główną

					// sprawdznie czy inne kategoria z `cat` ma już taką nazwę
					$countc = $this->model->count_catslug($module,$id,0,$slug);

					if($record[$id]['id'] === $record[$id]['parent']){

					// przed zmianą była kategorią główną
						if(!empty($countc)){
							 // bład = inn kategoria już tak sie nazywa
							$this->errors = array('Inna kategoria już tak sie nazywa.');
						}
						else{
							// nazwa zostaje zmieniona
							$this->model->up_category($id,$record[$id]['parent'],$record[$id]['depth'],$record[$id]['order'],$term,$slug);
							$array = array('term'=>$term, 'slug'=>$slug);
							Riudb::factory()->id($id)->save($array);
							Session::instance()->set('flash',array(array('success','Nazwa kategorii została zmieniona.')));
							$this->request->redirect('root/category/edit/'.$id);
						}
					}
					else{
					// przed zmianą była subkategorią

					// przed zmianą była kategorią główną
						if(!empty($countc)){
							 // bład = inn kategoria już tak sie nazywa
							$this->errors = array('Zmiany nie zostały zapisane ponieważ inna jest już kategoria główna o takiej nazwie.');
						}
						else{
							// nazwa zostaje zmieniona i subkategoria staje się kategorią główną
							$order = $this->model->count_categories($module);
							$order++;
							$this->model->up_category($id,$id,0,$order,$term,$slug);
							$array = array('parent'=>$id, 'term'=>$term, 'slug'=>$slug, 'order'=>$order);
							Riudb::factory()->id($id)->save($array);
							$this->model->up_subs($module,$record[$id]['parent']);
							Session::instance()->set('flash',array(array('success','Nazwa i poziom kategorii zostały zmieniony.')));
							$this->request->redirect('root/category/edit/'.$id);
						}
					}
				}

			}
			else{
			$this->errors = array('Podane dane są nieporawne');
			}
		}	

		$subs = $this->model->count_categories($module,$id);
		$parents = $this->model->get_parents($module,$id);
		$this->template->content = View::factory('root/category/edit')
				->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('record', $record[$id])->bind('parents', $parents)->bind('subs', $subs)->bind('sidebar',$this->sidebar);
	
	}
	public function action_delete()
	{
		$id = Request::current()->param('id');
		$param = Request::current()->param('param');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('root_category_delete_title').': '.$record[$id]['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_category_delete_crumb').': '.$record[$id]['term'])->render();
		$module = 4;
		if($param==='del'){
			// właściwe usuwanie rekordu
			DB::delete('records')->where('id','=',$id)->and_where('module','=',3)->execute();
			Riudb::factory()->id($id)->delfile()->deldir();

			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{
			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$subs = $this->model->count_categories($module,$id);
			$this->template->content = View::factory('root/category/delete')->bind('record', $record[$id])->bind('user', $this->uid)->bind('subs', $subs)->bind('referrer', $referrer);
		}
	}

}

