<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Root_Root extends Controller_Template_Default {

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->breadcrumb->add('root',__('root_index_crumb'));
			$this->_ispermission();
		}
	}
}

