<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Root_Settings extends Controller_Root_Root {

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();

			$this->breadcrumb->add('settings',__('root_settings_crumb'));
			$this->title = $this->title.' - '.__('root_settings_title');
			$this->sidebar = View::factory('root/settings/sidebar');
		}
	}

	public function action_index()
	{
		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$loadbase = Kohana::$config->load('core');
		if($_POST){
	
			$post = Validation::factory($_POST)
				->rule('apptitle', 'not_empty')
				->rule('appadmin', 'not_empty')
				->rule('appmail', 'not_empty')
				->rule('appmail', 'email');

				if ($post->check()){
				$apptitle = html::chars($_POST['apptitle']);
				$appadmin = html::chars($_POST['appadmin']);
				$appmail = html::chars($_POST['appmail']);

				$file = fopen(APPPATH.'config/core.php', 'w+');
	$file_content = '<?php defined(\'SYSPATH\') or die(\'No direct access allowed.\');

	return array
	(
		\'apptitle\' => \''.$apptitle.'\',
		\'expires\' => 3600,
		\'gmt\' => 3600,
		\'lifetime\' => 1209600,
		\'active\' => 1800,
		\'session_key\' => \'app_user\',
		\'cookie_key\' => \'app_auto_login\',
		\'admin\' => \''.$appadmin.'\',
		\'mail\' => \''.$appmail.'\',
		\'theme\' => \'default\',
	);
	';
			
				fwrite($file, $file_content);
				Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
				$this->request->redirect('root/settings');

				}
				else{

				$this->errors = array('Musisz prawidłowo podać tytuł strony, nazwę i email zwrotny dla poczty.');

				}
		}

		$this->template->content = View::factory('root/settings/index')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('base',$loadbase)->bind('sidebar',$this->sidebar);
	
	
	}


	public function action_database()
	{
		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$loadbase = Kohana::$config->load('database');
		if($_POST){
	
			$post = Validation::factory($_POST)
				->rule('hostname', 'not_empty')
				->rule('database', 'not_empty')
				->rule('username', 'not_empty')
				->rule('password', 'not_empty');

				if ($post->check()){
				$apptitle = html::chars($_POST['apptitle']);
				$admin = html::chars($_POST['admin']);
				$mail = html::chars($_POST['mail']);

				$file = fopen(APPPATH.'config/database.php', 'w+');
	$file_content = '<?php defined(\'SYSPATH\') or die(\'No direct access allowed.\');

	return array
	(
	\'default\' => array
		(
		\'type\' => \'mysql\',
		\'connection\' => array(
			\'hostname\'=>\''.$hostname.'\', 
			\'database\'=>\''.$database.'\', 
			\'username\'=>\''.$username.'\', 
			\'password\'=>\''.$password.'\', 
			\'persistent\'=> FALSE
		),
		\'table_prefix\' => \'\',
		\'charset\'      => \'utf8\',
		\'caching\'      => TRUE,
		\'profiling\'    => TRUE
		)
	);
	';
			
				fwrite($file, $file_content);
				Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
				$this->request->redirect('root/settings');

				}
				else{

				$this->errors = array('Musisz prawidłowo dane potrzebne do połączenia z bazą');

				}
		}

		$this->template->content = View::factory('root/settings/database')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('base',$loadbase)->bind('sidebar',$this->sidebar);
	
	
	}
	public function action_sender()
	{

		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$loadbase = Kohana::$config->load('sender');

		if($_POST){
	
			$post = Validation::factory($_POST)
				->rule('hostname', 'not_empty')
				->rule('password', 'not_empty')
				->rule('port', 'not_empty')
				->rule('username', 'not_empty')
				->rule('username', 'email');

				if ($post->check()){
				$hostname = html::chars($_POST['hostname']);
				$username = html::chars($_POST['username']);
				$password = html::chars($_POST['password']);
				$port = html::chars($_POST['port']);

				$file = fopen(APPPATH.'config/sender.php', 'w+');
	$file_content = '<?php defined(\'SYSPATH\') or die(\'No direct access allowed.\');

	return array
	(
		\'driver\' => \'smtp\',
		\'options\' => array(\'hostname\'=>\''.$hostname.'\', \'username\'=>\''.$username.'\', \'password\'=>\''.$password.'\', \'port\'=>\''.$port.'\')
	);
	';
			
				fwrite($file, $file_content);
				Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
				$this->request->redirect('root/settings/sender');

				}
				else{

				$this->errors = array('Musisz prawidłowo podać wszystkie dane.');

				}
		}
		$this->template->content = View::factory('root/settings/sender')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('base',$loadbase)->bind('sidebar',$this->sidebar);
	
	
	}
}

