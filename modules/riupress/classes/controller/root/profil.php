<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Root_Profil extends Controller_Root_Root  {

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('root_profil');
			$this->breadcrumb->add('profil',__('root_profil_crumb'));
			$this->title = $this->title.' - '.__('root_profil_title');
			$this->sidebar = View::factory('root/profil/sidebar');

		}
	}

	public function action_index(){
	$this->template->title = $this->title;
	$this->template->breadcrumb = $this->breadcrumb->render();
	$list = $this->_listUsers('root/profil/list',9);
	$this->template->content = View::factory("root/profil/index")
			->bind('list',$list)
			->bind('sidebar',$this->sidebar);

	}

	public function action_search()
	{

	$this->template->title = $this->title.' - '.__('root_profil_search_title');
	$this->template->breadcrumb = $this->breadcrumb->add('search',__('root_profil_search_crumb'))->render();

		if($_POST){
			$post = Validation::factory($_POST)->rule('search', 'not_empty');
			if ($post->check()){
			$search = $_POST['search'];
			Session::instance()->set('search',$search);

			}
			else{
			$this->request->redirect('root/profil');
			}
		}

			#Affects errors for further display
		$search = Session::instance()->get('search');
		if(!empty($search)){
		$list = $this->_listSearch('root/profil/list',9,$search);
		$this->template->content = View::factory("root/profil/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
		}
		else{
		$this->request->redirect('root/profil');
		}
	}
	public function action_email()
	{

	$this->template->title = $this->title.' - '.__('root_profil_email_title');
	$this->template->breadcrumb = $this->breadcrumb->add('search',__('root_profil_email_crumb'))->render();

		
		if($_POST){
			$post = Validation::factory($_POST)->rule('email', 'not_empty');
			if ($post->check()){
			$search = $_POST['email'];
			Session::instance()->set('email',$search);

			}
			else{
			$this->request->redirect('root/profil');
			}
		}

			#Affects errors for further display
		$search = Session::instance()->get('email');
		if(!empty($search)){
		$list = $this->_listEmail('root/profil/list',9,$search);
		$this->template->content = View::factory("root/profil/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
		}
		else{
		$this->request->redirect('root/profil');
		}
	}
	public function _listUsers($view, $onpage = 10){
	$offset = ($this->page*$onpage)-$onpage;
	$results = $this->model->allprofiles($offset,$onpage);
	$count = $this->model->countprofiles();
	return $this->_pagination($view, $results, $count, $onpage);
	}

	public function _listSearch($view, $onpage = 10,$search){
	$offset = ($this->page*$onpage)-$onpage;
	$results = $this->model->searchallprofiles($search,$offset,$onpage);
	$count = $this->model->searchcountprofiles($search);
	return $this->_pagination($view, $results, $count, $onpage);
	}

	public function _listEmail($view, $onpage = 10,$search){
	$offset = ($this->page*$onpage)-$onpage;
	$results = $this->model->emailallprofiles($search,$offset,$onpage);
	$count = $this->model->emailcountprofiles($search);
	return $this->_pagination($view, $results, $count, $onpage);
	}


	public function action_edit()
	{
		$ddd = array('members'=>array(""));
			Riudb::factory()->id(1)->save($ddd);
	$id = Request::current()->param('id');
	$page = Request::current()->param('page');
	$results = $this->model->idprofile($id);
		$this->template->title = $this->title.' - '.__('root_profil_edit_title').': '.$record[$id]['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('edit',__('root_profil_edit_crumb').': '.$results['name'])->render();

	$member = Riudb::factory()->get($id)->render();
	$members = $this->model->allmembers($id);
	$roles = $this->model->allmemberroles($member[$id]['members']);

	if($page > 1){
		if($_POST){

			$post = Validation::factory($_POST)->rule('role', 'not_empty');

			if ($post->check()){

				$role = html::chars($_POST['role']);
				$count = $this->model->isusermember($id,$role);

				if(empty($count)){

					$this->model->addrolemember($id,$role);
					$result = $this->model->memberusers($id);
					$resulte = json_decode(json_encode(explode(',',$result['roles'])));
					$array = array('members'=>$resulte);
					Riudb::factory()->id($id)->save($array);
					Session::instance()->set('flash','Rola została dodana.');
				}

			$this->request->redirect($this->request->referrer());

			}
			else{

			$this->request->redirect($this->request->referrer());

			}
				}
				else{
				$param = Request::current()->param('param');
				$count = $this->model->deletemember($param);


				$result = $this->model->memberusers($id);
			$resulte = json_decode(json_encode(explode(',',$result['roles'])));
					$array = array('members'=>$resulte);
					Riudb::factory()->id($id)->save($array);
				$this->request->redirect($this->request->referrer());
				}
			
		}

		if($_POST){
			$post = Validation::factory($_POST)->rule('is_active', 'not_empty')->rule('is_ban', 'not_empty');
			if ($post->check()){
			$is_active = html::chars($_POST['is_active']);
			$is_ban = html::chars($_POST['is_ban']);
			$this->model->saveidprofil($id,$is_active,$is_ban);
			$array = array('is_ban'=>$is_ban, 'is_active'=>$is_active);
					Riudb::factory()->id($id)->save($array);
			Session::instance()->set('flash','Zmiany zostały zapisane.');
			$this->request->redirect($this->request->referrer());
			}
			else{
			$this->errors = $post->errors('root/unit');
			}
		}
		$this->template->content = Theme::factory('root/profil/edit')
			->bind('results', $results)->bind('errors', $this->errors)->bind('flash', $this->flash)
			->bind('members', $members)->bind('roles', $roles)->bind('sidebar',$this->sidebar);

	}

	public function action_delete()
	{
		$id = Request::current()->param('id');
		$param = Request::current()->param('param');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('root_profil_delete_title').': '.$record[$id]['name'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_profil_delete_crumb').': '.$record[$id]['name'])->render();
		if($param==='del'){
			// właściwe usuwanie rekordu
			DB::delete('records')->where('id','=',$id)->and_where('module','=',1)->execute();
			Riudb::factory()->id($id)->delfile()->deldir();

			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{
			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('root/profil/delete')->bind('record', $record[$id])->bind('user', $this->uid)->bind('referrer', $referrer);
		}
	}
}
