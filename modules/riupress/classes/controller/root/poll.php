<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Root_Poll extends Controller_Root_Root {

	// Listowanie wszystkich userów.
	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('root_poll');
			$this->breadcrumb->add('poll',__('root_poll_crumb'));
			$this->title = $this->title.' - '.__('root_poll_title');
			$this->sidebar = View::factory('root/poll/sidebar');
			$styles = array(
				$this->media->uri(array('file' => 'js/ui/themes/base/jquery.ui.all.css'))  => 'screen',
				$this->media->uri(array('file' => 'css/jquery.ui.datepicker.css'))  => 'screen',
				$this->media->uri(array('file' => 'css/editor.css'))  => 'screen',
			);

			$scripts = array(
				$this->media->uri(array('file' => 'js/ui/ui/i18n/jquery.ui.datepicker-pl.js')),
				$this->media->uri(array('file' => 'js/backend/jquery-ui-1.8.11.custom.min.js')),
				$this->media->uri(array('file' => 'js/backend/timepicker.js')),
				$this->media->uri(array('file' => 'js/backend/poll.js')),
			);

			$this->template->scripts = array_merge( $scripts, $this->template->scripts);
			$this->template->styles = array_merge( $styles, $this->template->styles);

		}
	}

	public function action_index(){
	$this->template->title = $this->title;
	$this->template->breadcrumb = $this->breadcrumb->render();
	$list = $this->_listRecords(9);
	$this->template->content = View::factory("root/poll/index")
			->bind('list',$list)
			->bind('sidebar',$this->sidebar);
	}

	public function action_search()
	{

		$this->template->title = $this->title.' - '.__('root_poll_search_title');
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_poll_search_crumb'))->render();
		if($_POST){
			$post = Validation::factory($_POST)->rule('search', 'not_empty');
			if ($post->check()){
			$search = html::chars($_POST['search']);
			Session::instance()->set('search',$search);
			}
			else{
			$this->request->redirect('root/poll');
			}
		}

		$search = Session::instance()->get('search');
		if(!empty($search)){
			$list = $this->_listSearch(9,$search);
			$this->template->content = View::factory("root/poll/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
		}
		else{
			$this->request->redirect('root/poll');
		}

			
	}
	public function _listRecords($onpage = 10){
	$offset = ($this->page*$onpage)-$onpage;
	$records = $this->model->allRecords($offset,$onpage);
	$count = $this->model->countallRecords();
	$results = Riudb::factory()->get($records)->render();
	return $this->_pagination('root/poll/list', $results, $count, $onpage);
	}

	public function _listSearch($onpage = 10,$search){
	$offset = ($this->page*$onpage)-$onpage;
	$records = $this->model->searchallRecords($search,$offset,$onpage);
	$count = $this->model->searchcountallRecords($search);
	$results = Riudb::factory()->get($records)->render();
	return $this->_pagination('root/poll/list', $results, $count, $onpage);
	}
	// Usuwanie użytkownika.

	public function action_delete()
	{
		$id = Request::current()->param('id');
		$param = Request::current()->param('param');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('root_poll_delete_title').': '.$record[$id]['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_poll_delete_crumb').': '.$record[$id]['term'])->render();
		if($param==='del'){
			// usuwanie załączników

			DB::delete('records')->where('id','=',$id)->and_where('module','=',8)->execute();
			Riudb::factory()->id($id)->delfile()->deldir();

			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{
			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('root/poll/delete')->bind('record', $record[$id])->bind('user', $this->uid)->bind('referrer', $referrer);
		}
	}

	public function action_add(){

	$this->template->title = $this->title.' - '.__('root_poll_add_title');
	$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_poll_add_crumb'))->render();

		if($_POST){
		$post = Validation::factory($_POST)->rule('term', 'not_empty')
			->rule('term', 'min_length', array(':value', '3'))
			->rule('term', 'max_length', array(':value', '128'));

		if ($post->check()){
			$user = $this->uid;
			$term = html::chars($_POST['term']);
			$status = 2;
			$module = 8;
			$id = $this->model->add($module);
			$time = time();

			DB::insert('polls', array('record', 'user', 'status',  'publish', 'expiry', 'term'))
				->values(array($id, $user, $status, $time, $time, $term))->execute();

			$pre = $this->model->json_poll();
			Riudb::factory()->id($id)->add(TRUE, $pre);

		$array = array('id'=>$id,  'user'=>$user, 'publish'=>$time, 'expiry'=>$time, 'status'=>$status, 'term'=>$term, 'answers'=>array(), 'votes'=>array());
			Riudb::factory()->id($id)->save($array);


				Session::instance()->set('flash',array(array('success','Ankieta została Utworzona.')));
				$this->request->redirect('root/poll/edit/'.$id);
			}
			else{
			$this->errors = $post->errors('blog');
			}
		}

		$this->template->content = View::factory('root/poll/add')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('sidebar',$this->sidebar);


	}

	public function action_edit()
	{
		$id = Request::current()->param('id');
		$param = Request::current()->param('param');
		$subparam = Request::current()->param('subparam');
		$record = Riudb::factory()->get($id)->render();
		if(!empty($param)){
	
			switch($param)
			{
				case 'delete': 

					$this->model->delete_answer($subparam,$id);
					$this->model->up_answers($id);
						Session::instance()->set('flash',array(array('success','Odpowiedź została usunięta.')));
						$this->request->redirect('root/poll/edit/'.$id);
				break;
				case 'add': 
					if($_POST){
					$post = Validation::factory($_POST)->rule('answer', 'not_empty')
						->rule('answer', 'min_length', array(':value', '3'))
						->rule('answer', 'max_length', array(':value', '128'));
						if ($post->check()){
							$votes = 0;
							$answer = html::chars($_POST['answer']);
							$count = $this->model->count_answers($id);
							$count++;
							$ida = $this->model->add_answer($id,$answer,$votes,$count);
							$this->model->up_answers($id);


						Session::instance()->set('flash',array(array('success','Odpowiedź została dodana.')));
						$this->request->redirect('root/poll/edit/'.$id);
						}
						else{
						Session::instance()->set('flash',array(array('warning','Dodaj odpowiedź.')));
						$this->request->redirect('root/poll/edit/'.$id);
						}
					}
					else{
						$this->request->redirect('root/poll/edit/'.$id);
					}
				break;
				case 'edit': 

					if($_POST){
					$post = Validation::factory($_POST)->rule('answer', 'not_empty')
						->rule('answer', 'min_length', array(':value', '3'))
						->rule('answer', 'max_length', array(':value', '128'));
						if ($post->check()){
							$votes = 0;
							$answer = html::chars($_POST['answer']);
							$this->model->up_answer($subparam,$answer);
							$this->model->up_answers($id);
								Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
								$this->request->redirect('root/poll/edit/'.$id);
						}
						else{
						Session::instance()->set('flash',array(array('warning','Dodaj odpowiedź.')));
						$this->request->redirect('root/poll/edit/'.$id);
						}
					}
					else{
						$this->request->redirect('root/poll/edit/'.$id);
					}

				break;
				case 'move': 
					$move = explode('-', $subparam);
					$answer = $move[0];
					$moveup = $move[1];
					$movedown= $moveup+1;
					DB::update('poll_answers')->set(array('order'=>$movedown))->where('poll','=',$id)->where('order','=',$moveup)->execute();
					DB::update('poll_answers')->set(array('order'=>$moveup))->where('id','=',$answer)->execute();
					$this->model->up_answers($id);
						Session::instance()->set('flash',array(array('success','Odpowiedź została przesunięta.')));
						$this->request->redirect('root/poll/edit/'.$id);
				break;
				case 'save': 
					if($_POST){
					$post = Validation::factory($_POST)->rule('term', 'not_empty')
						->rule('term', 'min_length', array(':value', '3'))
						->rule('term', 'max_length', array(':value', '128'));
						if ($post->check()){

							$term = html::chars($_POST['term']);
							$status = $_POST['status'];
							$publish = $_POST['publish'];
							$publish = strtotime($publish);
							$expiry = $_POST['expiry'];
							$expiry = strtotime($expiry);

							$this->model->up_poll($id,$term,$publish,$expiry,$status);
							$array = array('publish'=>$publish, 'expiry'=>$expiry, 'status'=>$status, 'term'=>$term);
							Riudb::factory()->id($id)->save($array);

								Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
								$this->request->redirect('root/poll/edit/'.$id);
						}
						else{
						Session::instance()->set('flash',array(array('warning','Dodaj odpowiedź.')));
						$this->request->redirect('root/poll/edit/'.$id);
						}
					}
					else{
						$this->request->redirect('root/poll/edit/'.$id);
					}
				break;
			}
		}
		else{

		$this->template->title = $this->title.' - '.__('root_poll_edit_title').': '.$record[$id]['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_poll_edit_crumb').': '.$record[$id]['term'])->render();

		$this->template->content = View::factory('root/poll/edit')
					->bind('record', $record[$id])->bind('user', $this->uid)->bind('flash', $this->flash)->bind('errors', $this->errors)->bind('sidebar',$this->sidebar);
		

		}
	}
}
