<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Editor_Editor extends Controller_Template_Default {

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->breadcrumb->add('editor',__('editor_index_crumb'));
			$this->_ispermission();
		}
	}

	protected function post_post($id)
	{

		$post = Validation::factory($_POST)->rule('term', 'not_empty')->rule('post', 'not_empty')->rule('publish', 'not_empty');

		if ($post->check()){

			$term = html::chars($_POST['term']);
			$post = html::chars($_POST['post']);
			$post_json = Markdown::parse($_POST['post']);
			$slug = url::title($term, $separator = '-', $ascii_only = TRUE);
			$publish = $_POST['publish'];
			$status = $_POST['status'];
			$publish = strtotime($publish);

			DB::update('posts')->set(array('post'=>$post))->where('record','=',$id)->execute();
			DB::update('terms')->set(array('term'=>$term))->where('record','=',$id)->execute();
			DB::update('publishes')->set(array('publish'=>$publish, 'status'=>$status))->and_where('record','=',$id)->execute();

			$data = array('term'=>$term, 'post'=>$post_json, 'status'=>$status, 'publish'=>$publish);
			Riudb::factory()->id($id)->save($data);
			$success = 1;
			$message = ' - zmiany zostały zapisane :)';

		}
		else{

			$success = 0;
			$message = ' - niestety zapisanie nie powiodło się :(';

		}

		$json = array('success' => $success,'message' => $message);
		echo json_encode($json);

	}

	protected function is_comment($id)
	{

		$post = Validation::factory($_POST)->rule('is_comment', 'not_empty');

		if ($post->check()){

			$is_comment = $_POST['is_comment'];
			DB::update('count_comments')->set(array('is_comment'=>$is_comment))->where('record','=',$id)->execute();

			$data = array('is_comment'=>$is_comment);
			Riudb::factory()->id($id)->save($data);

			$success = 1;
			$message = ' - zmiany zostały zapisane :)';

		}
		else{

			$success = 0;
			$message = ' - niestety zapisanie nie powiodło się :(';

		}

		$json = array('success' => $success,'message' => $message);
		echo json_encode($json);

	}

	public function attachment($id)
	{

		$model = Model::factory('editor_files');
		$module = Request::current()->controller();

		if($_FILES){

			$post = new Validation(array_merge($_POST, $_FILES));
			$post->rule('file', 'Upload::not_empty')
				->rule('file', 'Upload::type', array(':value',array('jpg', 'jpeg', 'png', 'doc', 'odt', 'pdf', 'doc', 'mp3', 'flv', 'zip', 'gz', 'tar', 'tar.gz')));

			if ($post->check()){

				$user = Session::instance()->get('uid');
				$module = 6;
				$record = $model->add($module);

				$pre = $model->json_file();
				Riudb::factory()->id($record)->add(TRUE, $pre);

				$file = $_FILES['file'];

				$mime = File::ext_by_mime($file['type']);

				$array = explode('.', $file['name'], -1);
				$comma_separated = implode(" ", $array);
				$title = html::chars($comma_separated);

				$path = Riudb::factory()->id($record)->getpath();
				$filename = Upload::save($_FILES['file'],$filename = NULL,$path,0777);
				$basename = basename($filename);

				$photon = url::title($comma_separated, $separator = '-', $ascii_only = TRUE).'.'.$mime;
				rename($path.'/'.$basename.'', $path.'/'.$photon.'');

				if($mime=='jpg' or $mime=='jpeg'){

					$filename = $path.'/'.$photon;
					$this->gd_data = getimagesize($path.'/'.$photon.'');
					$this->gd_image_out = FALSE;
					$width = $this->gd_data[0];
					$height = $this->gd_data[1];

					if($width > 400 OR $height > 540){

						Image::factory($filename)
							->sharpen(20)
							->resize(400, 540, Image::AUTO)
							->save($path.'/'.$photon.'','85');

						$this->gd_data = getimagesize($path.'/'.$photon.'');
						$this->gd_image_out = FALSE;
						$width = $this->gd_data[0];
						$height = $this->gd_data[1];
					}
					else{

						Image::factory($filename)
							->sharpen(20)
							->save($path.'/'.$photon.'','85');

					}
				}
				else{

					$width = '';
					$height = '';

				}

				$size = filesize($path.'/'.$photon.'');

				$model->attachment_add($record, $user, $photon, $mime, $width, $height, $size);
				$model->add_relations($id,$record,$module);

				$array = array('id'=>$record, 'parent'=>$id, 'module'=>$module, 'user'=>$user, 'file'=>$photon, 'mime'=>$mime, 'width'=>$width, 'height'=>$height, 'size'=>$size);
				Riudb::factory()->id($record)->save($array);

				$newrid = $model->get_relations($id,$module);
				$farray = array('files'=>$newrid);
				Riudb::factory()->id($id)->save($farray);			


				$sizenew = Attach::format_bytes($size);
				$download = 'api/file/'.$record.'/'.$photon;
				$href = url::base().$download;
				$del = url::base().'editor/blog/deletefile/'.$record;
				echo '{"title":"'.$photon.'","mime":"'.$mime.'","size":"'.$sizenew.'","id":"'.$record.'","href":"'.$href.'","download":"'.$download.'","del":"'.$del.'"}';
				
			}
		}
	}


	protected function icon($id)
	{
		$model = Model::factory('editor_icons');
		$module = Request::current()->controller();

		if($_POST){

			if(!empty($_POST['rel'])){

				$this->auto_render = FALSE;
				$post = new Validation($_POST);
				$post = Validation::factory($_POST)->rule('rel', 'not_empty');

				if ($post->check()){

					$rel = html::chars($_POST['rel']);

					$query = $model->up_relations($id, $rel,5);

					if(!$query){

						$model->add_relations($id, $rel,5);

					}

					$data = array('icon'=>$rel);
					Riudb::factory()->id($id)->save($data);

					$message = ' - ikonka została przypisana!';

					$json = array('success' => 1, 'message' => $message, 'icon' => $rel);

				}
				else{

					$json = array('success' => 0, 'message' => ' - podaj prawidlową nazwę ikonki');
				}

				echo json_encode($json);

			}
			else{

				$this->auto_render = FALSE;

				$post = new Validation($_POST);
				$post = Validation::factory($_POST)->rule('icon', 'not_empty');

				if ($post->check()){


					$search = html::chars($_POST['icon']);
					$after = $model->get_icons($search);

					$ca = count($after);

					if($ca > 0){

						$message = ' - znalezionych ikonek: '.$ca.' ! :)';

					}
					else{

						$message = ' - nie znaleziono zadnej ikonki :(';

					}

					$module = $this->loader['module'];
					$json = array('success' => 1, 'message' => $message, 'arr' => $after);
				}
				else{

					$json = array('success' => 0, 'message' => 'Podaj prawidlową nazwę ikonki');
				}

				echo json_encode($json);

			}

		}
	}

	protected function tokens()
	{


		$this->auto_render = FALSE;
		$model = Model::factory('editor_tag');
		$user = Session::instance()->get('uid');
		$token = html::chars($_POST['token']);
		$results = $model->tag_token($user,$token);
		echo json_encode($results);

	}


	protected function tokentag($id)
	{
		$this->auto_render = FALSE;
		$model = Model::factory('editor_tag');
		$post = Validation::factory($_POST)->rule('token', 'not_empty');

		if ($post->check()){


			$tags = explode(',', $_POST['token'], -1);
			$tags = $this->array_flatten(array_unique($tags));
			$pre = $model->get_relations($id, 2);
			$p = 0;

			$user = Session::instance()->get('uid');
			foreach ($tags as $term){

				if( ! in_array($term, $pre, TRUE)){

				$p++;
				$model->add_relations($id,$term,2);

				}

			}

			if($p){

				$message = ' - dodanych tagów: '.$p;

			}
			else{

				$message = ' - nie dodano nowych tagów';

			}


			$tags = $model->get_relations($id, 2);
			$after = array('tags'=>$tags);
			Riudb::factory()->id($id)->save($after);
			$relations = $model->tag_relations($id);

			$uri = Request::current()->uri();
			$uri = str_replace("tokentag", "deletetag", $uri);
			$uri = url::base().$uri;
			$json = array('success' => 1, 'message' => $message, 'arr' => $relations, 'href' => $uri);
		}
		else{

			$json = array('success' => 1, 'message' => ' - status nie może być zmieniony');

		}

		echo json_encode($json);

	}

	protected function deletetag($id)
	{

		$model = Model::factory('editor_tag');
		$post = Validation::factory($_POST)->rule('rel', 'not_empty');
		$user = Session::instance()->get('uid');

		if ($post->check()){

			$tag = $_POST['rel'];
			$model->del_relations($id, $tag,2);

			$success = 1;

		}else{

			$success = 0;

		}

		$tags = $model->get_relations($id, 2);
		$after = array('tags'=>$tags);
		Riudb::factory()->id($id)->save($after);
		$json = array('success' => $success);
		echo json_encode($json);

		
	}


	protected function submitnewtag($id)
	{

		$model = Model::factory('editor_tag');
		$post = Validation::factory($_POST)->rule('newtag', 'not_empty');

		if ($post->check()){

			$user = Session::instance()->get('uid');

			$term = html::chars($_POST['newtag']);
			$slug = url::title($term, $separator = '-', $ascii_only = TRUE);
			$record = DB::select('record')->from('tags')->where('slug','=',$slug)->execute()->current();

			if(!empty($record['record'])){

				$isu = $model->count_relations($user,$record['record'],2);
				$isut = $model->count_relations($id,$record['record'],2);

				if(empty($isu)){
				$model->add_relations($user,$record['record'],2);
				}

				if(empty($isut)){
				$model->add_relations($id,$record['record'],2);
				}
			
			}
			else{


			$module = 2;
			$new = $model->add($module);
			$model->add_tags($new, $term, $slug);
			$model->add_relations($user,$new,2);
			$model->add_relations($id,$new,2);

				$pre = $model->json_tag();
				Riudb::factory()->id($new)->add(FALSE, $pre);
				$array = array('id'=>$new, 'module'=>$module, 'term'=>$term, 'slug'=>$slug);
				Riudb::factory()->id($new)->save($array);

			$term = $new;
			}

			$tags = $model->get_relations($id, 2);
			$after = array('tags'=>$tags);
			Riudb::factory()->id($id)->save($after);
			$relations = $model->tag_relations($id);
			$message = ' - dodany :)';

			$uri = Request::current()->uri();
			$uri = str_replace("submitnewtag", "deletetag", $uri);
			$uri = url::base().$uri;
			$json = array('success' => 1, 'message' => $message, 'arr' => $relations, 'href' => $uri);
		}
		else{

			$json = array('success' => 1, 'message' => 'Status nie może być zmieniony');

		}

		echo json_encode($json);

		
	}


	protected function reltokens($id)
	{

		$this->auto_render = FALSE;
		$model = Model::factory('editor_blog');

		$token = html::chars($_POST['token']);
		$results = $model->rel_token($token,$id);

		echo json_encode($results);

	}


	protected function tokenrelation($id)
	{

		$model = Model::factory('editor_blog');
		$this->auto_render = FALSE;
		$post = Validation::factory($_POST)->rule('reltoken', 'not_empty');

		if ($post->check()){

			$tags = explode(',', $_POST['reltoken'], -1);

			$tags = $this->array_flatten(array_unique($tags));
			$pre = $model->get_relations($id, 4);

			$p = 0;


			$user = Session::instance()->get('uid');
			foreach ($tags as $term){

				if( ! in_array($term, $pre, TRUE)){

				$p++;
				$model->add_relations($id,$term,4);

				}

			}


			if($p){

				$message = ' - dodanych podobnych: '.$p;

			}
			else{

				$message = ' - nie dodano nowych powiązanych';

			}

			$tags = $model->get_relations($id, 4);
			$after = array('relations'=>$tags);
			Riudb::factory()->id($id)->save($after);
			$relations = $model->token_relations($id);

			$uri = Request::current()->uri();
			$uri = str_replace("tokenrelation", "deleterelation", $uri);
			$uri = url::base().$uri;
			$json = array('success' => 1, 'message' => $message, 'arr' => $relations, 'href' => $uri);
		}
		else{

			$json = array('success' => 1, 'message' => 'Status nie może być zmieniony');

		}

		echo json_encode($json);

	}


	protected function deleterelation($id)
	{

		$model = Model::factory('editor_blog');
		$post = Validation::factory($_POST)->rule('rel', 'not_empty');

		$user = Session::instance()->get('uid');

		if ($post->check()){

			$rel = $_POST['rel'];
			$model->del_relations($id, $rel,4);

			$success = 1;

		}
		else{

			$success = 0;

		}

		$tags = $model->get_relations($id, 4);
		$after = array('relations'=>$tags);
		Riudb::factory()->id($id)->save($after);
		$json = array('success' => $success);
		echo json_encode($json);

	}

	protected function categories($id)
	{

		$model = Model::factory('root_category');
		$post = Validation::factory($_POST)->rule('rel', 'not_empty');
		$user = Session::instance()->get('uid');

		if ($post->check()){

			$rel = $_POST['rel'];
			$actions = explode('-', $rel);
			$relation = $actions[0];
			$action = $actions[1];	

			$record = Riudb::factory()->get($relation)->render();

			if($action === '1'){

				$model->add_relations($id, $relation ,3);
				$model->count_categoryplus($id,$relation);
				$action = 1;

			}
			else{

				$model->del_relations($id, $relation ,3);
				$model->count_categoryminus($id,$relation);
				$action = 2;

			}

			$count = $model->count_categorycount($relation);
			$after = array('count'=>$count);
			Riudb::factory()->id($relation)->save($after);

			$success = 1;

		}
		else{

			$success = 0;

		}

		$tags = $model->get_relations($id, 3);
		$after = array('cats'=>$tags);
		Riudb::factory()->id($id)->save($after);
		$json = array('success' => $success,'action' => $action,'relation' => $relation,'count' => $count);
		echo json_encode($json);

	}

	protected function array_flatten($array, &$new_array = array(), $index = '')
	{

		foreach ($array as $key => $value){

			if (!empty($index)){

			    $key = $index.'_'.$key;

			}

			if (is_array($value)){

			    $new_array = $this->array_flatten($value, $new_array, $key);

			}
			else{

			    $new_array[$key] = $value;

			}

		}

		return $new_array;
	}	
}

