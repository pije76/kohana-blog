<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Editor_Tag extends Controller_Editor_Editor{
	
	public $module;

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('editor_tag');
			$this->breadcrumb->add('tag',__('editor_tag_crumb'));
			$this->title = $this->title.' - '.__('editor_tag_title');
			$this->sidebar = View::factory('editor/tag/sidebar');
			$this->module = 2;
		}
	}

	public function _list($onpage = 9, $search = FALSE){

		$offset = ($this->page*$onpage)-$onpage;
		$records = $this->model->get_all($this->uid,$offset,$onpage, $search);
		$count = $this->model->count_all($this->uid, $search);
		$results = Riudb::factory()->get($records)->render();
		return $this->_pagination('editor/tag/list', $results, $count, $onpage);

	}

	public function action_index(){

		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$list = $this->_list(9);
		$this->template->content = View::factory("editor/tag/index")
			->bind('list',$list)
			->bind('sidebar',$this->sidebar);
	}

	public function action_search()
	{

		$this->template->title = $this->title.' - '.__('editor_tag_search_title');
		$this->template->breadcrumb = $this->breadcrumb->add('search',__('editor_tag_search_crumb'))->render();

		if($_POST){

			$post = Validation::factory($_POST)->rule('search', 'not_empty');

			if ($post->check()){

				$search = html::chars($_POST['search']);
				Session::instance()->set('search',$search);

			}
			else{

				$this->request->redirect('editor/tag');

			}
		}

		$search = Session::instance()->get('search');

		if(!empty($search)){

			$list = $this->_list(9,$search);
			$this->template->content = View::factory("editor/tag/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
		}
		else{

			$this->request->redirect('editor/tag');

		}

	}

	public function action_delete()
	{

		$this->template->title = $this->title.' - '.__('editor_tag_delete_title').': '.$this->loader['id']['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_tag_delete_crumb').': '.$this->loader['id']['term'])->render();

		if($this->param === 'del'){

			$this->model->delete_module($this->id,$this->module);
			Riudb::factory()->id($this->id)->delfile()->deldir();

			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{

			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('editor/tag/delete')
				->bind('record', $this->loader['id'])->bind('user', $this->uid)->bind('referrer', $referrer);
		}
	}

	public function action_add(){

		$this->template->title = $this->title.' - '.__('editor_tag_add_title');
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_tag_add_crumb'))->render();

		if($_POST){

			$post = Validation::factory($_POST)->rule('term', 'not_empty')
				->rule('term', 'min_length', array(':value', '3'))
				->rule('term', 'max_length', array(':value', '32'));

			if ($post->check()){

				$term = html::chars($_POST['term']);
				$slug = url::title($term, $separator = '-', $ascii_only = TRUE);

				$tag = $this->model->is_slug($slug);

				if(!empty($tag)){

					$countrelations = $this->model->count_relations($this->uid,$tag['record'],$this->module);

					if(empty($countrelations)){

						$this->model->add_relations($this->uid,$tag['record'],$this->module);
						Session::instance()->set('flash',array(array('warning','Ten tag już istnieje więc został przypisany do Ciebie.')));

					}
					else{

						Session::instance()->set('flash',array(array('warning','Oj.. przecież już masz ten tag ;).')));

					}

					$this->request->redirect('editor/tag/add');

				}
				else{

					$new = $this->model->add($this->module);
					$this->model->add_tags($new, $term, $slug);
					$this->model->add_relations($this->uid,$new,$this->module);

					$pre = $this->model->json_tag();
					Riudb::factory()->id($new)->add(FALSE, $pre);
					$array = array('id'=>$new, 'module'=>$this->module, 'term'=>$term, 'slug'=>$slug);
					Riudb::factory()->id($new)->save($array);

					Session::instance()->set('flash',array(array('success','Tag został dodany do bazy.')));
					$this->request->redirect('editor/tag/add');
				}
			}
			else{

				$this->errors = $post->errors('tag');

			}
		}

		$this->template->content = View::factory('editor/tag/add')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('sidebar',$this->sidebar);

	}

	public function action_edit()
	{

		$this->template->title = $this->title.' - '.__('editor_tag_edit_title').': '.$this->loader['id']['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_tag_edit_crumb').': '.$this->loader['id']['term'])->render();

		if($_POST){

			$post = Validation::factory($_POST)->rule('term', 'not_empty')
				->rule('term', 'min_length', array(':value', '2'))
				->rule('term', 'max_length', array(':value', '64'));

			if ($post->check()){

				$term = html::chars($_POST['term']);
				$slug = url::title($term, $separator = '-', $ascii_only = TRUE);
				$this->model->up_tag($this->id,$term,$slug);

				$array = array('term'=>$term, 'slug'=>$slug);
				Riudb::factory()->id($this->id)->save($array);

				Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
				$this->request->redirect('editor/tag/edit/'.$this->id);

			}
			else{

				$this->errors = $post->errors('tag');

			}

		}

		$this->template->content = View::factory('editor/tag/edit')
					->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('record', $this->loader['id'])->bind('sidebar',$this->sidebar);
		
	}
}
