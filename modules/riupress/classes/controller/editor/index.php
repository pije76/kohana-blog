<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Editor_Index extends Controller_Editor_Editor {

	public function action_index(){

		$this->template->breadcrumb = $this->breadcrumb->render();
		$this->template->title = $this->title;

	}
}
