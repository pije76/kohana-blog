<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Editor_Blog extends Controller_Editor_Editor{

	public $module;

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('editor_blog');
			$this->breadcrumb->add('blog',__('editor_blog_crumb'));
			$this->title = $this->title.' - '.__('editor_blog_title');
			$this->sidebar = View::factory('editor/blog/sidebar');
			$this->module = 4;

			$styles = array(
				$this->media->uri(array('file' => 'js/ui/themes/base/jquery.ui.all.css'))  => 'screen',
				$this->media->uri(array('file' => 'css/jquery.ui.datepicker.css'))  => 'screen',
				$this->media->uri(array('file' => 'css/jquery.fileupload-ui.css'))  => 'screen',
				$this->media->uri(array('file' => 'js/backend/token/token-input.css'))  => 'screen',
				$this->media->uri(array('file' => 'css/editor.css'))  => 'screen',
			);

			$scripts = array(
				$this->media->uri(array('file' => 'js/ui/ui/i18n/jquery.ui.datepicker-pl.js')),
				$this->media->uri(array('file' => 'js/backend/jquery-ui-1.8.11.custom.min.js')),
				$this->media->uri(array('file' => 'js/backend/timepicker.js')),
				$this->media->uri(array('file' => 'js/backend/jquery.fileupload.js')),
				$this->media->uri(array('file' => 'js/backend/jquery.fileupload-ui.js')),
				$this->media->uri(array('file' => 'js/backend/token/jquery.tokeninput.js')),
				$this->media->uri(array('file' => 'js/backend/blog.js')),
			);

			$this->template->scripts = array_merge( $scripts, $this->template->scripts);
			$this->template->styles = array_merge( $styles, $this->template->styles);

		}
	}

	public function _list($onpage = 9,$search = FALSE){

		$offset = ($this->page*$onpage)-$onpage;
		$records = $this->model->get_all($this->uid,$offset,$onpage,$search);
		$count = $this->model->count_all($this->uid,$search);
		$results = Riudb::factory()->get($records)->render();
		return $this->_pagination('editor/blog/list', $results, $count, $onpage);

	}

	public function action_index(){

		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$list = $this->_list(9);
		$this->template->content = View::factory("editor/blog/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);
	}

	public function action_search()
	{

		$this->template->title = $this->title.' - '.__('editor_blog_search_title');
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_blog_search_crumb'))->render();

		if($_POST){

			$post = Validation::factory($_POST)->rule('search', 'not_empty');

			if ($post->check()){

				$search = html::chars($_POST['search']);
				Session::instance()->set('search',$search);

			}
			else{

				$this->request->redirect('editor/blog');

			}
		}

		$search = Session::instance()->get('search');

		if(!empty($search)){

			$list = $this->_listSearch(9,$search);
			$this->template->content = View::factory("editor/blog/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);

		}
		else{

			$this->request->redirect('editor/blog');

		}

			
	}

	public function action_delete()
	{

		$this->template->title = $this->title.' - '.__('editor_blog_delete_title').': '.$this->loader['id']['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_blog_delete_crumb').': '.$this->loader['id']['term'])->render();

		if($param==='del'){

			$files = $this->loader['id']['files'];

			foreach($files as $f){

				$this->model->delete_module($f,6);
				Riudb::factory()->id($f)->delfile()->deldir();
			}

			$comments = $record[$id]['comments'];
			foreach($comments as $c=>$a){
				$this->model->delete_module($c,7);
				Riudb::factory()->id($c)->delfile()->deldir();
			}

			$this->model->delete_module($this->id,$this->module);
			Riudb::factory()->id($this->id)->delfile()->deldir();

			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);

		}
		else{

			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('editor/blog/delete')->bind('record', $record[$id])->bind('user', $this->uid)->bind('referrer', $referrer);

		}
	}

	public function action_add(){

		$this->template->title = $this->title.' - '.__('editor_blog_add_title');
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_blog_add_crumb'))->render();

		if($_POST){

			$post = Validation::factory($_POST)->rule('term', 'not_empty')
				->rule('term', 'min_length', array(':value', '3'))
				->rule('term', 'max_length', array(':value', '128'));

			if ($post->check()){


				$term = html::chars($_POST['term']);
				$status = 2;
				$publish = time();
				$count_comment = '0';
				$is_comment = '1';
				$icon = '';

				$record = $this->model->add($this->module);
				$this->model->add_publishes($record, $status, $this->uid, $publish);
				$this->model->add_terms($record, $term);
				$this->model->add_posts($record,'');


				$pre = $this->model->json_blog();
				Riudb::factory()->id($record)->add(TRUE, $pre);

				$array = array('id'=>$record, 'module'=>$this->module,  'user'=>$this->uid, 'publish'=>$publish, 'updated'=>$publish, 'status'=>$status, 'term'=>$term, 'post'=>'', 'icon'=>$icon, 'is_comment'=>$is_comment, 'comments'=>array(), 'count_comment'=>'',  'cats'=>array(), 'tags'=>array(), 'relations'=>array(), 'files'=>array(), 'sum_rating'=>'0','ratings'=>array());
				Riudb::factory()->id($record)->save($array);


				Session::instance()->set('flash',array(array('success','Wpis został utowrzony.')));
				$this->request->redirect('editor/blog/edit/'.$record);

			}
			else{

				$this->errors = $post->errors('blog');

			}
		}

		$this->template->content = View::factory('editor/blog/add')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('sidebar',$this->sidebar);

	}

	public function action_edit()
	{


		if ($this->request->is_ajax())  
		{
	
			$this->id = Request::current()->param('id');
			$this->param = Request::current()->param('param');

			switch($this->param)
			{
				case 'post': return $this->post_post($this->id); break;
				case 'comment': return $this->is_comment($this->id); break;
				case 'attachment': return $this->attachment($this->id); break;
				case 'icon': return $this->icon($this->id); break;
				case 'tokens': return $this->tokens($this->id); break;
				case 'tokentag': return $this->tokentag($this->id); break;
				case 'deletetag': return $this->deletetag($this->id); break;
				case 'submitnewtag': return $this->submitnewtag($this->id); break;
				case 'reltokens': return $this->reltokens($this->id); break;
				case 'tokenrelation': return $this->tokenrelation($this->id); break;
				case 'deleterelation': return $this->deleterelation($this->id); break;
				case 'categories': return $this->categories($this->id); break;
			}
		}
		else{

			$this->template->title = $this->title.' - '.__('editor_blog_edit_title').': '.$this->loader['id']['term'];
			$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_blog_edit_crumb').': '.$this->loader['id']['term'])->render();

			$record = Riudb::factory()->get($this->id)->join('files')->join('tags')->join('relations')->render();
			$categories = $this->model->get_cats(4);
			$postterm = $this->model->get_postterm($this->id);
			$this->template->content = View::factory('editor/blog/edit')
						->bind('record', $record[$this->id])->bind('cats', $categories)->bind('postterm',$postterm)->bind('user', $this->uid)->bind('sidebar',$this->sidebar);
		
		}
	}

	public function action_deletefile()
	{

		$this->template->title = $this->title.' - '.__('root_doc_deletefile_title').': '.$this->loader['id']['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('root_doc_deletefile_crumb').': '.$this->loader['id']['term'])->render();

		if($param==='del'){


			$this->model->delete_module($this->id,$this->module);
			Riudb::factory()->id($this->id)->delfile()->deldir();

			$newrid = $this->model->get_relations($this->loader['id']['parent'],6);
			$farray = array('files'=>$newrid);
			Riudb::factory()->id($this->loader['id']['parent'])->save($farray);		
			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);

		}
		else{

			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('editor/blog/deletefile')->bind('record', $this->loader['id'])->bind('user', $this->uid)->bind('referrer', $referrer);

		}
	}
}
