<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Editor_Icons extends Controller_Editor_Editor {

	public $module;

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('editor_icons');
			$this->breadcrumb->add('tag',__('editor_icon_crumb'));
			$this->title = $this->title.' - '.__('editor_icon_title');
			$this->sidebar = View::factory('editor/icons/sidebar');
			$this->module = 5;

			$styles = array(
				$this->media->uri(array('file' => 'css/editor.css'))  => 'screen',
			);

			$this->template->styles = array_merge( $styles, $this->template->styles);
		}
	}

	public function _list($onpage = 10, $search = FALSE){

		$offset = ($this->page*$onpage)-$onpage;
		$records = $this->model->get_all($this->uid,$offset,$onpage,$search);
		$count = $this->model->count_all($this->uid,$search);
		$results = Riudb::factory()->get($records)->render();
		return $this->_pagination('editor/icons/list', $results, $count, $onpage);

	}

	public function action_index()
	{

		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$list = $this->_list(9);
		$this->template->content = View::factory("editor/icons/index")
			->bind('list',$list)
			->bind('sidebar',$this->sidebar);

	}

	public function action_search()
	{
		$this->template->title = $this->title.' - '.__('editor_icon_search_title');
		$this->template->breadcrumb = $this->breadcrumb->add('search',__('editor_icon_search_crumb'))->render();

		if($_POST){

			$post = Validation::factory($_POST)->rule('search', 'not_empty');

			if ($post->check()){

				$search = html::chars($_POST['search']);
				Session::instance()->set('search',$search);
			}
			else{

				$this->request->redirect('editor/icons');

			}

		}
		$search = Session::instance()->get('search');

		if(!empty($search)){

			$list = $this->_list(9,$search);
			$this->template->content = View::factory("editor/icons/index")
				->bind('list',$list)
				->bind('sidebar',$this->sidebar);

		}
		else{

			$this->request->redirect('editor/icons');

		}

	}

	public function action_add()
	{

		$this->template->title = $this->title.' - '.__('editor_icon_add_title');
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_icon_add_crumb'))->render();

		if($_POST){

			$post = new Validation(array_merge($_POST, $_FILES));
			$post->rule('term', 'not_empty')
				->rule('term', 'min_length', array(':value', '2'))
				->rule('term', 'max_length', array(':value', '64'))
				->rule('file', 'Upload::not_empty')
				->rule('file', 'Upload::type', array(':value',array('jpg','jpeg')))
				->rule('file', 'Upload::size', array(':value','850KiB'));

				if ($post->check()){

					$term = html::chars($_POST['term']);
					$record = $this->model->add($this->module);
					$this->model->add_icon($record, $term);
					$this->model->add_relations($this->uid,$record,$this->module);

					$pre = $this->model->json_icon();
					Riudb::factory()->id($record)->add(TRUE, $pre);
					$array = array('id'=>$record, 'module'=>$this->module, 'term'=>$term);
					Riudb::factory()->id($record)->save($array);		
					$path = Riudb::factory()->id($record)->getpath();

					$filename = Upload::save($_FILES['file'],$filename = NULL,$path,0777);
					$basename = basename($filename);
					$temp = $path.'/temp.jpg';
					rename($path.'/'.$basename.'', $temp);

					Image::factory($temp)
						->resize(128, 128, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'large.jpg','85');
					Image::factory($temp)
						->resize(64, 64, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'medium.jpg','85');
					Image::factory($temp)
						->resize(32, 32, Image::AUTO)
						->save($path.DIRECTORY_SEPARATOR.'small.jpg','85');

					if (file_exists($temp)){

						unlink($temp);

					}

					Session::instance()->set('flash',array(array('success','Ikonka została dodana.')));			
					$this->request->redirect('editor/icons/add');
				}
				else{

			    		$this->errors = $post->errors('icons');

				}
		}

		$this->template->content = View::factory('editor/icons/add')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('sidebar',$this->sidebar);

	}

	public function action_delete()
	{

		$this->template->title = $this->title.' - '.__('editor_icon_delete_title').': '.$this->loader['id']['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_icon_delete_crumb').': '.$this->loader['id']['term'])->render();

		if($this->param === 'del'){

			$this->model->delete_module($this->id,$this->module);
			Riudb::factory()->id($this->id)->delfile()->deldir();

			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{

			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('editor/icons/delete')
				->bind('record', $this->loader['id'])->bind('user', $this->uid)->bind('referrer', $referrer);

		}
	}

	public function action_edit()
	{

		$this->template->title = $this->title.' - '.__('editor_icon_edit_title').': '.$this->loader['id']['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('editor_icon_edit_crumb').': '.$this->loader['id']['term'])->render();

		if($_POST){

			$post = Validation::factory($_POST)->rule('term', 'not_empty')
				->rule('term', 'min_length', array(':value', '2'))
				->rule('term', 'max_length', array(':value', '64'));

			if ($post->check()){

				$term = html::chars($_POST['term']);

				$this->model->up_icon($this->id,$term);
				$array = array('term'=>$term);
				Riudb::factory()->id($this->id)->save($array);

				Session::instance()->set('flash',array(array('success','Zmiany zostały zapisane.')));
				$this->request->redirect('editor/icons/edit/'.$this->id);

			}
			else{

				$this->errors = $post->errors('icons');

			}

		}

		$this->template->content = View::factory('editor/icons/edit')
					->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('record', $this->loader['id'])->bind('user', $this->uid)->bind('sidebar',$this->sidebar);
		
	}
}
