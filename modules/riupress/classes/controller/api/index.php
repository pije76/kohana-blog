<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Api_Index extends Controller {

	public $media;

	public function before()
	{
		if ($this->request->directory() === 'api')
		{
			// Do not template media files
			$this->auto_render = FALSE;
		}

		parent::before();
	}

	
	public function action_themes()
	{
	
		// Get the file path from the request
		
		$filename = $this->request->param('file');
		// Find the file extension
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		// Remove the extension from the filename
		$file = substr($filename, 0, -(strlen($ext) + 1));

		if (is_file(THEMEPATH.$filename))
		{
			// Check if the browser sent an "if-none-match: <etag>" header, and tell if the file hasn't changed
			$this->response->check_cache(sha1($this->request->uri()).filemtime(THEMEPATH.$filename), $this->request);

			// Send the file content as the response
			$this->response->body(file_get_contents(THEMEPATH.$filename));

			// Set the proper headers to allow caching
			$this->response->headers('content-type',  File::mime_by_ext($ext));
			$this->response->headers('last-modified', date('r', filemtime(THEMEPATH.$filename)));
		}
		else
		{
			// Return a 404 status
			$this->response->status(404);
		}
		
	}

	public function action_media()
	{
	
		// Get the file path from the request
		
		$filename = $this->request->param('file');
		// Find the file extension
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		// Remove the extension from the filename
		$file = substr($filename, 0, -(strlen($ext) + 1));

		if ($file = Kohana::find_file('media', $file, $ext))
		{
			// Check if the browser sent an "if-none-match: <etag>" header, and tell if the file hasn't changed
			$this->response->check_cache(sha1($this->request->uri()).filemtime($file), $this->request);
			
			// Send the file content as the response
			$this->response->body(file_get_contents($file));

			// Set the proper headers to allow caching
			$this->response->headers('content-type',  File::mime_by_ext($ext));
			$this->response->headers('last-modified', date('r', filemtime($file)));
		}
		else
		{
			// Return a 404 status
			$this->response->status(404);
		}
		
	}

	public function action_file()
	{
	

		// Get the file path from the request
		$rid = $this->request->param('rid');

		$path = Riudb::factory()->id($rid)->getpath();

		$file = $this->request->param('file');
		$filename = $file;
		// Find the file extension
		$ext = pathinfo($filename, PATHINFO_EXTENSION);

		// Remove the extension from the filename
		$file = substr($filename, 0, -(strlen($ext) + 1));

		if (is_file($path.DIRECTORY_SEPARATOR.$filename))
		{
			// Check if the browser sent an "if-none-match: <etag>" header, and tell if the file hasn't changed
			$this->response->check_cache(sha1($this->request->uri()).filemtime($path.DIRECTORY_SEPARATOR.$filename), $this->request);

			// Send the file content as the response
			$this->response->body(file_get_contents($path.DIRECTORY_SEPARATOR.$filename));

			// Set the proper headers to allow caching
			$this->response->headers('content-type',  File::mime_by_ext($ext));
			$this->response->headers('last-modified', date('r', filemtime($path.DIRECTORY_SEPARATOR.$filename)));
		}
		else
		{
			// Return a 404 status
			$this->response->status(404);


		}
		
	}

}
