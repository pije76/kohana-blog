<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Dev_Doc extends Controller_Root_Root {

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();

			$this->model = Model::factory('dev_doc');
			$this->breadcrumb->add('doc',__('dev_doc_crumb'));
			$this->title = $this->title.' - '.__('dev_doc_title');
			$this->sidebar = View::factory('dev/doc/sidebar');

			$styles = array(
				$this->media->uri(array('file' => 'css/jquery.fileupload-ui.css'))  => 'screen',
				$this->media->uri(array('file' => 'css/editor.css'))  => 'screen',
			);

			$scripts = array(
				$this->media->uri(array('file' => 'js/backend/jquery.fileupload.js')),
				$this->media->uri(array('file' => 'js/backend/jquery.fileupload-ui.js')),
				$this->media->uri(array('file' => 'js/backend/doc.js')),
			);

			$this->template->scripts = array_merge( $scripts, $this->template->scripts);
			$this->template->styles = array_merge( $styles, $this->template->styles);
		}
	}

	public function action_index(){
	$this->template->title = $this->title;
	$this->template->breadcrumb = $this->breadcrumb->render();
	$param = Request::current()->param('param');

	if(!empty($param)){

	$tablica = explode('-', $param);
	$depth = $tablica[0];
	$record = $tablica[1];
	$order = $tablica[2];
	$neworder = $order+1;
	if($depth>0){
	$upnew = DB::select('record','parent')->from('docs')->where('record','=',$record)->execute()->current();
	$downnew = DB::select('record')->from('docs')->where('depth','=',$depth)->and_where('parent','=',$upnew['parent'])->and_where('order','=',$order)->execute()->current();
	}
	else{
	$upnew = DB::select('record')->from('docs')->where('record','=',$record)->execute()->current();
	$downnew = DB::select('record')->from('docs')->where('depth','=',$depth)->and_where('order','=',$order)->execute()->current();
	}
	DB::update('docs')->set(array('order'=>$order))->where('record','=',$upnew['record'])->execute();
	DB::update('docs')->set(array('order'=>$neworder))->where('record','=',$downnew['record'])->execute();


	$data = array('order'=>$order);
	Riudb::factory()->id($upnew['record'])->save($data);

	$data = array('order'=>$neworder);
	Riudb::factory()->id($downnew['record'])->save($data);

	$this->request->redirect('dev/doc');

	}

	$cats = $this->model->get_all(4);
	$list = View::factory("dev/doc/list")->bind('cats',$cats)->bind('param',$depth);
	$this->template->content = View::factory("dev/doc/index")
			->bind('list',$list)
			->bind('sidebar',$this->sidebar);
	}



	// Listowanie użytkowników.

	public function action_add(){
	
	$this->template->title = $this->title.' - '.__('dev_doc_add_title');
	$this->template->breadcrumb = $this->breadcrumb->add('search',__('dev_doc_add_crumb'))->render();

		if($_POST){
			$post = Validation::factory($_POST)->rule('term', 'not_empty')
			->rule('term', 'min_length', array(':value', '3'))
			->rule('term', 'max_length', array(':value', '32'));
			if ($post->check()){

				$term = html::chars($_POST['term']);
				$slug = url::title($term, $separator = '-', $ascii_only = TRUE);
				$cat = html::chars($_POST['cat']);

				$module = 9;
			
				$record = $this->model->add($module);

				if($cat>1){
					$parent = $cat;
					$depth = 1;
				}
				else{
					$parent = $record;
					$depth = 0;
				}

				$count = $this->model->add_doc($record,$parent,$depth,$term,$slug);


				$pre = $this->model->json_doc();

			Riudb::factory()->id($record)->add(FALSE, $pre);
			$array = array('id'=>$record, 'parent'=>$parent, 'term'=>$term, 'slug'=>$slug, 'depth'=>$depth, 'order'=>$count);
			Riudb::factory()->id($record)->save($array);


			Session::instance()->set('flash',array(array('success','Strona została dodana.')));
			$this->request->redirect('dev/doc/edit/'.$record);
			}
		}
		$parents = $this->model->get_parents(4);
		$this->template->content = View::factory('dev/doc/add')
			->bind('errors', $this->errors)->bind('flash', $this->flash)->bind('parents', $parents)->bind('sidebar',$this->sidebar);
		

	}
	public function action_edit()
	{
	$id = Request::current()->param('id');
	$record = Riudb::factory()->get($id)->join('files')->render();
	if ($this->request->is_ajax())  
	{
		return $this->attachment($id);
	}
	else{


	$this->template->title = $this->title.' - '.__('dev_doc_edit_title').': '.$record[$id]['term'];
	$this->template->breadcrumb = $this->breadcrumb->add('blog',__('dev_doc_edit_crumb').': '.$record[$id]['term'])->render();
	$module = 9;
	if($_POST){

		$post = Validation::factory($_POST)->rule('term', 'not_empty')
			->rule('term', 'min_length', array(':value', '2'))
			->rule('term', 'max_length', array(':value', '64'))
			->rule('post', 'not_empty');

		if ($post->check()){
			$term = html::chars($_POST['term']);
			$slug = url::title($term, $separator = '-', $ascii_only = TRUE);
			$cat = html::chars($_POST['cat']);
			$post = html::chars($_POST['post']);
			$post_json = Markdown::parse($_POST['post']);

			if($cat>1){

				// sprawdznie czy inne subkategoria z `cat` ma już taką nazwę
				$countc = $this->model->count_catsubslug($id,$cat,1,$slug);

				// zmiana na subkategorie
			
				if($cat === $record[$id]['parent']){
				// kategoria główna nie zostaje zmieniona

					if(!empty($countc)){
						 // bład = inn subkategoria już tak sie nazywa
						$this->errors = array('Inna subkategoria już tak sie nazywa.');
					}
					else{
						// nazwa zostaje zmieniona
						$this->model->up_doc($id,$record[$id]['parent'],$record[$id]['depth'],$record[$id]['order'],$term,$slug,$post);
						$array = array('term'=>$term, 'slug'=>$slug, 'post'=>$post_json);
						Riudb::factory()->id($id)->save($array);
						Session::instance()->set('flash',array(array('success','Nazwa kategorii została zmieniona.')));
						$this->request->redirect('dev/doc/edit/'.$id);
					}
				}
				else{
				// kategoria główna jest zmieniona
					if(!empty($countc)){
						 // bład = inn subkategoria już tak sie nazywa
						$this->errors = array('Zmiany nie zostały zapisane, ponieważ inna subkategoria wybranej kategorii już tak sie nazywa.');
					}
					else{
						// nazwa i kategoria nadrzędna zostaje zmieniona
						
						$order = $this->model->count_docs($cat);
						$order++;
						$depth = 1;
						$this->model->up_doc($id,$cat,$depth,$order,$term,$slug,$post);
						$array = array('parent'=>$cat, 'term'=>$term, 'slug'=>$slug, 'order'=>$order, 'depth'=>$depth, 'post'=>$post_json);
						Riudb::factory()->id($id)->save($array);
						if($record[$id]['depth'] ==='1'){
						$this->model->up_subs($record[$id]['parent']);
						}
						else{
						$this->model->up_subs();
						}
						Session::instance()->set('flash',array(array('success','Nazwa i kategoria nadrzędna kategorii zostały zmienione.')));
						$this->request->redirect('dev/doc/edit/'.$id);
						
					}
				}
			
			}
			else{
			// zmiana na kategorie główną

				// sprawdznie czy inne kategoria z `cat` ma już taką nazwę
				$countc = $this->model->count_catslug($id,0,$slug);

				if($record[$id]['id'] === $record[$id]['parent']){

				// przed zmianą była kategorią główną
					if(!empty($countc)){
						 // bład = inn kategoria już tak sie nazywa
						$this->errors = array('Inna kategoria już tak sie nazywa.');
					}
					else{
						// nazwa zostaje zmieniona
						$this->model->up_doc($id,$record[$id]['parent'],$record[$id]['depth'],$record[$id]['order'],$term,$slug,$post);
						$array = array('term'=>$term, 'slug'=>$slug, 'post'=>$post_json);
						Riudb::factory()->id($id)->save($array);
						Session::instance()->set('flash',array(array('success','Nazwa kategorii została zmieniona.')));
						$this->request->redirect('dev/doc/edit/'.$id);
					}
				}
				else{
				// przed zmianą była subkategorią

				// przed zmianą była kategorią główną
					if(!empty($countc)){
						 // bład = inn kategoria już tak sie nazywa
						$this->errors = array('Zmiany nie zostały zapisane ponieważ inna jest już kategoria główna o takiej nazwie.');
					}
					else{
						// nazwa zostaje zmieniona i subkategoria staje się kategorią główną
						$order = $this->model->count_docs();
						$order++;
						$this->model->up_doc($id,$id,0,$order,$term,$slug, $post);
						$array = array('parent'=>$id, 'term'=>$term, 'slug'=>$slug, 'order'=>$order, 'post'=>$post_json);
						Riudb::factory()->id($id)->save($array);
						$this->model->up_subs($record[$id]['parent']);
						Session::instance()->set('flash',array(array('success','Nazwa i poziom kategorii zostały zmieniony.')));
						$this->request->redirect('dev/doc/edit/'.$id);
					}
				}
			}

		}
		else{
		$this->errors = array('Podane dane są nieporawne');
		}
	}	
	$subs = $this->model->count_docs($id);
	$parents = $this->model->get_parents($id);
	$postterm = $this->model->get_postterm($id);
	$this->template->content = View::factory('dev/doc/edit')
			->bind('errors', $this->errors)->bind('flash', $this->flash)
			->bind('record', $record[$id])->bind('parents', $parents)->bind('postterm', $postterm)
			->bind('subs', $subs)->bind('sidebar',$this->sidebar);
	
	}
	}
	public function action_delete()
	{
		$id = Request::current()->param('id');
		$param = Request::current()->param('param');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('dev_doc_delete_title').': '.$record[$id]['term'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('dev_doc_delete_crumb').': '.$record[$id]['term'])->render();
		if($param==='del'){
			$files = $record[$id]['files'];
			foreach($files as $f){
				DB::delete('records')->where('id','=',$f)->and_where('module','=',6)->execute();
				Riudb::factory()->id($f)->delfile()->deldir();
			}

			// właściwe usuwanie rekordu
			DB::delete('records')->where('id','=',$id)->and_where('module','=',9)->execute();
			Riudb::factory()->id($id)->delfile()->deldir();

			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{
			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$subs = $this->model->count_docs($id);
			$this->template->content = View::factory('dev/doc/delete')->bind('record', $record[$id])->bind('user', $this->uid)->bind('subs', $subs)->bind('referrer', $referrer);
		}
	}

	public function action_deletefile()
	{
		$id = Request::current()->param('id');
		$param = Request::current()->param('param');
		$record = Riudb::factory()->get($id)->render();
		$this->template->title = $this->title.' - '.__('dev_doc_deletefile_title').': '.$record[$id]['file'];
		$this->template->breadcrumb = $this->breadcrumb->add('blog',__('dev_doc_deletefile_crumb').': '.$record[$id]['file'])->render();
		if($param==='del'){

			// właściwe usuwanie rekordu
			DB::delete('records')->where('id','=',$id)->and_where('module','=',6)->execute();
			Riudb::factory()->id($id)->delfile()->deldir();
			$newrid = $this->model->get_relations($record[$id]['parent'],6);
			$farray = array('files'=>$newrid);
			Riudb::factory()->id($record[$id]['parent'])->save($farray);		
			$referrer = Session::instance()->get('referrer');
			Request::current()->redirect($referrer);
		}
		else{
			$referrer = Request::current()->referrer();
			Session::instance()->set('referrer',$referrer);
			$this->template->content = View::factory('dev/doc/deletefile')->bind('record', $record[$id])->bind('user', $this->uid)->bind('referrer', $referrer);
		}
	}

	public function attachment($id)
	{

	$model = Model::factory('editor_files');
	if($_FILES){
			$post = new Validation(array_merge($_POST, $_FILES));
			$post->rule('file', 'Upload::not_empty')
			->rule('file', 'Upload::type', array(':value',array('jpg', 'jpeg', 'png', 'doc', 'odt', 'pdf', 'doc', 'mp3', 'flv', 'zip', 'gz', 'tar', 'tar.gz')));
			if ($post->check()){
			$user = Session::instance()->get('uid');
			$module = 6;
			$record = $model->add($module);

			$pre = $model->json_file();
			Riudb::factory()->id($record)->add(TRUE, $pre);

			$file = $_FILES['file'];

			$mime = File::ext_by_mime($file['type']);

			$array = explode('.', $file['name'], -1);
			$comma_separated = implode(" ", $array);
			$title = html::chars($comma_separated);

			$path = Riudb::factory()->id($record)->getpath();
			$filename = Upload::save($_FILES['file'],$filename = NULL,$path,0777);
			$basename = basename($filename);

			$photon = url::title($comma_separated, $separator = '-', $ascii_only = TRUE).'.'.$mime;
			rename($path.'/'.$basename.'', $path.'/'.$photon.'');

			if($mime=='jpg' or $mime=='jpeg'){
			$filename = $path.'/'.$photon;
			$this->gd_data = getimagesize($path.'/'.$photon.'');
			$this->gd_image_out = FALSE;
			$width = $this->gd_data[0];
			$height = $this->gd_data[1];
				if($width > 400 OR $height > 540){
					Image::factory($filename)
					->sharpen(20)
					->resize(400, 540, Image::AUTO)
					->save($path.'/'.$photon.'','85');

					$this->gd_data = getimagesize($path.'/'.$photon.'');
					$this->gd_image_out = FALSE;
					$width = $this->gd_data[0];
					$height = $this->gd_data[1];
					}
					else{
					Image::factory($filename)
					->sharpen(20)
					->save($path.'/'.$photon.'','85');
					}
			}
			else{
			$width = '';
			$height = '';
			}
			$size = filesize($path.'/'.$photon.'');
			$model->attachment_add($record, $user, $photon, $mime, $width, $height, $size);
			$model->add_relations($id,$record,$module);

			$array = array('id'=>$record, 'parent'=>$id, 'module'=>$module, 'user'=>$user, 'file'=>$photon, 'mime'=>$mime, 'width'=>$width, 'height'=>$height, 'size'=>$size);
			Riudb::factory()->id($record)->save($array);

			$newrid = $model->get_relations($id,$module);
			$farray = array('files'=>$newrid);
			Riudb::factory()->id($id)->save($farray);			


			$sizenew = Attach::format_bytes($size);
			$download = 'api/file/'.$record.'/'.$photon;
			$href = url::base().$download;
			$del = url::base().'dev/doc/deletefile/'.$record;
			echo '{"title":"'.$photon.'","mime":"'.$mime.'","size":"'.$sizenew.'","id":"'.$record.'","href":"'.$href.'","download":"'.$download.'","del":"'.$del.'"}';
				
			}
	}
	}

}

