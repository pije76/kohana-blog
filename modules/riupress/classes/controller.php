<?php defined('SYSPATH') OR die('No direct access allowed.');

class Controller extends Kohana_Controller {

	public $loader;

	public function before()
	{
		$directory = Request::initial()->param('directory');
		if($directory !== 'api'){
			$this->loader = Core::factory(true)->param();
		}
	}

}
