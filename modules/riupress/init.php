<?php defined('SYSPATH') or die('No direct script access.');

Route::set('api/index/media', 'api/media(/<file>)', array('file' => '.+'))
	->defaults(array('directory' => 'api', 'controller' => 'index', 'action' => 'media', 'file' => NULL));

Route::set('api/index/themes', 'themes(/<file>)', array('file' => '.+'))
	->defaults(array('directory' => 'api', 'controller' => 'index', 'action' => 'themes', 'file' => NULL));

Route::set('api/index/avatar', 'api/avatar(/<uid>/<file>)', array('uid'  => '[0-9-]+', 'file' => '.+'))
	->defaults(array('directory' => 'api', 'controller' => 'index', 'action' => 'avatar', 'file' => NULL));

Route::set('api/index/file', 'api/file(/<rid>/<file>)', array('rid'  => '[0-9-]+', 'file' => '.+'))
	->defaults(array('directory' => 'api', 'controller' => 'index', 'action' => 'file', 'file' => NULL));

// profil użytkownika

Route::set('profil', 'profil(/index/<page>)',array('action'  => 'index', 'page'  => '[0-9-]+'))
	->defaults(array('directory' => 'default','controller' => 'profil','action' => 'index', 'page'  => '1'));

Route::set('profil/recover', 'profil/recover(/<id>/<key>)',array('id'  => '[0-9-]+', 'key'  => '[0-9A-Za-z-]+'))
	->defaults(array('directory' => 'default', 'controller' => 'profil', 'action' => 'recover'));

Route::set('profil/activate', 'profil/activate/<id>/<key>',array('id'  => '[0-9-]+', 'key'  => '[0-9A-Za-z-]+'))
	->defaults(array('directory' => 'default', 'controller' => 'profil', 'action' => 'activate'));

Route::set('profil/<action>', 'profil/<action>(/<id>)', array('uid'  => 'register|login|pass|avatar', 'id'  => '[0-9-]+'))
	->defaults(array('directory' => 'default', 'controller' => 'profil', 'action' => 'register'));

$directories = 'root|editor|moderator|dev';
// routing editora

Route::set('editor', 'editor(/<page>)',array('directory' => 'editor', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'blog', 'action' => 'index', 'page' => '1'));


Route::set('editor/blog', 'editor/blog(/<page>)',array('directory' => 'editor', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'blog', 'action' => 'index', 'page' => '1'));

Route::set('editor/blog/search', 'editor/blog/search(/<page>)',array('directory' => 'editor', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'blog', 'action' => 'search', 'page' => '1'));

Route::set('editor/tag/search', 'editor/tag/search(/<page>)',array('directory' => 'editor', 'controller'  => 'tag', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'tag', 'action' => 'search', 'page' => '1'));

Route::set('editor/tag', 'editor/tag(/<page>)',array('directory' => 'editor', 'controller'  => 'tag', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'tag', 'action' => 'index', 'page' => '1'));


Route::set('editor/icons/search', 'editor/icons/search(/<page>)',array('directory' => 'editor', 'controller'  => 'icons', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'icons', 'action' => 'search', 'page' => '1'));

Route::set('editor/icons', 'editor/icons(/<page>)',array('directory' => 'editor', 'controller'  => 'icons', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'icons', 'action' => 'index', 'page' => '1'));

Route::set('editor/<controller>/<action>', 'editor/<controller>/<action>(/<id>)(/<param>)', array('directory' => 'editor', 'action'  => 'edit|add|delete|deletefile', 'id'  => '[0-9-]+', 'param'  => '[a-z0-9-]+'))
		->defaults(array('directory' => 'editor', 'controller' => 'index', 'action' => 'index', 'page' => '1'));

// root profil

Route::set('root/profil/edit', 'root/profil/edit/<id>(/<page>(/<param>))', array('directory'  => 'root',  'id'  => '[0-9-]+','page'  => '[0-9-]+','param'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'profil', 'action' => 'edit', 'page' => '1'));

Route::set('root/profil/search', 'root/profil/search(/<page>)',array('directory' => 'root', 'controller'  => 'profil', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'profil', 'action' => 'search', 'page' => '1'));

Route::set('root/profil/email', 'root/profil/email(/<page>)',array('directory' => 'root', 'controller'  => 'profil', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'profil', 'action' => 'email', 'page' => '1'));

Route::set('root/profil', 'root/profil(/<page>)',array('directory' => 'root', 'controller'  => 'profil', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'profil', 'action' => 'index', 'page' => '1'));



Route::set('moderator', 'moderator(/<page>)',array('directory' => 'moderator', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'moderator', 'controller' => 'comments', 'action' => 'index', 'page' => '1'));

Route::set('moderator/comments/<action>', 'moderator/comments/<action>/<id>(/<param>)', array('directory'  => 'moderator',  'action'  => 'edit|add|delete|deletefile', 'id'  => '[0-9-]+', 'param'  => '[a-z-]+'))
		->defaults(array('directory' => 'moderator', 'controller' => 'comments', 'action' => 'edit'));

Route::set('moderator/comments', 'moderator/comments(/<page>)',array('directory' => 'moderator', 'controller'  => 'comments', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'moderator', 'controller' => 'comments', 'action' => 'index', 'page' => '1'));


Route::set('root/poll', 'root/poll(/<page>)',array('directory' => 'editor', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'poll', 'action' => 'index', 'page' => '1'));

Route::set('root/poll/search', 'root/poll/search(/<page>)',array('directory' => 'editor', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'poll', 'action' => 'search', 'page' => '1'));

Route::set('root/poll/edit/<id>', 'root/poll/edit/<id>(/<param>(/<subparam>))', array('directory' => 'root',  'id'  => '[0-9-]+', 'param'  => '[a-z-]+', 'subparam'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'poll', 'action' => 'edit'));


// root docs
Route::set('dev', 'dev(/<param>)',array('directory' => 'dev', 'param'  => '[0-9-]+'))
		->defaults(array('directory' => 'dev', 'controller' => 'doc', 'action' => 'index'));

Route::set('dev/doc/edit', 'dev/doc/edit/<id>(/<page>(/<param>))', array('directory'  => 'dev',  'id'  => '[0-9-]+','page'  => '[0-9-]+','param'  => '[0-9-]+'))
		->defaults(array('directory' => 'dev', 'controller' => 'doc', 'action' => 'edit', 'page' => '1'));

Route::set('dev/doc', 'dev/doc(/<param>)',array('directory' => 'dev', 'controller'  => 'doc', 'param'  => '[0-9-]+'))
		->defaults(array('directory' => 'dev', 'controller' => 'doc', 'action' => 'index'));

Route::set('dev/<controller>/<action>', 'dev/<controller>/<action>(/<id>)(/<param>)', array('directory' => 'dev', 'action'  => 'edit|add|delete|deletefile', 'id'  => '[0-9-]+', 'param'  => '[a-z0-9-]+'))
		->defaults(array('directory' => 'dev', 'controller' => 'index', 'action' => 'index', 'page' => '1'));

// root category

Route::set('root/category/edit', 'root/category/edit/<id>(/<page>(/<param>))', array('directory'  => 'root',  'id'  => '[0-9-]+','page'  => '[0-9-]+','param'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'category', 'action' => 'edit', 'page' => '1'));

Route::set('root/category/search', 'root/category/search(/<page>)',array('directory' => 'root', 'controller'  => 'category', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'category', 'action' => 'search', 'page' => '1'));

Route::set('root/category/email', 'root/category/email(/<page>)',array('directory' => 'root', 'controller'  => 'category', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'category', 'action' => 'email', 'page' => '1'));

Route::set('root/category', 'root/category(/<param>)',array('directory' => 'root', 'controller'  => 'category', 'param'  => '[0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'category', 'action' => 'index'));

Route::set('root/<controller>/<action>', 'root/<controller>/<action>(/<id>)(/<param>)', array('directory' => 'root', 'action'  => 'edit|add|delete|deletefile', 'id'  => '[0-9-]+', 'param'  => '[a-z0-9-]+'))
		->defaults(array('directory' => 'root', 'controller' => 'index', 'action' => 'index', 'page' => '1'));

Route::set('root/settings', 'root/settings(/<action>)',array('action'  => 'database|sender'))
		->defaults(array('directory' => 'root', 'controller' => 'settings', 'action' => 'index'));

Route::set('icons', 'icons')
		->defaults(array('directory' => 'default', 'controller' => 'index', 'action' => 'icons'));

Route::set('<directory>', '<directory>', array('directory'  => $directories))
		->defaults(array('directory' => 'root', 'controller' => 'index', 'action' => 'index'));

Route::set('poll/vote/<id>', 'poll/vote/<id>',array( 'id'  => '[0-9-]+'))
		->defaults(array('directory' => 'default', 'controller' => 'poll', 'action' => 'vote'));

Route::set('tag/<tag>', 'tag/<tag>(/<page>)',array( 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'default', 'controller' => 'blog', 'action' => 'tag', 'page' => '1'));



Route::set('rss', 'rss')
		->defaults(array('directory' => 'default', 'controller' => 'rss', 'action' => 'index'));

Route::set('rss/tag/<tag>', 'rss/tag/<tag>',array('tag'  => '[a-z0-9-]+'))
	->defaults(array('directory' => 'default', 'controller' => 'rss', 'action' => 'tag'));

Route::set('rss/<category>/<subcategory>', 'rss/<category>/<subcategory>',array('category'  => '[a-z-]+', 'subcategory'  => '[a-z-]+'))
	->defaults(array('directory' => 'default', 'controller' => 'rss', 'action' => 'category'));

Route::set('rss/<category>', 'rss/<category>',array('category'  => '[a-z0-9-]+'))
	->defaults(array('directory' => 'default', 'controller' => 'rss', 'action' => 'category'));

Route::set('doc', 'doc')
	->defaults(array('directory' => 'default','controller' => 'doc','action' => 'index'));

Route::set('doc/<param>', 'doc/<param>(/<page>)',array('param'  => '[0-9.-]+', 'page'  => '[0-9-]+'))
	->defaults(array('directory' => 'default','controller' => 'doc','action' => 'doc','page' => '1'));


Route::set('blog', 'blog(/<page>)',array( 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'default', 'controller' => 'blog', 'action' => 'index', 'page' => '1'));

Route::set('blog/search', 'blog/search(/<page>)',array( 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'default', 'controller' => 'blog', 'action' => 'search', 'page' => '1'));

Route::set('blog/<category>/<subcategory>', 'blog/<category>/<subcategory>(/<page>)',array('category'  => '[a-z-]+', 'subcategory'  => '[a-z-]+', 'page'  => '[0-9-]+'))
	->defaults(array('directory' => 'default','controller' => 'blog','action' => 'category','page' => '1'));

Route::set('blog/<category>', 'blog/<category>(/<page>)',array('category'  => '[a-z0-9-]+', 'page'  => '[0-9-]+'))
	->defaults(array('directory' => 'default','controller' => 'blog','action' => 'category','page' => '1'));



Route::set('<id>', '<id>(/<page>)',array( 'id'  => '[0-9-]+', 'page'  => '[0-9-]+'))
		->defaults(array('directory' => 'default', 'controller' => 'blog', 'action' => 'id', 'page' => '1'));

