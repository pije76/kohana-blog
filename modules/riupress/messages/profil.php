<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'name' => array(
		'not_empty' => __('valid_not_empty'),
		'min_length' => __('valid_min_length'),
		'max_length' => __('valid_max_length'),
	),
	'email' => array(
		'email' => __('valid_not_email'),
		'not_empty' => __('valid_not_empty'),
		'email_check' => __('valid_email_check'),
		'useremail_check' => __('valid_useremail_check'),
		'get_recover_mail' => __('valid_useremail_check_yes'),
	),
	'pass' => array(
		'not_empty' => __('valid_not_empty'),
		'min_length' => __('valid_min_length'),
		'max_length' => __('valid_max_length'),
	),
	'oldpass' => array(
		'not_empty' => __('valid_not_empty'),
		'pass_check' => __('valid_profil_bad_pass'),
		'min_length' => __('valid_min_length'),
		'max_length' => __('valid_max_length'),
	),
	'register_yes' => array(
		'not_empty' => __('valid_register_yes'),
	),
	'file' => array(
		'Upload::not_empty' => __('valid_not_empty'),
		'Upload::size' => __('valid_not_size'),
		'Upload::type' => __('valid_not_type_jpg'),
	),

);

