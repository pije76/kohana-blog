<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'nick' => array(
		'not_empty' => __('valid_not_empty'),
		'min_length' => __('valid_min_length'),
		'max_length' => __('valid_max_length'),
	),
	'post' => array(
		'not_empty' => __('valid_not_empty'),
	),
	'captchasum' => array(
		'captcha_check' => 'Podaj poprawną sumę',
	),
);

