<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'term' => array(
		'not_empty' => 'Pole taga nie może być puste',
		'min_length' => 'Tag musi składać się przynajmniej z 3 znaków',
		'max_length' => 'Tag może składać się z maksymalnie 32 znaków',
	),
);

