<?php defined('SYSPATH') or die('No direct script access.');

return array(
	'term' => array(
		'not_empty' => 'Nazwa ikonki nie może być pusta',
		'min_length' => 'Nazwa ikonki musi składać się przynajmniej z 3 znaków',
		'max_length' => 'Nazwa ikonki może składać się z maksymalnie 64 znaków',
	),
	'file' => array(
		'Upload::not_empty' => 'Wybierz plik jpg do stworzenia ikonki',
		'Upload::type' => 'Nazwa ikonki musi składać się przynajmniej z 3 znaków',
		'Upload::size' => 'Nazwa ikonki może składać się z maksymalnie 64 znaków',
	),
);

