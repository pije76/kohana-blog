<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('term', ' Tytuł strony dokumentacji.');
echo Form::input('term', html::charsdecode($postterm['term']), array('placeholder'=>'Tytuł...','class'=>'span7'), TRUE);
echo Form::textarea('post', html::charsdecode($postterm['post']), array('class' => 'span8'));
$main = array('id'=>'1','name'=>'--- rozdział ---');
if(!empty($subs)){
$cats = Arr::select(array($main));
echo Form::label('cat', ' Strona <strong>'.$record['term'].'</strong> podstrony, a więc nie może być tylko rozdziałem. Aby zmienić go w stronę, usuń lub przenieś wszystkie jego strony do innych rozdziałów.');
}
else{
array_unshift($parents, $main);
$cats = Arr::select($parents);
echo Form::label('cat', ' Wybierz rozdział:');
}
echo Form::select('cat', $cats,$record['parent']);
echo Form::button('save', 'Zapisz zmiany', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>

<?php
echo Form::open('dev/doc/edit'.DIRECTORY_SEPARATOR.$record['id'], array('class'=>'cb', 'id'=>'file_upload', 'enctype'=>'multipart/form-data'), TRUE);
echo '<input type="file" name="file" multiple> ';
echo Form::button('save', 'Wyślij<span></span>', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo '<div>wyślij plik</div>';
echo Form::close();
?>
<div class="cb"></div>
<table id="files"></table>
<?php
$c = 0;
//echo Debug::vars($record);
if(empty($record['files'])){
echo '<table class="table table-striped files"><tr class="nofiles"><td>Nie zostały dołączone żadne załączniki.</td></tr></table>';
}
else{
echo '<table class="table table-striped files">';
foreach( $record['files'] as $w )
{

if(is_array($w)){
?>
<tr>
<td><?php echo html::chars($w['file']);  ?>, <?php echo Attach::format_bytes($w['size']); ?><?php if(!empty($w['width'])){ echo ', '.$w['width'].'/'; } ?><?php if(!empty($w['height'])){ echo $w['height'].' px'; } ?></td>
<?php
echo '<td>'.Html::anchor('dev/doc/deletefile/'.$w['id'], '<i class="icon-remove icon-white"></i> Usuń', array('rev'=>'Usunąć plik <strong>'.$w['file'].'</strong> ?', 'class'=>'btn btn-mini btn-danger')).'</td>';
echo '<td>'.Html::anchor('api/file/'.$w['id'].'/'.$w['file'], '<i class="icon-download icon-white"></i> Pobierz', array('class'=>'btn btn-mini btn-info')).'</td>';
//icon-download
?>
<td class="inputfile"><input type="text" value="<?php echo 'api/file/'.$w['id'].'/'.$w['file']; ?>" /></td>
</tr>
<?php
}
}
echo '</table>';
}
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
