<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('term', ' Tytuł strony dokumentacji');
echo Form::input('term', FALSE, array('placeholder'=>'Tytuł...','class'=>'span7'), TRUE);
if($parents){
array_unshift($parents, array('id'=>'1','name'=>'--- rozdział ---'));
$cats = Arr::select($parents);
echo Form::label('cat', ' Wybierz rozdział:');
echo Form::select('cat', $cats);
}
echo Form::button('save', 'Dodaj stronę', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
