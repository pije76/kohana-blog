<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
if(!empty($subs)){
echo '<p>Rozdział <strong>'.$record['term'].'</strong> zawiera strony dokumentacji, a więc nie może zostać usunięty. Usuń wszystkie strony do niego należące by móc go usunąć.</p>';
echo '<p>'.Html::anchor($referrer, '<i class="icon-backward"></i> Wróć', array('class'=>'btn')).'</p>';
}
else{
?>
<p>Czy na pewno chcesz usunąć kategorię <strong><?php echo $record['term']; ?></strong> ?</p>
<p><?php echo Html::anchor('dev/doc/delete/'.$record['id'].'/del', '<i class="icon-remove-sign icon-white"></i> Tak, usuń', array('class'=>'btn btn-success')).' '.Html::anchor($referrer, '<i class="icon-backward"></i> Wróć i nie usuwaj', array('class'=>'btn')); ?></p>
<?php
}

?>
</section>
</section>
