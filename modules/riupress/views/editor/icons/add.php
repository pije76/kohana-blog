<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb', 'enctype' => 'multipart/form-data'), TRUE);
echo Form::open_fieldset();
echo Form::label('term', ' Dodaj nową ikonkę.');
echo Form::input('term', FALSE, array('placeholder'=>'Tytuł ikonki...','class'=>'span7'), TRUE);
echo Form::file('file', NULL, TRUE);
echo Form::hidden('upload');
echo Form::button('save', 'Dodaj ikonkę', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>

</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
