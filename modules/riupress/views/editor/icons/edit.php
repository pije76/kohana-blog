<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL);
echo Form::open_fieldset();
echo Form::label('term', 'Tytuł wiadomości:');
echo Form::input('term', $record['term'], array('placeholder'=>'Tytuł wiadomości...','class'=>'span8'), TRUE);
echo Form::button('save', 'Zapisz<span></span>', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>

