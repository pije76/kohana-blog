<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<p>Czy na pewno chcesz usunąć załącznik: <strong><?php echo $record['file']; ?></strong> ?</p>
<p><?php echo Html::anchor('editor/blog/deletefile/'.$record['id'].'/del', '<i class="icon-remove-sign icon-white"></i> Tak, usuń', array('class'=>'btn btn-success')).' '.Html::anchor($referrer, '<i class="icon-backward"></i> Nie usuwaj', array('class'=>'btn')); ?></p>
</section>
</section>
