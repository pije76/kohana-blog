<?php defined('SYSPATH') OR die('No direct access allowed.'); 
//echo Debug::vars($cats);
?>
<section class="row">
<section class="span8">
<section class="tabbable"> <!-- Only required for left/right tabs -->
  <ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Treść</a></li>
    <li><a href="#tab2" data-toggle="tab">Komentarze</a></li>
    <li><a href="#tab3" data-toggle="tab">Załączniki</a></li>
    <li><a href="#tab4" data-toggle="tab">Ikona</a></li>
    <li><a href="#tab5" data-toggle="tab">Tagi</a></li>
    <li><a href="#tab6" data-toggle="tab">Kategorie</a></li>
    <li><a href="#tab7" data-toggle="tab">Powiązane</a></li>
  </ul>
<section> 
  <section class="tab-content">
    <section class="tab-pane active" id="tab1">
	<?php echo View::factory('editor/blog/edit/post_term')->bind('record',$record)->bind('postterm',$postterm)
		->set('directory','editor')->set('module','blog')->set('action','post'); ?>
    </section>
    <section class="tab-pane" id="tab2">
	<?php echo View::factory('editor/blog/edit/is_comment')->bind('record',$record)
		->set('directory','editor')->set('module','blog')->set('action','comment'); ?>
    </section>

    <section class="tab-pane" id="tab3">
	<?php echo View::factory('editor/blog/edit/attachment')->bind('record',$record)
		->set('directory','editor')->set('module','blog')->set('action','attachment'); ?>
    </section>

    <section class="tab-pane" id="tab4">
	<?php echo View::factory('editor/blog/edit/icon')->bind('record',$record)
		->set('directory','editor')->set('module','blog')->set('action','icon'); ?>
    </section>

    <section class="tab-pane" id="tab5">
	<?php echo View::factory('editor/blog/edit/token_tag')->bind('record',$record)
		->set('directory','editor')->set('module','blog')->set('action','tokentag'); ?>
    </section>

    <section class="tab-pane" id="tab6">
	<?php echo View::factory('editor/blog/edit/categories')->bind('record',$record)->bind('cats',$cats)
		->set('directory','editor')->set('module','blog')->set('action','categories'); ?>
    </section>

    <section class="tab-pane" id="tab7">
	<?php echo View::factory('editor/blog/edit/token_relation')->bind('record',$record)
		->set('directory','editor')->set('module','blog')->set('action','tokenrelation'); ?>
    </section>
  </section>
</section>
</section>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>

