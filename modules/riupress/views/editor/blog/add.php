<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('term', 'Tytuł wpisu:');
echo Form::input('term', FALSE, array('placeholder'=>'Tytuł wiadomości...','class'=>'span7'), TRUE);
echo Form::button('save', 'Rozpocznij nowy wpis', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>

</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
