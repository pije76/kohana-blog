<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span4">
<ul class="nav nav-tabs nav-stacked mb8">
<li><a href="<?php echo url::base() ?>editor/blog/add">Dodaj nową wiadomość</a></li> 
<li><a href="<?php echo url::base() ?>editor/blog">Przeglądaj wszystkie</a></li> 
</ul> 
<?php
$search = Session::instance()->get('search');
?>
<section>
	<h3>Szukaj po tytule</h3>
	<form action="<?php echo url::base() ?>editor/blog/search" method="post">
	<fieldset>
	<input  type="text" id="search" name="search" class="span4" value="<?php  if(!empty($search)){echo $search;} ?>" required />
	</fieldset>
	</form> 
</section>
</aside>
