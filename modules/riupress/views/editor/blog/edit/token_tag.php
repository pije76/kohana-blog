<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<?php
echo Form::open($directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.$action, array('class'=>'cb', 'id'=>'tokens', 'rel'=>$record['id']));
echo Form::open_fieldset();
echo Form::label('token', 'Szukaj tagu:');
echo Form::input('token', FALSE, array('placeholder'=>'Tag...','class'=>'span8', 'id'=>'token'), TRUE);
echo Form::button('save', 'Dodaj wyszukane tagi<span></span>', array('type' => 'submit', 'class' => 'btn btn-success cb', 'style' => 'margin-top: 8px;'));
echo Form::close_fieldset();
echo Form::close();
?>
<ul class="tagi">
<?php
foreach( $record['tags'] as $t )
{
?>
<li><a class="listowany" href="<?php echo url::base() ?>editor/tag/edit/<?php echo $t['id']; ?>"><?php echo html::chars($t['term']); ?></a><a class="fastdelete" rel="<?php echo $t['id']; ?>" href="<?php echo url::base().$directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.'deletetag'; ?>">x</a></li>
<?php
}
?>
</ul>
<?php

echo Form::open($directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.'submitnewtag', array('class'=>'cb', 'id'=>'submitnewtag', 'rel'=>$record['id']));
echo Form::open_fieldset();
echo Form::label('newtag', 'Wpisz, które ma być nowym tagiem:');
echo Form::input('newtag', FALSE, array('placeholder'=>'Tag...','class'=>'span4', 'id'=>'newtag'), TRUE);
echo Form::button('save', 'Dodaj nowy tag<span></span>', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
/*


Former::open(false, 'editor/'.$controller.'/edytuj/'.$record['id'].'/22/'.$record['module'], 'newtoken', 'cb');
Former::openfset('fieldsety bg-grey mt6');
Former::token('newtoken', 'email fl');
?>
<button type="submit" class="action red"><span class="label">dodaj nowy tag</span></button><span class="msg"></span>
<?php
Former::closefset();
Former::close();
*/
?>
