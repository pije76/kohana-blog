<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
echo Form::open($directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.$action, array('class'=>'cb', 'id'=>'file_upload', 'enctype'=>'multipart/form-data'), TRUE);
echo '<input type="file" name="file" multiple> ';
echo Form::button('save', 'Wyślij<span></span>', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo '<div>wyślij plik</div>';
echo Form::close();
?>
<div class="cb"></div>
<table id="files"></table>
<?php
$c = 0;
//echo Debug::vars($record);
if(empty($record['files'])){
echo '<table class="table table-striped files"><tr class="nofiles"><td>Nie zostały dołączone żadne załączniki.</td></tr></table>';
}
else{
echo '<table class="table table-striped files">';
foreach( $record['files'] as $w )
{

if(is_array($w)){
?>
<tr>
<td><?php echo html::chars($w['file']);  ?>, <?php echo Attach::format_bytes($w['size']); ?><?php if(!empty($w['width'])){ echo ', '.$w['width'].'/'; } ?><?php if(!empty($w['height'])){ echo $w['height'].' px'; } ?></td>
<?php
echo '<td>'.Html::anchor('editor/blog/deletefile/'.$w['id'], '<i class="icon-remove icon-white"></i> Usuń', array('rev'=>'Usunąć plik <strong>'.$w['file'].'</strong> ?', 'class'=>'btn btn-mini btn-danger delete', 'id'=>$w['id'])).'</td>';
echo '<td>'.Html::anchor('api/file/'.$w['id'].'/'.$w['file'], '<i class="icon-download icon-white"></i> Pobierz', array('class'=>'btn btn-mini btn-info')).'</td>';
//icon-download
?>
<td class="inputfile"><input type="text" value="<?php echo 'api/file/'.$w['id'].'/'.$w['file']; ?>" /></td>
</tr>
<?php
}
}
echo '</table>';
}
?>
