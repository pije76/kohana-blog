<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
echo Form::open($directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.$action, array('class'=>'cb', 'id'=>'wiadomosc'));
echo Form::open_fieldset();
echo Form::label('term', 'Tytuł wiadomości:');
echo Form::input('term', html::charsdecode($postterm['term']), array('placeholder'=>'Tytuł wiadomości...','class'=>'span8'), TRUE);
echo Form::textarea('post', html::charsdecode($postterm['post']), array('class' => 'span8'));
$publish = strftime('%d-%m-%Y %R', $record['publish']);
echo Form::label('publish', 'Data opublikowania:', array('class' => 'cb'));
echo Form::input('publish', $publish, array('class'=>'span2', 'id'=>'publish'), TRUE);
$options = array( '1'=>'opublikowana', '2'=>'nieopublikowana', '3'=>'do publikacji','4'=>'kopia robocza', '5'=>'zablokowany');
echo Form::label('status', 'Status publikacji:', array('class' => 'cb'));
echo Form::select('status', $options, $record['status']);
echo Form::button('save', 'Zapisz<span></span>', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>

