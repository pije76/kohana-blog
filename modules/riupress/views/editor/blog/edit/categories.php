<?php defined('SYSPATH') OR die('No direct access allowed.'); 
$link = $directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.$action;
if(!empty($cats)){
echo '<table class="table table-striped table-condensed">';
echo '<tbody>';

$newcats = array();
foreach($cats as $a){

	if($a['record']===$a['parent']){
	$newcats[$a['record']] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count'],'subs'=>array());
	}
	else{
	$newcats[$a['parent']]['subs'][] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count']);
	}

}

foreach($newcats as $a){

if(in_array($a['id'],$record['cats'])){
echo '<tr class="activetrcat">';
}
else{
echo '<tr>';
}
echo '<td><h3>'.$a['term'].' <span class="count-'.$a['id'].'">('.$a['count'].')</span></h3></td>
        	<td>'.Html::anchor($link, '<i class="icon-white icon-plus-sign"></i>', array('title'=>'Dodaj do kategorii: '.$a['term'], 'class'=>'btn btn-success btn-mini setcat', 'rel'=>$a['id'].'-1'));
		echo ' '.Html::anchor($link, '<i class="icon-white icon-minus-sign"></i>', array('title'=>'Usuń z kategorii: '.$a['term'], 'class'=>'btn btn-danger btn-mini setcat', 'rel'=>$a['id'].'-2'));
	echo '</td>
          </tr>';


		if(!empty($a['subs'])){
			foreach($a['subs'] as $s){
			if(in_array($s['id'],$record['cats'])){
			echo '<tr class="activetrcat">';
			}
			else{
			echo '<tr>';
			}
			    echo '<td>/ '.$s['term'].' <span class="count-'.$s['id'].'">('.$s['count'].')</span></td>
				<td>'.Html::anchor($link, '<i class="icon-plus-sign icon-white"></i>', array('title'=>'Dodaj do kategorii: '.$s['term'], 'class'=>'btn btn-success btn-mini setcat', 'rel'=>$s['id'].'-1')).' '.Html::anchor($link, '<i class="icon-white icon-minus-sign"></i>', array('title'=>'Usuń z kategorii: '.$s['term'], 'class'=>'btn btn-danger btn-mini setcat', 'rel'=>$s['id'].'-2'));
			echo '</td>
			  </tr>';
			}
		}

}
echo '</tbody></table>';
}
?>
