<?php defined('SYSPATH') OR die('No direct access allowed.');
echo Form::open($directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.$action, array('class'=>'cb', 'id'=>'komentarz'));
echo Form::open_fieldset();
$options = array( '1'=>'włączone/widoczne', '2'=>'włączone/niewidoczne', '3'=>'wyłączone');
echo Form::label('is_comment', 'Status dla komentarzy:', array('class' => 'cb'));
echo Form::select('is_comment', $options, $record['is_comment']);
echo Form::button('save', 'Zapisz<span></span>', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
