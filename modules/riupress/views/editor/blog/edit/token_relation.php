<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>

<?php
echo Form::open($directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.$action, array('class'=>'cb', 'id'=>'rels', 'rel'=>$record['id']));
echo Form::open_fieldset();
echo Form::label('reltoken', 'Szukaj tagu:');
echo Form::input('reltoken', FALSE, array('placeholder'=>'Tytuł powiązanego wpisu..','class'=>'span8', 'id'=>'reltoken', 'rel'=>'reltoken'), TRUE);
echo Form::button('save', 'Dodaj powiązane wpisy<span></span>', array('type' => 'submit', 'class' => 'btn btn-success cb', 'style' => 'margin-top: 8px;'));
echo Form::close_fieldset();
echo Form::close();
?>
<article class="cb">
<ul class="powiazane">
<?php
foreach( $record['relations'] as $t )
{
?>
<li><a class="listowany" href="<?php echo url::base() ?>editor/blog/edit/<?php echo $t['id']; ?>"><?php echo html::chars($t['term']); ?></a><a class="fastdelete" rel="<?php echo $t['id']; ?>" href="<?php echo url::base().$directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.'deleterelation'; ?>">x</a></li>
<?php
}
?>
</ul>
</article>

