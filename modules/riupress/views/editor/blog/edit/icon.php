<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<ul class="ikony cb mb4 oldico">
<?php
if(!empty($record['icon']))
{
?>
<li class="mb4 even"> 
	<dl> 
	<dt><?php echo Icon::icon('large','Ikona',$record['icon'],'oldicon'); ?></dt>
	</dl> 
</li> 
<?php
}
?>
</ul>
<div class="cb"></div>

<?php
echo Form::open($directory.DIRECTORY_SEPARATOR.$module.DIRECTORY_SEPARATOR.'edit'.DIRECTORY_SEPARATOR.$record['id'].DIRECTORY_SEPARATOR.$action, array('class'=>'cb', 'id'=>'icons'));
echo Form::open_fieldset();
echo Form::label('icon', 'Nazwa ikonki:');
echo Form::input('icon', FALSE, array('placeholder'=>'Tytuł ikonki...','class'=>'span6'), TRUE);
echo Form::button('save', 'Szukaj ikony<span></span>', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>

<ul class="ikony cb mt6 mb4 newico">
</ul>
<div class="cb"></div>

