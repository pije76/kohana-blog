<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<p>Czy na pewno chcesz usunąć wpis: <strong><?php echo $record['term']; ?></strong> ?</p>
<p><?php echo Html::anchor('editor/blog/delete/'.$record['id'].'/del', '<i class="icon-remove-sign icon-white"></i> Tak, usuń ten wpis', array('class'=>'btn btn-success')).' '.Html::anchor($referrer, '<i class="icon-backward"></i> Nie usuwaj', array('class'=>'btn')); ?></p>
</section>
</section>
