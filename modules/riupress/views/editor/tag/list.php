<?php defined('SYSPATH') OR die('No direct access allowed.'); 

if($count > 0){
	
	echo '<table class="table table-striped">';
		$time = time();
		foreach( $results as $r )
		{
			echo '<tr>'."\n";
			echo '<td class="termtd"><h3>'.Html::anchor('editor/tag/edit/'.$r['id'],$r['term']).'</h3></td>'."\n";
			echo '<td>'.Html::anchor('editor/tag/edit/'.$r['id'], '<i class="icon-edit icon-white"></i> Zmień', array('class'=>'btn btn-success')).' '.Html::anchor('editor/tag/delete/'.$r['id'], '<i class="icon-remove icon-white"></i> Usuń', array('class'=>'btn btn-danger')).'</td>';
			echo '</tr>'."\n";
		}
	echo '</table>';


	if(!empty($pagination)){
	echo $pagination;
	}

}else{
echo '<article><p>Nie masz jeszcze żadnych tagów. Myślę, że czas najwyższy pomyśleć o jakimś, nieprawdaż?</p></article>';
}

?>
