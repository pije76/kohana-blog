<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<p>Czy na pewno chcesz usunąć komentarz <strong><?php echo $record['nick']; ?></strong> ?</p>
<p><?php echo Html::anchor('moderator/comments/delete/'.$record['id'].'/del', '<i class="icon-remove-sign icon-white"></i> Tak, usuń ten komentarz', array('class'=>'btn btn-success')).' '.Html::anchor($referrer, '<i class="icon-backward"></i> Wróć i nie usuwaj', array('class'=>'btn')); ?></p>
</section>
</section>
