<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL);
echo Form::open_fieldset();
echo Form::label('post', '<strong>'.$comment['nick'].'</strong> napisał(a):');
echo Form::textarea('post', html::charsdecode($comment['post']), array('class' => 'span8'));
$options = array( '1'=>'aktywny', '2'=>'nowy / nieprzeczytany', '3'=>'zablokowany');
echo Form::label('status', 'Status publikacji:', array('class' => 'cb'));
echo Form::select('status', $options, $comment['status']);
echo Form::button('save', 'Zapisz', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</section>
</section>
