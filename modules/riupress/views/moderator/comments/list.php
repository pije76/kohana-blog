<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
if($count > 0){
echo '<table class="table table-striped">';
echo '<tbody>';
//echo Debug::vars($results);
$time = time();
foreach( $results as $r )
{

switch ($r['status']) {
    case 1:
		$status = '<span class="label-'.$r['id'].' label label-success">aktywny</span>';
        break;
    case 2:
		$status = '<span class="label-'.$r['id'].' label  label-warning">nowy / niezatwierdzony</span>';
        break;
    case 3:
	$status = '<span class="label-'.$r['id'].' label label-important">zbanownay</span>';
        break;
}

echo '<tr>
            <td>'.$status.'</td>
            <td><p>'.Avatar::avatar($r['user']['id'], $r['user']['name'], $r['user']['is_avatar'], $r['user']['gender'], 'favicon', false).' <span class="label">'.$r['user']['name'].'</span> <span class="label">'.Date::fullnominativetime($r['publish']).'</span> do <span class="label">'.$r['parent']['term'].'</span></p>'.$r['post'].'</td>
        	<td>'.Html::anchor('moderator/comments/edit/'.$r['id'], '<i class="icon-edit icon-white"></i>', array('title'=>'Edytuj komentarz', 'class'=>'btn btn-success')).' '.Html::anchor('moderator/comments/delete/'.$r['id'], '<i class="icon-white icon-trash"></i>', array('title'=>'Usuń komentarz', 'class'=>'btn btn-danger')).'  '.Html::anchor('moderatort/comments', '<i class="icon-white icon-thumbs-up"></i>', array('title'=>'Zatwierdź komentarz', 'class'=>'btn btn-inverse setstatus', 'rel'=>''.$r['id'].'-1-'.$r['parent']['id'].'')).'  '.Html::anchor('moderator/comments', '<i class="icon-white icon-thumbs-down"></i>', array('title'=>'Zablokuj komentarz', 'class'=>'btn btn-inverse setstatus', 'rel'=>''.$r['id'].'-3-'.$r['parent']['id'].'')).'</td>
          </tr>';


}
echo '        </tbody>
      </table>';
echo $pagination;
}else{
echo '<article class="list listlist">'."\n";
?>
<p>Nie ma jeszcze żadnych komentarzy.</p>
<?php
echo '</article>'."\n";
}
?>
