<?php defined('SYSPATH') OR die('No direct access allowed.'); 

if($count > 0){
	
	echo '<table class="table table-striped">';
	echo '<tbody>';

	foreach( $results as $r )
	{

	echo '<tr>
		    <td><h3>'.Html::anchor('root/poll/edit/'.$r['id'], $r['term'], array('title'=>'Edytuj ankietę: '.$r['term'])).'</h3></td>
			<td>'.Html::anchor('root/poll/edit/'.$r['id'], '<i class="icon-edit icon-white"></i> Edytuj', array('title'=>'Edytuj  ankietę', 'class'=>'btn btn-success')).' '.Html::anchor('root/poll/delete/'.$r['id'], '<i class="icon-white icon-trash"></i> Usuń', array('title'=>'Usuń  ankietę '.$r['term'], 'class'=>'btn btn-danger')).'</td>
		  </tr>';
	}
	echo '</tbody></table>';


	if(!empty($pagination)){
	echo $pagination;
	}


}else{
$action = Request::current()->action();
if($action === 'search'){
echo '<article><p>Nie znaleziono żadnych ankiet.</p></article>';
}
else{
echo '<article><p>Nie masz jeszcze żadnych ankiet. Myślę, że czas najwyższy pomyśleć o jakiejś nieprawdaż?</p></article>';
}
}

?>
