<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span4">
<ul class="nav nav-tabs nav-stacked mb8">
<li><a href="<?php echo url::base() ?>root/poll/add">Dodaj nową ankietę</a></li> 
<li><a href="<?php echo url::base() ?>root/poll">Przeglądaj wszystkie ankiety</a></li> 
</ul> 
<?php
$search = Session::instance()->get('search');
?>
<section>
	<h3>Szukaj ankiety</h3>
	<form action="<?php echo url::base() ?>root/poll/search" method="post">
	<fieldset>
	<input  type="text" id="search" name="search" class="span4" value="<?php  if(!empty($search)){echo $search;} ?>" required />
	</fieldset>
	</form> 
</section>
</aside>
