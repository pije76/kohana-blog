<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<p>Czy na pewno chcesz usunąć ankietę: <strong><?php echo $record['term']; ?></strong> ?</p>
<p><?php echo Html::anchor('root/poll/delete/'.$record['id'].'/del', '<i class="icon-remove-sign icon-white"></i> Tak, usuń tą ankietę', array('class'=>'btn btn-success')).' '.Html::anchor($referrer, '<i class="icon-backward"></i> Nie usuwaj', array('class'=>'btn')); ?></p>
</section>
</section>
