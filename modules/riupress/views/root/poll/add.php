<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('term', 'Pytanie do ankiety');
echo Form::input('term', FALSE, array('placeholder'=>'Pytanie do ankiety...','class'=>'span7'), TRUE);
echo Form::button('save', 'Dodaj nową ankietę', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>

</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
