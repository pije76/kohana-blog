<?php defined('SYSPATH') OR die('No direct access allowed.'); 
//echo Debug::vars($cats);
echo Request::current()->param('subparam');
?>
<section class="row">
<section class="span8">
<article>
<?php
Flash::msg($errors,$flash);
echo Form::open('root/poll/edit/'.$record['id'].'/save');
echo Form::open_fieldset();
echo Form::label('term', 'Pytanie do ankiety');
echo Form::input('term', html::charsdecode($record['term']), array('placeholder'=>'Pytanie do ankiety...','class'=>'span8'), TRUE);
$publish = strftime('%d-%m-%Y %R', $record['publish']);
echo Form::label('publish', 'Data opublikowania:', array('class' => 'cb'));
echo Form::input('publish', $publish, array('class'=>'span2', 'id'=>'publish'), TRUE);
$publish = strftime('%d-%m-%Y %R', $record['expiry']);
echo Form::label('expiry', 'Data zakończenia:', array('class' => 'cb'));
echo Form::input('expiry', $publish, array('class'=>'span2', 'id'=>'expiry'), TRUE);
$options = array( '1'=>'opublikowana', '2'=>'nieopublikowana');
echo Form::label('status', 'Status publikacji:', array('class' => 'cb'));
echo Form::select('status', $options, $record['status']);
echo Form::button('save', 'Zapisz', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>
<article>
<h3>Odpowiedzi:</h3>
<?php
//echo Debug::vars($record['answers']);

if(!empty($record['answers'])){
echo '<table class="table table-striped">';
echo '<tbody>';
foreach($record['answers'] as $a){
echo '<tr>
            <td>';
echo Form::open('root/poll/edit/'.$record['id'].'/edit/'.$a->id, array('class'=>'form-inline'));
echo Form::open_fieldset();
echo Form::input('answer', $a->answer, array('placeholder'=>'Tekst odpowiedzi...','class'=>'span5'), TRUE);
echo ' '.Form::button('save', 'Zapisz', array('type' => 'submit', 'class' => 'btn btn-success'));
echo Form::close_fieldset();
echo Form::close();

		echo '</td>
        	<td>'.Html::anchor('root/poll/edit/'.$record['id'].'/delete/'.$a->id, '<i class="icon-white icon-trash"></i> Usuń', array('title'=>'Usuń '.$a->answer, 'class'=>'btn btn-danger'));
		if($a->order > 1){
		$order = $a->order-1;
		echo ' '.Html::anchor('root/poll/edit/'.$record['id'].'/move/'.$a->id.'-'.$order, '<i class="icon-white icon-arrow-up"></i> Przesuń', array('title'=>'Przesuń do góry ', 'class'=>'btn btn-inverse'));
		}
	echo '</td>
          </tr>';
}
echo '</tbody></table>';
}
?>
</article>
<article>
<h3>Dodaj odpowiedź:</h3>
<?php
echo Form::open('root/poll/edit/'.$record['id'].'/add');
echo Form::open_fieldset();
echo Form::label('answer', 'Odpowiedź');
echo Form::input('answer', FALSE, array('placeholder'=>'Tekst odpowiedzi...','class'=>'span8'), TRUE);
echo Form::button('save', 'Dodaj odpowiedź', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>

