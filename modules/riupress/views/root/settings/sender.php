<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
	echo Form::open(NULL);
	echo Form::open_fieldset();
	echo Form::label('hostname', 'Nazwa hosta:');
	echo Form::input('hostname', $base['options']['hostname'], array('placeholder'=>'domena.pl','class'=>'span3'));
	echo Form::label('username', 'Nazwa użytkownika:');
	echo Form::input('username', $base['options']['username'], array('placeholder'=>'email@domena.pl','class'=>'span3'));
	echo Form::label('password', 'Hasło dla użytkownika:');
	echo Form::input('password', $base['options']['password'], array('placeholder'=>'123admin','class'=>'span3', 'type'=>'password'));
	echo Form::label('port', 'Port:');
	echo Form::input('port', $base['options']['port'], array('placeholder'=>'587','class'=>'span1'));
	echo Form::close_fieldset();
	echo Form::open_fieldset();
	echo Form::button('save', 'Zapisz ustawienia', array('type' => 'submit', 'class' => 'btn btn-success'));
	echo Form::close_fieldset();
	echo Form::close();
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
