<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
	echo Form::open(NULL);
	echo Form::open_fieldset();
	echo Form::label('hostname', 'Nazwa hosta:');
	echo Form::input('hostname', $base['default']['connection']['hostname'], array('placeholder'=>'localhost','class'=>'span3'));
	echo Form::label('database', 'Nazwa bazy:');
	echo Form::input('database', $base['default']['connection']['database'], array('placeholder'=>'user_user','class'=>'span3'));
	echo Form::label('username', 'Nazwa użytkownika bazy:');
	echo Form::input('username', $base['default']['connection']['username'], array('placeholder'=>'user_user','class'=>'span3'));
	echo Form::label('password', 'Hasło dla użytkownika bazy:');
	echo Form::input('password', $base['default']['connection']['password'], array('placeholder'=>'123admin','class'=>'span3', 'type'=>'password'));
	echo Form::close_fieldset();
	echo Form::open_fieldset();
	echo Form::button('save', 'Zapisz ustawienia', array('type' => 'submit', 'class' => 'btn btn-success'));
	echo Form::close_fieldset();
	echo Form::close();
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
