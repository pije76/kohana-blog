<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
	echo Form::open(NULL);
	echo Form::open_fieldset();
	echo Form::label('apptitle', 'Tytuł strony:');
	echo Form::input('apptitle', $base['apptitle'], array('placeholder'=>'Kolejna fajna strona...','class'=>'span3'));
	echo Form::label('admin', 'Nazwa administratora:');
	echo Form::input('admin', $base['admin'], array('placeholder'=>'Jan Kowalski','class'=>'span3'));
	echo Form::label('mail', 'Email administratora:');
	echo Form::input('mail', $base['mail'], array('placeholder'=>'email@domena.pl','class'=>'span3'));
	echo Form::close_fieldset();
	echo Form::open_fieldset();
	echo Form::button('save', 'Zapisz ustawienia', array('type' => 'submit', 'class' => 'btn btn-success'));
	echo Form::close_fieldset();
	echo Form::close();
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
