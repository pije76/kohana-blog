<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span4">
<ul class="nav nav-tabs nav-stacked mb8">
<li><a href="<?php echo url::base() ?>root/settings">Ustawienia główne</a></li> 
<li><a href="<?php echo url::base() ?>root/settings/database">Ustawienia bazy danych</a></li> 
<li><a href="<?php echo url::base() ?>root/settings/sender">Obsługa wysyłania poczty</a></li> 
</ul> 
</aside>
