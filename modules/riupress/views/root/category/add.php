<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('term', ' Nazwa kategorii.');
echo Form::input('term', FALSE, array('placeholder'=>'Kategoria...','class'=>'span7'), TRUE);
if($parents){
array_unshift($parents, array('id'=>'1','name'=>'--- kategoria główna ---'));
$cats = Arr::select($parents);
echo Form::label('cat', ' Wybierz kategorię:');
echo Form::select('cat', $cats);
}
echo Form::button('save', 'Dodaj kategorię', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
