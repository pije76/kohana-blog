<?php defined('SYSPATH') OR die('No direct access allowed.'); 

if(!empty($cats)){
echo '<table class="table table-striped">';
echo '<thead>
          <tr>
            <th>id</th>
            <th>nazwa</th>
            <th>akcje</th>
          </tr>
        </thead>
	<tbody>';

$newcats = array();
foreach($cats as $a){

	if($a['record']===$a['parent']){
	$newcats[$a['record']] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'subs'=>array());
	}
	else{
	$newcats[$a['parent']]['subs'][] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth']);
	}

}

foreach($newcats as $a){

echo '<tr>
            <td>'.$a['id'].'</td>
            <td><h3>'.Html::anchor('root/category/edit/'.$a['id'], $a['term'], array('title'=>'edytuj '.$a['term'])).'</h3></td>
        	<td>'.Html::anchor('root/category/edit/'.$a['id'], '<i class="icon-edit icon-white"></i> Edytuj', array('title'=>'Edytuj '.$a['term'], 'class'=>'btn btn-success'));
		if(empty($a['subs'])){
		echo ' '.Html::anchor('root/category/delete/'.$a['id'], '<i class="icon-white icon-trash"></i> Usuń', array('title'=>'Usuń '.$a['term'], 'class'=>'btn btn-danger'));
		}
		if($a['order']>1){
		$order = $a['order']-1;
		echo ' '.Html::anchor('root/category/'.$a['depth'].'-'.$a['id'].'-'.$order, '<i class="icon-white icon-arrow-up"></i> Przesuń', array('title'=>'Przesuń do góry ', 'class'=>'btn btn-inverse'));
		}
	echo '</td>
          </tr>';


		if(!empty($a['subs'])){
			foreach($a['subs'] as $s){
		echo '<tr>
			    <td>'.$s['id'].'</td>
			    <td>/ '.Html::anchor('root/category/edit/'.$s['id'], $s['term'], array('title'=>'edytuj '.$s['term'])).'</td>
				<td>'.Html::anchor('root/category/edit/'.$s['id'], '<i class="icon-edit icon-white"></i> Edytuj ', array('title'=>'Edytuj '.$s['term'], 'class'=>'btn btn-success btn-mini')).' '.Html::anchor('root/category/delete/'.$s['id'], '<i class="icon-white icon-trash"></i> Usuń', array('title'=>'Usuń '.$s['term'], 'class'=>'btn btn-danger btn-mini'));
				if($s['order']>1){
				$sorder = $s['order']-1;
				echo ' '.Html::anchor('root/category/'.$s['depth'].'-'.$s['id'].'-'.$sorder, '<i class="icon-white icon-arrow-up"></i> Przesuń', array('title'=>'Przesuń do góry ', 'class'=>'btn btn-inverse btn-mini'));
				}
			echo '</td>
			  </tr>';
			}
		}

}
echo '</tbody></table>';
}
?>
