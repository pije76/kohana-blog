<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('term', ' Nazwa kategorii.');
echo Form::input('term', $record['term'], array('placeholder'=>'Kategoria...','class'=>'span7'), TRUE);
$main = array('id'=>'1','name'=>'--- kategoria główna ---');
if(!empty($subs)){
$cats = Arr::select(array($main));
echo Form::label('cat', ' Kategoria <strong>'.$record['term'].'</strong> zawiera podkategorię, a więc nie może być tylko kategorią główną. Aby zmienić ją w podkategorię, usuń lub przenieś wszystkie jej podkategorie do innych kategorii głównych.');
}
else{
array_unshift($parents, $main);
$cats = Arr::select($parents);
echo Form::label('cat', ' Wybierz kategorię:');
}
echo Form::select('cat', $cats,$record['parent']);
echo Form::button('save', 'Zapisz zmiany', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
