<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span4">
<ul class="nav nav-tabs nav-stacked mb8">
<li><a href="<?php echo url::base() ?>root/profil">Przeglądaj profile</a></li> 
</ul> 
<?php
$search = Session::instance()->get('search');
$email = Session::instance()->get('email');
?>
<section>
	<h4>Szukaj użytkownika po nazwie:</h4>
	<form action="<?php echo url::base() ?>root/profil/search" method="post">
	<fieldset>
	<input  type="text" id="search" name="search" class="span4" value="<?php  if(!empty($search)){echo $search;} ?>" required />
	</fieldset>
	</form> 
</section>
<section>
	<h4>Szukaj użytkownika po adresie email:</h4>
	<form action="<?php echo url::base() ?>root/profil/email" method="post">
	<fieldset>
	<input  type="text" id="email" name="email" class="span4" value="<?php  if(!empty($email)){echo $email;} ?>" required />
	</fieldset>
	</form> 
</section>
</aside>
