<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<?php
if($count > 0){

echo '<table class="table table-striped">';
echo '<thead>
          <tr>
            <th>id</th>
            <th>avatar</th>
            <th>nazwa, email i status</th>
            <th>akcje</th>
          </tr>
        </thead>
	<tbody>';
//echo Debug::vars($results);
$time = time();
foreach( $results as $r )
{
if($r['is_ban']===1){
	$status = '<span class="label label-important">zbanownay</span>';
}
else{
	if($r['is_active']==='1'){
		$status = '<span class="label label-success">aktywny</span>';
	}
	else{
		$status = '<span class="label">nieaktywny</span>';
	}
}
if(!empty($r['email'])){
$email = '<span class="label">'.$r['email'].'</span> ';
}
else{
$email ='';
}
echo '<tr>
            <td>'.$r['user'].'</td>
            <td>'.Avatar::avatar($r['user'], $r['name'], $r['is_avatar'], $r['gender'], 'small', false).'</td>
            <td><h3>'.Html::anchor('root/profil/edit/'.$r['user'], $r['name'], array('title'=>'edytuj profil'.$r['name'])).'</h3>'.$email.$status.'</td>
        	<td>'.Html::anchor('root/profil/edit/'.$r['user'], '<i class="icon-edit icon-white"></i> Edytuj profil', array('title'=>'Edytuj profil '.$r['login'], 'class'=>'btn btn-success')).' '.Html::anchor('root/profil/delete/'.$r['user'], '<i class="icon-white icon-trash"></i> Usuń profil', array('title'=>'Usuń profil '.$r['name'], 'class'=>'btn btn-danger')).'</td>
          </tr>';
}
echo '        </tbody>
      </table>';
echo $pagination;
}else{
echo '<article class="list listlist">'."\n";
?>
<p>Nie znaleziono żadnych użytkowników.</p>
<?php
echo '</article>'."\n";
}
?>
