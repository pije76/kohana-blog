<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<p>Czy na pewno chcesz usunąć profil <strong><?php echo $record['name']; ?></strong> ?</p>
<p><?php echo Html::anchor('root/profil/delete/'.$record['id'].'/del', '<i class="icon-remove-sign icon-white"></i> Tak, usuń ten profil', array('class'=>'btn btn-success')).' '.Html::anchor($referrer, '<i class="icon-backward"></i> Wróć i nie usuwaj', array('class'=>'btn')); ?></p>
</section>
</section>
