<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<section class="row">
<section class="span2">
<?php echo Avatar::avatar($results['user'], $results['name'], $results['is_avatar'], $results['gender'], 'large', false); ?>
</section>
<section class="span6">
<h1><?php echo $results['name']; ?></h1>
<table class="table table-striped table-condensed">
<tbody>
<?php
//echo Debug::vars($results);
$time = time();
if($results['gender']==1){
	$gender = "mężczyzna";
}
else{
	$gender = "kobieta";
}

if($results['is_ban']==='1'){
	$status = '<span class="label label-important">zbanownay</span>';
}
else{
	if($results['is_active']==='1'){
		$status = '<span class="label label-success">aktywny</span>';
	}
	else{
		$status = '<span class="label">nieaktywny</span>';
	}
}
?>

<tr>
	<td>email</td>
	<td><?php echo $results['email']; ?></td>
</tr>
<tr>
	<td>status</td>
	<td><?php echo $status; ?></td>
</tr>
<tr>
	<td>id</td>
	<td><?php echo $results['user']; ?></td>
</tr>
<tr>
	<td>płeć</td>
	<td><?php echo $gender; ?></td>
</tr>
<tr>
	<td>utworzono</td>
	<td><?php echo Date::distanceOfTimeInWords($time, $results['joined']); ?></td>
</tr>
<tr>
	<td>logowań</td>
	<td><?php echo $results['summary_login']; ?></td>
</tr>
<tr>
	<td>aktualizacja</td>
	<td><?php echo $results['update']; ?></td>
</tr>
</tbody>
</table>
<section class="row">
<section class="span3">
<?php

echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('is_active', 'Czy jest aktywny?');
$is_actives = array(1 => 'aktywny' , 2 => 'nieaktywny');
echo Form::select('is_active', $is_actives, $results['is_active']);
echo Form::label('is_ban', 'Czy jest aktywny?');
$is_bans = array(1 => 'zbanowany' , 2 => 'niezbanownay');
echo Form::select('is_ban', $is_bans, $results['is_ban']);
echo Form::button('save', 'Zapisz zmiany', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();

?>
</section>

<section class="span3">
<h3>Posiadane uprawnienia:</h3>
<?php
$cm = count($members);
if($cm > 0){
echo '<table class="table table-striped table-condensed"><tbody>';
foreach($members as $m){

?>
<tr>
<td><h4><?php echo __('role_directory_'.$m['role']); ?></h4></td>
<td><?php echo Html::anchor('root/profil/edit/'.$m['user'].'/2/'.$m['id'], '<i class="icon-white icon-trash"></i> Usuń rolę</a>', array('class'=>'btn btn-small btn-danger pull-right')); ?></td>
</tr>
<?php
}
echo '</tbody></table>';
}
?>
<?php
//echo Debug::vars($roles);
if(!empty($roles)){
$new_array = array();
foreach ($roles as $a)
{
		    $new_array[$a['id']] = __('role_directory_'.$a['name']);
}
echo Form::open('root/profil/edit/'.$results['user'].'/2', array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('role', 'Wybierz i dodaj rolę');
echo Form::select('role', $new_array );
echo Form::button('save', 'Dodaj rolę', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
}
?>
</section>
</section>
</section>
</section>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
