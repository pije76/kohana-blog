<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<article class="clearfix">
<?php 
echo Blog::icon('small',$record['term'],$record['icon']);
echo '<h1>'.$record['term'].'</h1>'."\n";
echo '<p class="pmeta"><span>'.Date::fullnominativetime($record['publish']).'</span></p>';
echo Blog::full($record['post'],$page);
?>
</article>

<?php
echo '<section class="row article">';
if(!empty($record['relations'])){
echo '<section class="span8">';
echo Blog::relations($record['relations']);
echo '</section>';
}
echo '<section class="span4">';
echo Blog::tags($record['tags']);
echo '</section>';
echo '<section class="span4">';
echo Blog::rating($record['id'],$uid,$record['id'],$record['sum_rating'],$record['ratings'])."\n";
echo '</section>';
echo '</section>';	
echo '<ul class="pager">';
if(!empty($next)){
			echo '<li class="previous">'.Html::anchor($next['record'], '&laquo; '.$next['term'], array('title'=>$next['term'])).'</li>';
}
if(!empty($prev)){
			echo '<li class="next">'.Html::anchor($prev['record'], $prev['term'].' &raquo;', array('title'=>$prev['term'])).'</li>';
}

echo '</ul>';



		if($record['count_comment']>0){
		echo '<section id="comments">'."\n";
		echo '<h2>Komentarze <small> ( '.$record['count_comment'].' )</small></h2>'."\n";
			if(!empty($record['comments'])){
				foreach( $record['comments'] as $data )
				{
		echo '<article class="comment clearfix" id="'.$data['id'].'">'."\n";
		echo '<header>'."\n";
		echo Avatar::avatar($data['user']['id'], $data['user']['name'], $data['user']['is_avatar'], $data['user']['gender'], 'small', false);
		echo '</header>'."\n";
		echo '<section>'."\n";
		echo '<p class="pmeta"><span class="author">'.$data['user']['name'].'</span> <span>';
		echo Date::fullnominativetime($data['publish']);
		echo '</span></p>'."\n";
		echo $data['post']."\n";
		echo '</section>'."\n";
		echo '</article>'."\n";
				}
			}
		echo '</section>'."\n";
		}

		if(!empty($uid) AND $uid>1 AND $record['is_comment']==='1'){

		//echo '<section id="comment">';
		Flash::msg($errors,$flash);
		echo '<form id="commentform" action="'.url::base().$record['id'].'" method="post">
		<fieldset>
		<label>Dodaj nowy komentarz</label>
		<input type="hidden" name="nick" value="login" required />
		<textarea class="post span8" id="post" name="post" required ></textarea>
		<button type="submit" class="btn btn-success">Dodaj komentarz</button>
		</fieldset>
		</form>';

		}
		else{
		Flash::msg($errors,$flash);
		echo '<form id="commentform" action="'.url::base().$record['id'].'" method="post">
		<fieldset>
		<label>Przedstaw się:</label>
		<input type="text" name="nick" value="';
		if(!empty($_POST['nick'])){ echo $_POST['nick']; }
		echo '" id="nick"/>
		<label>Dodaj nowy komentarz:</label>
		<textarea class="post span8" id="post" name="post" >';
		if(!empty($_POST['post'])){ echo $_POST['post']; }
		echo '</textarea>';
		Matchcaptcha::get();
		echo '<button type="submit" class="btn btn-success cb">Dodaj komentarz</button>
		</fieldset>
		</form>';
		echo '<section class="clearfix">'."\n";
		echo '<article class="comment"><p class="nologin">Chcesz napisać komentarz? Potrzebujesz być <a href="'.url::base().'profil/login" title="Zaloguj mnie!">zalogowany(a)</a> aby dodać komentarz. Jeśli nie posiadasz konta możesz sie <a href="'.url::base().'profil/register" title="Załóż nowe konto!">zarejestrować</a>.</p></article>';
		echo '</section>'."\n";
		}



?>	


</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
