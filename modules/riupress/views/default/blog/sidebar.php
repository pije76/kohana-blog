<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span4">
<?php
if(!empty($cats)){
echo '<ul class="nav nav-tabs nav-stacked">';

$newcats = array();
foreach($cats as $a){

	if($a['record']===$a['parent']){
	$newcats[$a['record']] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count'],'subs'=>array());
	}
	else{
	$newcats[$a['parent']]['subs'][] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count']);
	}

}

foreach($newcats as $a){

	echo '<li><a class="menu" href="'.url::base().'blog'.DIRECTORY_SEPARATOR.$a['slug'].'">'.$a['term'].' <span>('.$a['count'].')</span></a>';


		if(!empty($a['subs'])){
			echo '<ul>';
			foreach($a['subs'] as $s){
			echo '<li><a href="'.url::base().'blog'.DIRECTORY_SEPARATOR.$a['slug'].DIRECTORY_SEPARATOR.$s['slug'].'">'.$s['term'].' <span>('.$s['count'].')</span></a></li>';

			}
			echo '</ul>';
		}
	echo '</li>';

}
echo '</ul>';
}
?>
<?php
if(!empty($poll)){
//echo Debug::vars($poll);
echo '<h6>Ankieta</h6>';
//echo $uid;

echo '<h3>'.$poll['term'].'</h3>';
echo '<section>';
if(!empty($poll['answers'])){
if(in_array($uid,$poll['votes']) OR $uid === 1){
	$votes = 0;
	foreach($poll['answers'] as $a){
		$votes = $votes+$a->votes;
	}
echo '<ul class="poll clearfix"">';
	foreach($poll['answers'] as $a){
	$percent = ($a->votes*100)/$votes;
	$percent = round($percent, 0);
	echo '<li>'.$a->answer.' <small>( '.$percent.'% )</small>
	<section class="progress progress-striped progress-success mb4"><section class="bar" style="width: '.$percent.'%;"></section></section></li>';
	}
echo '</ul>';
}
else{
echo Form::open('poll/vote/'.$poll['id']);
echo Form::open_fieldset();
echo '<ul class="poll clearfix">';
foreach($poll['answers'] as $a){
echo '<li>'.Form::radio('answer', $a->id, false,array('class'=>'pull-left')).'<span> '.$a->answer.'</span></li>';
}
echo '</ul>';
echo Form::button('save', 'Głosuj', array('type' => 'submit', 'class' => 'btn btn-success cb'));
echo Form::close_fieldset();
echo Form::close();
}
}
echo '</section>';

}

$search = Session::instance()->get('search');
?>


<section>
	<h6>Szukaj na blogu</h6>
	<form action="<?php echo url::base() ?>blog/search" method="post">
	<fieldset>
	<input  type="text" id="search" name="search" class="span4" value="<?php  if(!empty($search)){echo $search;} ?>" required />
	</fieldset>
	</form> 
</section>
</aside>
