<?php defined('SYSPATH') OR die('No direct access allowed.');
//echo Debug::vars($results);
echo '<?xml version="1.0" encoding="UTF-8"?'.'>'; ?>
<rss version="2.0">
<channel>
	<title><?php echo $rsstitle; ?></title>
	<link><?php echo $rsslink; ?></link>
	<description><?php echo $rssdesc; ?></description>
    <generator>Riupress</generator>
<?php
foreach( $results as $i )
{
//echo Debug::vars(Blog::meta($i['post']));
?>
	<item>

      <author><?php echo $i['user']['name']; ?></author>
      <pubDate><?php echo date('r', $i['publish']); ?></pubDate>
      <title><![CDATA[<?php echo $i['term']; ?>]]></title>
      <description><![CDATA[<?php echo Blog::torss($i['post']); ?>]]></description>
      <link><?php echo url::base().$i['id']; ?></link>
      <guid><?php echo url::base().$i['id']; ?></guid>
	</item>
<?php
}
?>
</channel>
</rss>

