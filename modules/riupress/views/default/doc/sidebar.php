<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span4">
<?php
if(!empty($docs)){
echo '<ul class="nav nav-tabs nav-stacked">';
//echo $active;
$newcats = array();
foreach($docs as $a){

	if($a['record']===$a['parent']){
	$newcats[$a['record']] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count'],'subs'=>array());
	}
	else{
		if($a['parent'] == $active){
		$newcats[$a['parent']]['subs'][] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count']);
		}
	}
}

foreach($newcats as $a){

	echo '<li><a class="menu" href="'.url::base().'doc'.DIRECTORY_SEPARATOR.$a['order'].'">'.$a['order'].'. '.$a['term'].'</a>';


		if(!empty($a['subs'])){
			echo '<ul>';
			foreach($a['subs'] as $s){
			echo '<li><a href="'.url::base().'doc'.DIRECTORY_SEPARATOR.$a['order'].'.'.$s['order'].'">'.$a['order'].'.'.$s['order'].'. '.$s['term'].'</a></li>';

			}
			echo '</ul>';
		}
	echo '</li>';

}
echo '</ul>';
}
?>
</aside>
