<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<?php
if(!empty($docs)){
$newcats = array();
foreach($docs as $a){

	if($a['record']===$a['parent']){
	$newcats[$a['record']] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count'],'subs'=>array());
	}
	else{
	$newcats[$a['parent']]['subs'][] = array('id'=>$a['record'],'slug'=>$a['slug'],'term'=>$a['term'],'order'=>$a['order'],'depth'=>$a['depth'],'count'=>$a['count']);
	}

}
//echo Debug::vars($newcats);

foreach($newcats as $a){
	echo '<article>';
	echo '<h2><a class="menu" href="'.url::base().'doc'.DIRECTORY_SEPARATOR.$a['order'].'">'.$a['order'].'. '.$a['term'].'</a></h2>';


		if(!empty($a['subs'])){
			echo '<ul>';
			foreach($a['subs'] as $s){
			echo '<li><a href="'.url::base().'doc'.DIRECTORY_SEPARATOR.$a['order'].'.'.$s['order'].'">'.$a['order'].'.'.$s['order'].'. '.$s['term'].'</a></li>';

			}
			echo '</ul>';

		}
	echo '</article>';
}
}
else{
	echo '<article><p>Niestety, ale dokumentacja nie zawiera jeszcze żadnych dokumentów.</p></article>';
}
?>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
