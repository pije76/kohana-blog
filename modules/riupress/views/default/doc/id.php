<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<article class="clearfix">
<?php 
echo '<h1>'.$record['term'];
if(!empty($subrecord['term'])){
echo ' <small>/ '.$subrecord['term'].'</small>';
}
echo '</h1>'."\n";
$newcats = array();
foreach($docs as $a){

	if($a['record']===$a['parent']){
	$newcats[$a['record']] = array('id'=>$a['record'],'term'=>$a['term'],'order'=>$a['order'],'subs'=>array());
	}
	else{
	$newcats[$a['parent']]['subs'][] = array('id'=>$a['record'],'term'=>$a['term'],'order'=>$a['order']);
	}

}
$all = array();
$i = 0;
foreach($newcats as $a){
	$all[] =  array('id'=>$a['id'],'term'=>$a['term'],'order'=>$a['order']);
	if($record['id'] == $a['id']){
	$current = $i;
	}
	$i++;
	if(!empty($a['subs'])){
		foreach($a['subs'] as $b){
		if(!empty($subrecord) AND $subrecord['id'] == $b['id']){
		$current = $i;
		}
		$all[] = array('id'=>$b['id'],'term'=>$b['term'],'order'=>$a['order'].'.'.$b['order']);
		$i++;
		}
	}
}

if(!empty($subrecord)){
echo Blog::full($subrecord['post'],$page);
}
else{
echo Blog::full($record['post'],$page);
}

?>
</article>

<?php
$prev = $current-1;
$next = $current+1;
//echo Debug::vars($next);
echo '<ul class="pager">';

if(!empty($all[$prev])){
			echo '<li class="previous">'.Html::anchor('doc/'.$all[$prev]['order'], '&laquo; '.$all[$prev]['order'].'. '.$all[$prev]['term'], array('title'=>$all[$prev]['term'])).'</li>';
}
if(!empty($all[$next])){
			echo '<li class="next">'.Html::anchor('doc/'.$all[$next]['order'], $all[$next]['order'].'. '.$all[$next]['term'].' &raquo;', array('title'=>$all[$next]['term'])).'</li>';
}
echo '</ul>';

?>	
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
