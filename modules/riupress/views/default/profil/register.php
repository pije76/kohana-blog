<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span12">
<article>
<h2>Stwórz nowy profil</h2>
<?php
Flash::msg($errors);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('name', 'Imię i nazwisko (lub nick):');
echo Form::input('name', $fields['name'], array('placeholder'=>'Imię i nazwisko...','class'=>'span4'), TRUE);
echo Form::label('email', 'Twój email:');
echo Form::input('email', $fields['email'], array('placeholder'=>'Email...','type'=>'email','class'=>'span4'), TRUE);
echo Form::label('pass', 'Hasło do konta:');
echo Form::input('pass', FALSE, array('placeholder'=>'Hasło...','type'=>'password','class'=>'span3'), TRUE);
$genders = array(1 => 'on' , 2 => 'ona');
echo Form::label('gender', 'Płeć:');
echo Form::select('gender', $genders, $fields['gender'], array('class'=>'span1'));
echo Form::button('save', 'Załóż nowe konto', array('type' => 'submit', 'class' => 'btn btn-success cb'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>
</section>

<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
