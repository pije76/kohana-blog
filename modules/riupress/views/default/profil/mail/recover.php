<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Link aktywacyjny dla nowego hasła</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
Witaj, <?php echo $name; ?><br />
Mam przyjemność poinformować Ciebie, iż zgodnie z Twoim życzniem na stronie <a href="<?php echo url::base(); ?>"><?php echo url::base(); ?></a> został wygenerowany link (poniżej), dzięki któremu zmienisz swoje hasło na nowe. <br /><br />

<a href="<?php echo $path; ?>">Ustaw nowe hasło</a>
<br /><br />
Dziękuje i pozdrawiam.<br />
<br />
<?php echo $admin_name; ?><br />
<?php echo $admin_mail; ?><br /><br />
</body>
</html>

