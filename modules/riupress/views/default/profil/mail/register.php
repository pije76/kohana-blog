<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Rejestracja</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body>
<strong>Witaj, <?php echo $name; ?></strong><br />
Mam przyjemność poinformować Ciebie, iż Twoja rejestracja na stronie <a href="<?php echo url::base(); ?>"><?php echo url::base(); ?></a> przebiegła pomyślnie. Jedyne co jeszcze musisz zrobić to potwierdzić otrzymanie maila, którego teraz czytasz, za pomocą kliknięcia w poniższy link: <br /><br />
<a href="<?php echo $path; ?>">Potwierdź moją rejestrację!</a>
<br />
Po aktywacji możesz zalogować się korzystając z danych podanych podczas rejestracji.<br />
<br />
Dziękuje i pozdrawiam.
<br /><br />
<?php echo $admin_name; ?><br />
<?php echo $admin_mail; ?><br /><br />
Jeśli nie rejestrowałeś(aś) konta na w/w stronie www, bardzo proszę zignoruj tą wiadomość.
</body>
</html>

