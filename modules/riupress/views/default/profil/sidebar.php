<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span12">
<p>Nie masz jeszcze konta? <a href="<?php echo url::base(); ?>profil/register" title="Załóż nowy profil!">Załóż nowy profil!</a>. <a href="<?php echo url::base(); ?>profil/login" title="Zaloguj się">Zaloguj się</a> lub <a href="<?php echo url::base(); ?>profil/recover" title="Przypomnij hasło!">przypomnij hasło!</a>.</p> 
</aside>

