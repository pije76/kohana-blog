<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<article>
<h2>Avatar</h2>
<p>Avatar musi być w formacie .jpg, maksymalny rozmiar: 200 x 200 px, maksymalna waga: 150 kb. Jeśli chcesz ustawić avatar domyślny <a href="<?php echo url::base() ?>profil/avatar/1">kliknij tutaj</a>.</p>
<?php
Flash::msg($errors,$flash);

if($data['me']['is_avatar']==='1'){
echo '<p>';
echo Avatar::avatar($data['me']['id'], $data['me']['name'], $data['me']['is_avatar'], $data['me']['gender'], 'large', false);
echo '</p>';
}
?>
<?php
echo Form::open(NULL, array('class'=>'cb mt8', 'enctype' => 'multipart/form-data'));
echo Form::open_fieldset();
echo Form::file('file');
echo Form::button('save', 'Załaduj zdjęcie', array('type' => 'submit', 'class' => 'btn btn-success cb'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>

