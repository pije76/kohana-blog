<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<article>
<h2>Ustawienie nowego hasła</h2>
<p>Witaj <?php echo $data['name'] ?>, teraz możesz ustawić nowe hasło dla swojego konta.</p>
<?php
$s = Session::instance()->get('flash');
echo Debug::vars($s);
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb mt8'));
echo Form::open_fieldset();
echo Form::label('pass', 'Twoje nowe hasło:');
echo Form::input('pass', FALSE, array('placeholder'=>'Nowe hasło...','type'=>'password','class'=>'span3'), TRUE);
echo Form::button('save', 'Zmień hasło', array('type' => 'submit', 'class' => 'btn btn-success cb'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
