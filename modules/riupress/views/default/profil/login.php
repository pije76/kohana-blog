<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span12">
<article>
<h2>Logowanie</h2>
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('email', 'Email:');
echo Form::input('email', FALSE, array('placeholder'=>'Email...','class'=>'span4'), TRUE);
echo Form::label('pass', 'Hasło:');
echo Form::input('pass', FALSE, array('placeholder'=>'Hasło...','type'=>'password','class'=>'span3'), TRUE);
$token = Form::input('token', 1, array('type'=>'checkbox'));
echo Form::label('token', ''.$token.' Zapamiętaj mnie na tym komputerze.', array('class'=>'checkbox'));
echo Form::button('save', 'Zaloguj', array('type' => 'submit', 'class' => 'btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
