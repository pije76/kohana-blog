<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<aside class="span4">
<?php
$action = Request::current()->action();
$array = array(
	9 => array('action'=>'index', 'title'=>'Imię, nazwisko, płeć', 'class'=>'icon-user'),
	10 => array('action'=>'pass', 'title'=>'Zmiana hasła', 'class'=>'icon-refresh'), 
	11 => array('action'=>'avatar', 'title'=>'Zmiana avatara', 'class'=>'icon-picture'), 
);


?>
<ul class="nav nav-tabs nav-stacked mb2">
<?php
foreach($array as $a){
?>
<li<?php if($action==$a['action']) {echo ' class="active"'; } ?>><a href="<?php echo url::base(); ?>profil<?php echo DIRECTORY_SEPARATOR.$a['action']; ?>"><i class="<?php echo $a['class']; ?>"></i> <?php echo $a['title']; ?></a></li> 
	<?php
}
?>
</ul> 
</aside>
