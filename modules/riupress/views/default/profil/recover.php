<?php defined('SYSPATH') OR die('A co Ty robisz?.'); ?>
<section class="row">
<section class="span12"> 
<article>
<h2>Przypomnij hasło</h2>
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb'));
echo Form::open_fieldset();
echo Form::label('email', 'Wpisz email, którego użyłeś(aś) do rejestracji konta:');
echo Form::input('email', FALSE, array('placeholder'=>'Email...','type'=>'email','class'=>'span4'), TRUE);
echo Form::button('save', 'Przypomnij', array('type' => 'submit', 'class' => 'btn btn-success cb'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>

</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
