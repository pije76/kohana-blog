<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
<section class="span8">
<article>
<h2>Profil</h2>
<?php
Flash::msg($errors,$flash);
echo Form::open(NULL, array('class'=>'cb mt8'));
echo Form::open_fieldset();
echo Form::label('name', 'Imię i nazwisko:');
echo Form::input('name', $data['me']['name'], array('placeholder'=>'Imię i nazwisko...','class'=>'span4'));
$genders = array(1 => 'on' , 2 => 'ona');
echo Form::label('gender', 'Płeć:');
echo Form::select('gender', $genders, $data['me']['gender'], array('class'=>'span1'));
echo Form::button('save', 'Zapisz zmiany', array('type' => 'submit', 'class' => 'cb btn btn-success'));
echo Form::close_fieldset();
echo Form::close();
?>
</article>
</section>
<?php
if(!empty($sidebar)){
echo $sidebar;
}
?>
</section>
