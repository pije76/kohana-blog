$(document).ready(function(){
    // ================= klasa usuwająca


    // ================= edycja wiadomości

	$('#wiadomosc textarea').markItUp(mySettings);
	$.datepicker.setDefaults($.datepicker.regional['pl']);
	$('#publish').datepicker({
		duration: '',
		showTime: true,
		constrainInput: false
	});

	function showValues(data,action,id) {
	$('span.msg').each(function()
	{
	$('span.msg').css('display', 'none');
	});

	var but = $('#' + id + ' button span');
		$.post(
		    action,
		    data.serializeArray(),
		    function(data){
		        if (data.success==1){
		        but.text(data.message).fadeIn().delay(3000).fadeOut();
			}
		        else{
				but.text(data.message).fadeIn(function(){
						 but.parent().removeClass('btn-success').addClass('btn-danger');
						}).delay(3000).fadeOut(function(){
						 but.parent().removeClass('btn-danger').addClass('btn-success');
					});
			}
		    },
		    "json"
		);
	    }
	$("#wiadomosc").submit(function(ev){
	ev.preventDefault();
	var action = $("#wiadomosc").attr("action");
	var id = $("#wiadomosc").attr("id");
	var data = $(this);
	showValues(data,action,id);
	});

	$("#komentarz").submit(function(ev){
	ev.preventDefault();
	var action = $("#komentarz").attr("action");
	var id = $("#komentarz").attr("id");
	var data = $(this);
	showValues(data,action,id);
	});


	// ===================== załączniki
	$('#file_upload').fileUploadUI({
		uploadTable: $('#files'),
		downloadTable: $('.files'),
		buildUploadRow: function (files, index) {
		    return $('<tr><td class="filebar">' + files[index].name + '<\/td>' +
		            '<td class="bar"><div class="file_upload_progress"><div><\/div><\/div><\/td>' +
		            '<td class="file_upload_cancel">' +
		            '<button class="ui-state-default ui-corner-all" title="Cancel">' +
		            '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
		            '<\/button><\/td><\/tr>');
		},
		buildDownloadRow: function (file) {
			$('tr.nofiles').css('display', 'none');
		    return $('<tr><td>' +
			'' + file.title + ', ' + file.size + '<\/td>' +
			'<td><a class="btn btn-mini btn-danger" title="Usuń plik" href="' + file.del + '"><i class="icon-remove icon-white"><\/i> Usuń<\/a><\/td>' +
			'<td><a class="btn btn-mini btn-info" title="Pobierz plik" href="' + file.href + '"><i class="icon-download icon-white"><\/i> Pobierz<\/a><\/td>' +
			'<td class="inputfile"><input type="text" value="' + file.download + '" /><\/td>' +
			'<\/tr>');
		},
		onComplete: function (event, files, index, xhr, handler) {
		    handler.onCompleteAll(files);
		},
		onAbort: function (event, files, index, xhr, handler) {
		    handler.removeNode(handler.uploadRow);
		    handler.onCompleteAll(files);
		},
		onCompleteAll: function (files) {
		    // The files array is a shared object between the instances of an upload selection.
		    // We extend it with a uploadCounter to calculate when all uploads have completed:
		    if (!files.uploadCounter) {
		        files.uploadCounter = 1;  
		    } else {
		        files.uploadCounter = files.uploadCounter + 1;
		    }
		    if (files.uploadCounter === files.length) {
		        /* your code after all uplaods have completed */
				alert(file.message);
		    }
		}
	});

	// ================ ikonka

	$("#icons").submit(function(ev){
	ev.preventDefault();
	var action = $("#icons").attr("action");
	var id = $("#icons").attr("id");
	var data = $(this);
	var module = $("#icon").attr("rel");
	var but = $('#' + id + ' button span');
	$('span.msg').each(function()
	{
	$('span.msg').css('display', 'none');
	});
		$.post(
		    action,
		    data.serializeArray(),
		    function(data){
		        if (data.success==1){
				$('ul.newico li').each(function()
				{
				$(this).remove();
				});

				var record_id = data.record_id;
				var page = data.page;

				length = data.arr.length;
				for( i=0; i < length; i++){
				var rel = data.arr[i].rel;


				var li = $("<li></li>");
				var dl = $("<dl></dl>");
				var dt = $("<dt></dt>");
				var dd = $("<dd></dd>");
				//var a = $("<a></a>");

				var href = action;
				$("ul.newico").append(li);
				li.addClass('mb4 even');
				dl.appendTo(li);
				dt.appendTo(dl);
				dt.html('<img src="/api/file/' + data.arr[i].rel + '/medium.jpg" alt="' + data.arr[i].term + '" />')
				dl.append(dd);
				dd.html('<a title="Ustaw: ' + data.arr[i].term + '" class="ustaw ustaw-' + data.arr[i].rel + '" rel="' + data.arr[i].rel + '" href="' + href + '"><i class="icon-repeat"></i></a>');
				$("a.ustaw-" + data.arr[i].rel).click(function(ev) {
				ev.preventDefault();
				var self = $(this);
				var rell = self.attr("rel");
				var hreff = self.attr("href");
	  				seticonrel(hreff,rell,self);

				});
				}

			but.text(data.message).fadeIn().delay(7000).fadeOut();


			}
		        else{
		           but.text(data.message).fadeIn().delay(7000).fadeOut();
			}
		    },
		    "json"
		);
	});

	// usuwanie relacji międy rekordami

	function seticonrel(href,rel,self) {
		$.post(
		    href,
		    { "rel": rel },
		    function(data){
		        if (data.success==1){
				$('ul.oldico li').each(function()
				{
				$(this).remove();
				});

				var icon = data.icon;
				var li = $("<li></li>");
				var dl = $("<dl></dl>");
				var dt = $("<dt></dt>");
				li.addClass('mb4 even');
				dl.appendTo(li);
				dt.appendTo(dl);
				dt.html('<img src="/api/file/' + icon + '/large.jpg" />')
				$(".oldico").append(li);

		        $('#icons button span').text(data.message).fadeIn().delay(6000).fadeOut();
		        }else{
		        return false;}
		    },
		    "json"
		);
	    }


	// ================ tagi

	$("#submitnewtag").submit(function(ev){
	ev.preventDefault();
	var action = $("#submitnewtag").attr("action");
	var id = $("#submitnewtag").attr("id");
	var data = $(this);
	$('span.msg').each(function()
	{
	$('span.msg').css('display', 'none');
	});
	submittags(action,data);
	});
	// rozwijana lista z szukaniem live
	var actiontoken = $("#tokens").attr("rel");
	var posttoken = '/editor/blog/edit/' + actiontoken + '/tokens';
	$('#token').tokenInput(posttoken, {
	hintText: 'Wpisz przynajmniej 3 znaki aby znaleźć TAG',
	noResultsText: 'Niestety nic nie znaleziono',
	});


	// wysyłanie danych po wyszukaniu, zwracania listy tagów

	function submittags(action,data){
		$.post(
		    action,
		    data.serializeArray(),
		    function(data){
		        if (data.success==1){
				$('ul.token-input-list li').each(function()
				{
				$('li.token-input-token').remove();
				});
				$('ul.tagi li').each(function()
				{
				$(this).remove();
				});
				var ul = $("ul.tagi");
				var list = data.arr;
				for(var i = 0, l = list.length; i < l; i++) {
				listi = list[i];
				li = $("<li></li>");
				a = $('<a rel="' + listi.id + '" class="ajaxfastdelete"  href="' + data.href + '">x</a>');

				a.click(function(ev) {
				ev.preventDefault();
				var self = $(this);
				var id = self.attr('rel');
	  				delrelation(data.href,id,self);

				});
				li.append('<a class="listowany" href="/editor/tag/edit/' + list[i].id + '">' + list[i].term + '</a>').append(a);
				ul.append(li);
				}


		        $('#tokens button span').text(data.message).fadeIn().delay(3000).fadeOut();

			}
		        else{
		            $('#tokens button span').text(data.message).fadeIn().delay(3000).fadeOut();}
		    },
		    "json"
		);

	}

    	$("#tokens").submit(function(ev){
	ev.preventDefault();

	var action = $("#tokens").attr("action");
	var id = $("#tokens").attr("id");
	var data = $(this);

	$('span.msg').each(function()
	{
	$('span.msg').css('display', 'none');
	});
	submittags(action,data);

    });


	// rozwijana lista z szukaniem live
	var actionrel = $("#rels").attr("rel");
	var postrel = '/editor/blog/edit/' + actionrel + '/reltokens';
	$('#reltoken').tokenInput(postrel, {
	hintText: 'Wpisz przynajmniej 3 znaki aby szukać...',
	noResultsText: 'Niestety nic nie znaleziono',
	});

    	$("#rels").submit(function(ev){
	ev.preventDefault();

	var action = $("#rels").attr("action");
	var id = $("#rels").attr("id");
	var data = $(this);

	$('p.msg').each(function()
	{
	$('p.msg').css('display', 'none');
	});
	submitrels(action,data);

    });

	// wysyłanie danych po wyszukaniu, zwracania listy tagów

	function submitrels(action,data){
		$.post(
		    action,
		    data.serializeArray(),
		    function(data){
		        if (data.success==1){
				$('ul.token-input-list li').each(function()
				{
				$('li.token-input-token').remove();
				});
				$('ul.powiazane li').each(function()
				{
				$(this).remove();
				});
				var ul = $("ul.powiazane");
				var list = data.arr;
				for(var i = 0, l = list.length; i < l; i++) {
				listi = list[i];
				li = $("<li></li>");
				a = $('<a rel="' + listi.id + '" class="ajaxfastdelete"  href="' + data.href + '">x</a>');

				a.click(function(ev) {
				ev.preventDefault();
				var self = $(this);
				var id = self.attr('rel');
	  				delrelation(data.href,id,self);

				});
				li.append('<a class="listowany" href="/editor/blog/edit/' + list[i].id + '">' + list[i].term + '</a>').append(a);
				ul.append(li);
				}


		        $('#rels button span').text(data.message).fadeIn().delay(3000).fadeOut();

			}
		        else{
		            $('#rels button span').text(data.message).fadeIn().delay(3000).fadeOut();}
		    },
		    "json"
		);

	}

	// usuwanie relacji międy rekordami

	function delrelation(href,rel,self) {
		$.post(
		    href,
		    { "rel": rel },
		    function(data){
		        if (data.success==1)
		        self.parent().remove();
		        else
		        return false;
		    },
		    "json"
		);
	    }

	// usuwanie tagów z listy tagów po załadowaniu edycji rekordu

	$('.fastdelete').click(function(ev) {
	ev.preventDefault();
	var self = $(this);
	var rel = self.attr("rel");
	var href = self.attr("href");
	delrelation(href,rel,self);
	});

	function sendcategory(href,rel,self) {
		$.post(
		    href,
		    { "rel": rel },
		    function(data){
		        if (data.action==1){
		        self.parent().parent().addClass('activetrcat');
			$('.count-' + data.relation).text('(' + data.count+ ')');
			}
		        else{
		        self.parent().parent().removeClass('activetrcat');
			$('.count-' + data.relation).text('(' + data.count+ ')');
			}
		    },
		    "json"
		);
	    }

	// usuwanie tagów z listy tagów po załadowaniu edycji rekordu

	$('.setcat').click(function(ev) {
	ev.preventDefault();
	var self = $(this);
	var rel = self.attr("rel");
	var href = self.attr("href");
	sendcategory(href,rel,self);
	});
});

