$(document).ready(function(){

	$('textarea').markItUp(mySettings);

	function setstatus(href,rel,self) {
		$.post(
		    href,
		    { "rel": rel },
		    function(data){
			$('.label-' + data.id).removeClass('label label-success label-warning label-important').addClass('label ' + data.style).text(data.status);
		    },
		    "json"
		);
	    }

	// usuwanie tagów z listy tagów po załadowaniu edycji rekordu

	$('.setstatus').click(function(ev) {
	ev.preventDefault();
	var self = $(this);
	var rel = self.attr("rel");
	var href = self.attr("href");
	setstatus(href,rel,self);
	});
});

