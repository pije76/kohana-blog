$(document).ready(function(){

	$('textarea').markItUp(mySettings);

	// ===================== załączniki
	$('#file_upload').fileUploadUI({
		uploadTable: $('#files'),
		downloadTable: $('.files'),
		buildUploadRow: function (files, index) {
		    return $('<tr><td class="filebar">' + files[index].name + '<\/td>' +
		            '<td class="bar"><div class="file_upload_progress"><div><\/div><\/div><\/td>' +
		            '<td class="file_upload_cancel">' +
		            '<button class="ui-state-default ui-corner-all" title="Cancel">' +
		            '<span class="ui-icon ui-icon-cancel">Cancel<\/span>' +
		            '<\/button><\/td><\/tr>');
		},
		buildDownloadRow: function (file) {
			$('tr.nofiles').css('display', 'none');
		    return $('<tr><td>' +
			'' + file.title + ', ' + file.size + '<\/td>' +
			'<td><a class="btn btn-mini btn-danger" title="Usuń plik" href="' + file.del + '"><i class="icon-remove icon-white"><\/i> Usuń<\/a><\/td>' +
			'<td><a class="btn btn-mini btn-info" title="pobierz plik" href="' + file.href + '"><i class="icon-download icon-white"><\/i> Pobierz<\/a><\/td>' +
			'<td class="inputfile"><input type="text" value="' + file.download + '" /><\/td>' +
			'<\/tr>');
		},
		onComplete: function (event, files, index, xhr, handler) {
		    handler.onCompleteAll(files);
		},
		onAbort: function (event, files, index, xhr, handler) {
		    handler.removeNode(handler.uploadRow);
		    handler.onCompleteAll(files);
		},
		onCompleteAll: function (files) {
		    // The files array is a shared object between the instances of an upload selection.
		    // We extend it with a uploadCounter to calculate when all uploads have completed:
		    if (!files.uploadCounter) {
		        files.uploadCounter = 1;  
		    } else {
		        files.uploadCounter = files.uploadCounter + 1;
		    }
		    if (files.uploadCounter === files.length) {
		        /* your code after all uplaods have completed */
				alert(file.message);
		    }
		}
	});
});

