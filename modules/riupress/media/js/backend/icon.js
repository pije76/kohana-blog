$(document).ready(function(){

	$('.deleteicon').click(function(ev) {
	ev.preventDefault();
	$('.fast_confirm').each(function(){
	$('.fast_confirm').fadeout();
	});
	var check = $(this);
	var rel = $(this).attr("rel");
	var rev = $(this).attr("rev");
	var href = $(this).attr("href");
		$(this).fastConfirm({
			position: "right",
			questionText: rev,
			proceedText: "Tak, usuń!",
			cancelText: "Nie usuwaj",
			onProceed: function(trigger) {
				$.post(
				    href,
				    { "rel": rel },
				    function(data){
					if (data.success==1){
					$(check).parent().parent().parent().fadeOut(200);
					}
				    },
				    "json"
				);
			},
			onCancel: function(trigger) {
				return false;
			}
		});
	});

	$("#icons").submit(function(ev){
	ev.preventDefault();
	var action = $("#icons").attr("action");
	var id = $("#icons").attr("id");
	var data = $(this);
	var module = $("#icon").attr("rel");
	$('span.msg').each(function()
	{
	$('span.msg').css('display', 'none');
	});
		$.post(
		    action,
		    data.serializeArray(),
		    function(data){
		        if (data.success==1){
				$('ul.newico li').each(function()
				{
				$(this).remove();
				});

				var record_id = data.record_id;
				var page = data.page;

				length = data.arr.length;
				for( i=0; i < length; i++){
				var rel = data.arr[i].rel;


				var li = $("<li></li>");
				var dl = $("<dl></dl>");
				var dt = $("<dt></dt>");
				var dd = $("<dd></dd>");
				//var a = $("<a></a>");

				var href = '/editor/' + module + '/edytuj/' + record_id + '/' + page;
				$("ul.newico").append(li);
				li.addClass('mb4 even');
				dl.appendTo(li);
				dt.appendTo(dl);
				dt.html('<img src="/api/file/' + data.arr[i].rel + '/small.jpg" alt="' + data.arr[i].term + '" />')
				dl.append(dd);
				dd.html('<a title="Ustaw ikonkę" class="ustaw ustaw-' + data.arr[i].rel + '" rel="' + data.arr[i].rel + '" href="' + href + '">ustaw</a>');
				$("a.ustaw-" + data.arr[i].rel).click(function(ev) {
				ev.preventDefault();
				var self = $(this);
				var rell = self.attr("rel");
				var hreff = self.attr("href");
	  				seticonrel(hreff,rell,self);

				});
				}


		        $('#icons span.msg').removeClass('blad').addClass('ok').text(data.message).fadeIn().delay(3000).fadeOut();

			}
		        else{
		            $('#icons span.msg').removeClass('ok').addClass('blad').text(data.message).fadeIn().delay(3000).fadeOut();}
		    },
		    "json"
		);
	});

	// usuwanie relacji międy rekordami

	function seticonrel(href,rel,self) {
		$.post(
		    href,
		    { "rel": rel },
		    function(data){
		        if (data.success==1){
				$('ul.oldico li').each(function()
				{
				$(this).remove();
				});

				var record_id = data.record_id;
				var page = data.page;
				var icon = data.icon;
				var li = $("<li></li>");
				var dl = $("<dl></dl>");
				var dt = $("<dt></dt>");
				li.addClass('mb4 even');
				dl.appendTo(li);
				dt.appendTo(dl);
				dt.html('<img src="/api/file/' + icon + '/small.jpg" />')
				$(".oldico").append(li);

		        $('#icons span.msg').removeClass('blad').addClass('ok').text(data.message).fadeIn().delay(3000).fadeOut();
		        }else{
		        return false;}
		    },
		    "json"
		);
	    }



});

