<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
	<section class="span12">
		<section class="page-header">
		<h1>Rozpoczęczęcie instalacji <small>...lub nowego etapu w życiu!</small></h1>
		</section>

		<section>
      <p>Witaj! Fajnie, że chcesz skorzystać z Riupress. Ten instalator pozwoli Ci szybko w kilku krokach poradzić sobie z instalacją. W razie problemów koniecznie odwiedż stronę wsparcia <a href="">Riupress</a> na której znajdziesz dokumentację, oraz wsparcie ze strony osób korzystających z Riupress.</p>
      <p><a class="btn btn-success btn-large" href="<?php echo url::base().'setup/part1'; ?>"><i class="icon-ok icon-white"></i> Ok, zaczynamy instalację</a></p>
		</section>
	</section>
</section>
