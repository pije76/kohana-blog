<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
	<section class="span12">
		<section class="page-header">
		<h1>Dodanie użytkownika <small> - zostajesz właśnie panem i władcą!</small></h1>
		</section>

		<section>
      <p>Stwórz swoje konto! Jesteś już bardzo blisko zdobycia władzy absolutnej!</p>

	<?php
	
	if(!empty($warning)){
	echo '<section class="alert alert-error"><a class="close" data-dismiss="alert" href="#">×</a>'.$warning.'</section>';
	$name = $_POST['name'];
	$pass = $_POST['pass'];
	$email = $_POST['email'];
	}
	else{
	$name = Session::instance()->get('name');
	$pass = Session::instance()->get('pass');
	$email = Session::instance()->get('email');

	echo '<p>...a więc nazywać się będziesz <strong>'.$name.'</strong>!. Logować się będziesz za pomocą adresu email: <strong>'.$email.'</strong> i hasła: <strong>'.$pass.'</strong>. Oczywiście jeśli chcesz możesz, jeszcze zmienić teraz te dane. Lub później... cokolwiek....</p>';
	}
	echo Form::open(NULL);
	echo Form::open_fieldset();
	echo Form::label('name', 'Nazwa użytkownika:');
	echo Form::input('name', $name, array('placeholder'=>'Dark Vader...','class'=>'span3'));
	echo Form::label('email', 'Email użytkownika:');
	echo Form::input('email', $email, array('placeholder'=>'dark@vader.com','class'=>'span3'));
	echo Form::label('pass', 'Hasło:');
	echo Form::input('pass', $pass, array('placeholder'=>'tajne hasło','class'=>'span3','type'=>'password',));
	echo Form::close_fieldset();
	echo Form::open_fieldset();
	echo Form::button('save', 'Zapisz ustawienia', array('type' => 'submit', 'class' => 'btn btn-success'));
	echo Form::close_fieldset();
	echo Form::close();

	if(empty($warning)){
	?>
      <p>Czy ja dobrze widzę? Czy to banan na twarzy? ... już 80% instalacji za Tobą!</p>
<section class="progress progress-striped progress-success active"><section class="bar" style="width: 80%;"></section></section>
      <p><a class="btn btn-success btn-large" href="<?php echo url::base().'setup/part5'; ?>"><i class="icon-ok icon-white"></i> Ok, mam już wszystko - robię kaboom i instaluje!!!</a></p>
	<?php
	}
	?>
		</section>
	</section>
</section>
