<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
	<section class="span12">
		<section class="page-header">
		<h1>Ustawienia strony <small> - takie tam administracyjne!</small></h1>
		</section>

		<section>
      <p>Poniżej są domyślne dane, dotyczące tytułu Twojej strony, oraz nazwy i emaila zwrotnego dla powiadomień wysyłanych ze strony. Warto byś ustawił tu swoje dane!</p>

	<?php
	if(!empty($warning)){
	echo '<section class="alert alert-error"><a class="close" data-dismiss="alert" href="#">×</a>'.$warning.'</section>';
	}
	echo Form::open(NULL);
	echo Form::open_fieldset();
	echo Form::label('apptitle', 'Tytuł strony:');
	echo Form::input('apptitle', $base['apptitle'], array('placeholder'=>'Kolejna fajna strona...','class'=>'span3'));
	echo Form::label('appadmin', 'Nazwa administratora:');
	echo Form::input('appadmin', $base['admin'], array('placeholder'=>'Jan Kowalski','class'=>'span3'));
	echo Form::label('appmail', 'Email administratora:');
	echo Form::input('appmail', $base['mail'], array('placeholder'=>'email@domena.pl','class'=>'span3'));
	echo Form::close_fieldset();
	echo Form::open_fieldset();
	echo Form::button('save', 'Zapisz ustawienia', array('type' => 'submit', 'class' => 'btn btn-success'));
	echo Form::close_fieldset();
	echo Form::close();

	if(empty($warning)){
	?>
      <p>Szaleństwo - już 60% instalacji za Tobą!</p>
<section class="progress progress-striped progress-success active"><section class="bar" style="width: 60%;"></section></section>
      <p><a class="btn btn-success btn-large" href="<?php echo url::base().'setup/part4'; ?>"><i class="icon-ok icon-white"></i> Ok, a teraz do kroku 4!</a></p>
	<?php
	}
	?>
		</section>
	</section>
</section>
