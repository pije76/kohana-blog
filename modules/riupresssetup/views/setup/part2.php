<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
	<section class="span12">
		<section class="page-header">
		<h1>Połęczenie z bazą <small> - bo baze trzeba czaić!</small></h1>
		</section>

		<section>
      <p>No chłopaku/dziewczynko! Teraz trzeba się połączyć.</p>

	<?php
	if(!empty($warning)){
	echo '<section class="alert alert-error"><a class="close" data-dismiss="alert" href="#">×</a>'.$warning.'</section>';
	echo Form::open(NULL);
	echo Form::open_fieldset();
	echo Form::label('hostname', 'Nazwa hosta:');
	echo Form::input('hostname', $base['default']['connection']['hostname'], array('placeholder'=>'localhost','class'=>'span3'));
	echo Form::label('database', 'Nazwa bazy:');
	echo Form::input('database', $base['default']['connection']['database'], array('placeholder'=>'baza','class'=>'span3'));
	echo Form::label('username', 'Użytkownik bazy:');
	echo Form::input('username', $base['default']['connection']['username'], array('placeholder'=>'user','class'=>'span3'));
	echo Form::label('password', 'Hasło do bazy:');
	echo Form::input('password', $base['default']['connection']['password'], array('placeholder'=>'hasło','class'=>'span3','type'=>'password',));
	echo Form::close_fieldset();
	echo Form::open_fieldset();
	echo Form::button('save', 'Sprawdź i zapisz dane do połączenia', array('type' => 'submit', 'class' => 'btn btn-success'));
	echo Form::close_fieldset();
	echo Form::close();
	}else{
	?>
      <p>Cuda, wianki, bazy, a tymczasem 40% instalacji za Tobą!</p>
<section class="progress progress-striped progress-success active"><section class="bar" style="width: 40%;"></section></section>
      <p><a class="btn btn-success btn-large" href="<?php echo url::base().'setup/part3'; ?>"><i class="icon-ok icon-white"></i> Ok, idziemy do kroku 3!</a></p>
	<?php
	}
	?>
		</section>
	</section>
</section>
