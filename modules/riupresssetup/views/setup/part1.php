<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
	<style type="text/css">
	
	table { border-collapse: collapse; width: 100%; }
		table th,
		table td { padding: 0.4em; text-align: left; vertical-align: top; }
		table th { font-weight: normal; }
		table tr:nth-child(odd) { background: #eee; }
		table td.pass { color: #191; }
		table td.fail { color: #b94a48; }
	#results { padding: 0.8em; color: #fff; font-size: 1.5em; }
	#results.pass { background: #191; }
	#results.fail { background: #b94a48; }

	.alert { font-size: 1.2em;}
	</style>
<section class="row">
	<section class="span12">
		<section class="page-header">
		<h1>Testy ustawień <small>- sprawdzamy czy wszystko jest na swoim miejscu!</small></h1>
		</section>

		<section>
      <p>Poniżej znajudję się tabela z ustawieniami, które wymagane są dla prawidłowego działania Riupress.</p>

	<?php $failed = FALSE ?>
	<table class="table table-striped">
		<tr>
			<th>Wersja PHP</th>
			<?php if (version_compare(PHP_VERSION, '5.3.4', '>=')): ?>
				<td class="pass"><?php echo PHP_VERSION ?></td>
			<?php else: $failed = TRUE ?>
				<td class="fail">Riupress wymaga wersji PHP 5.3.4 albo nowszej, a tutaj jest <?php echo PHP_VERSION ?>.</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Ścieżka do folderu system</th>
			<?php if (is_dir(SYSPATH) AND is_file(SYSPATH.'classes/kohana'.EXT)): ?>
				<td class="pass"><?php echo SYSPATH ?></td>
			<?php else: $failed = TRUE ?>
				<td class="fail">Folder <code>system</code> nie istnieje, lub brakuje jakiś plików (robale na serwerze zjadły).</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Folder aplikacji</th>
			<?php if (is_dir(APPPATH) AND is_file(APPPATH.'bootstrap'.EXT)): ?>
				<td class="pass"><?php echo APPPATH ?></td>
			<?php else: $failed = TRUE ?>
				<td class="fail">Folder <code>application</code> nie istnieje (a matrixa wymaga, żeby jednak był).</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Folder cache</th>
			<?php if (is_dir(APPPATH) AND is_dir(APPPATH.'cache') AND is_writable(APPPATH.'cache')): ?>
				<td class="pass"><?php echo APPPATH.'cache/' ?></td>
			<?php else: $failed = TRUE ?>
				<td class="fail">Folder <code><?php echo APPPATH.'cache/' ?></code> nie ma praw zapisu (chmody albo śmierć).</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Folder logów</th>
			<?php if (is_dir(APPPATH) AND is_dir(APPPATH.'logs') AND is_writable(APPPATH.'logs')): ?>
				<td class="pass"><?php echo APPPATH.'logs/' ?></td>
			<?php else: $failed = TRUE ?>
				<td class="fail">Folder logów <code><?php echo APPPATH.'logs/' ?></code> nie ma praw zapisu (królestwo za uprawnienia).</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>PCRE UTF-8</th>
			<?php if ( ! @preg_match('/^.$/u', 'ñ')): $failed = TRUE ?>
				<td class="fail"><a href="http://php.net/pcre">PCRE</a> nie jest kompatybilny z UTF-8 (cokolwiek to znaczy).</td>
			<?php elseif ( ! @preg_match('/^\pL$/u', 'ñ')): $failed = TRUE ?>
				<td class="fail"><a href="http://php.net/pcre">PCRE</a> nie jest kompatybilny z ustawieniem Unicode (się tak nie patrz tylko popraw).</td>
			<?php else: ?>
				<td class="pass">Prawidłowo</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Dostępność SPL</th>
			<?php if (function_exists('spl_autoload_register')): ?>
				<td class="pass">Prawidłowo</td>
			<?php else: $failed = TRUE ?>
				<td class="fail">PHP <a href="http://www.php.net/spl">SPL</a> nie zostało załadowane lub poprawnie skompilowane.</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Dostępność Reflection</th>
			<?php if (class_exists('ReflectionClass')): ?>
				<td class="pass">Prawidłowo</td>
			<?php else: $failed = TRUE ?>
				<td class="fail">PHP <a href="http://www.php.net/reflection">reflection</a> nie zostało załadowane lub poprawnie skompilowane.</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Dostępność Filters</th>
			<?php if (function_exists('filter_list')): ?>
				<td class="pass">Prawidłowo</td>
			<?php else: $failed = TRUE ?>
				<td class="fail"><a href="http://www.php.net/filter">filter</a> nie zostało załadowane lub poprawnie skompilowane.</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Dostępność Iconv</th>
			<?php if (extension_loaded('iconv')): ?>
				<td class="pass">Prawidłowo</td>
			<?php else: $failed = TRUE ?>
				<td class="fail"><a href="http://php.net/iconv">iconv</a> nie zostało załadowane.</td>
			<?php endif ?>
		</tr>
		<?php if (extension_loaded('mbstring')): ?>
		<tr>
			<th>Niekonfliktowe Mbstring</th>
			<?php if (ini_get('mbstring.func_overload') & MB_OVERLOAD_STRING): $failed = TRUE ?>
				<td class="fail"><a href="http://php.net/mbstring">mbstring</a> nadpisuje natywne funkcje stringów PHP.</td>
			<?php else: ?>
				<td class="pass">Prawidłowo</td>
			<?php endif ?>
		</tr>
		<?php endif ?>
		<tr>
			<th>Character Type (CTYPE)</th>
			<?php if ( ! function_exists('ctype_digit')): $failed = TRUE ?>
				<td class="fail">The <a href="http://php.net/ctype">ctype</a> nie jest dostępne.</td>
			<?php else: ?>
				<td class="pass">Prawidłowo</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>URI</th>
			<?php if (isset($_SERVER['REQUEST_URI']) OR isset($_SERVER['PHP_SELF']) OR isset($_SERVER['PATH_INFO'])): ?>
				<td class="pass">Prawidłowo</td>
			<?php else: $failed = TRUE ?>
				<td class="fail"><code>$_SERVER['REQUEST_URI']</code>, <code>$_SERVER['PHP_SELF']</code>, albo <code>$_SERVER['PATH_INFO']</code> jest dostępne (a nie powinno).</td>
			<?php endif ?>
		</tr>

		<tr>
			<th>Dostępność GD</th>
			<?php if (function_exists('gd_info')): ?>
				<td class="pass">Prawidłowo</td>
			<?php else: $failed = TRUE ?>
				<td class="fail">Riupress wymaga <a href="http://php.net/gd">GD</a> v2 dla klasy Image.</td>
			<?php endif ?>
		</tr>
		<tr>
			<th>Dostępność MySQL</th>
			<?php if (function_exists('mysql_connect')): ?>
				<td class="pass">Prawidłowo</td>
			<?php else: $failed = TRUE ?>
				<td class="fail">Riupress korzysta z <a href="http://php.net/mysql">MySQL</a> dla bazy danych.</td>
			<?php endif ?>
		</tr>
	</table>

	<?php if ($failed === TRUE): ?>
		<section class="alert alert-error">
		  <a class="close" data-dismiss="alert" href="#">×</a>
		  Ajajaj! Jest problem... Potrzebujesz poprawić ustawienia!
		</section>
	<?php else: ?>
      <p>Ha! ...jeszcze na dobre się nie zaczęło, a już 20% instalacji za Tobą!</p>
<section class="progress progress-striped progress-success active"><section class="bar" style="width: 20%;"></section></section>
		<p><a class="btn btn-success btn-large" href="<?php echo url::base().'setup/part2'; ?>"><i class="icon-ok icon-white"></i> Wszytko ok, przejdźmy do kroku 2</a></p>
	<?php endif ?>

		</section>
	</section>
</section>
