<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
	<section class="span12">
		<section class="page-header">
		<h1>Instalacja zakończona <small>- a teraz czas na podbój internetu!</small></h1>
		</section>

		<section>
      <p>Gratuluję - przebrnięcie przez moje poczucie humoru to nie lada wyczyn. Poniższy paseczek widzisz po raz ostatni, ponieważ ma on 100%. Zrób mu "Papa!" i zacznij korzystać ze swojego Riupressa! Najlepiej <a href="<?php echo url::base().'profil/login'; ?>">zaloguj się</a>.</p>

<section class="progress progress-striped progress-success active"><section class="bar" style="width: 100%;"></section></section>
		<p><a class="btn btn-success btn-large" href="<?php echo url::base(); ?>"><i class="icon-ok icon-white"></i> Idę sobie stąd!</a></p>

		</section>
	</section>
</section>
