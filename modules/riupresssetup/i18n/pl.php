<?php defined('SYSPATH') or die('No direct script access.');

return array(

	// tytuł aplikacji 
	'setup.crumb'			=> 'setup',
	'setup.title'			=> 'Instalacja Riupress',

	'setup.part1.crumb'		=> 'part1',
	'setup.part1.title'		=> 'Krok 1: sprawdzenie ustawień serwera',

	'setup.part2.crumb'		=> 'part2',
	'setup.part2.title'		=> 'Krok 2: połęczenie z bazą',

	'setup.part3.crumb'		=> 'part3',
	'setup.part3.title'		=> 'Krok 3: ustawienia strony',

	'setup.part4.crumb'		=> 'part4',
	'setup.part4.title'		=> 'Krok 4: dodanie użytkownika',

	'setup.part5.crumb'		=> 'part5',
	'setup.part5.title'		=> 'Krok 5: instalacja zakończona',

);

