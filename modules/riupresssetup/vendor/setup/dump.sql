CREATE TABLE IF NOT EXISTS {records} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `module` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `module` (`module`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {roles} (
  `id` tinyint(2) unsigned NOT NULL AUTO_INCREMENT,
  `slug` varchar(32) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

INSERT INTO `roles` (`id`, `slug`) VALUES
(1, 'default'),
(2, 'api'),
(3, 'editor'),
(4, 'root'),
(5, 'moderator'),
(6, 'dev');

CREATE TABLE IF NOT EXISTS {categories} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `depth` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `order` smallint(2) unsigned NOT NULL DEFAULT '1',
  `module` tinyint(3) unsigned NOT NULL,
  `term` varchar(64) NOT NULL,
  `slug` varchar(64) NOT NULL,
  `count` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `parent` (`parent`),
  CONSTRAINT `categories_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `categories_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {docs} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `depth` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `order` smallint(2) unsigned NOT NULL DEFAULT '1',
  `term` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `post` text,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `parent` (`parent`),
  CONSTRAINT `docs_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `docs_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {comments} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `parent` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `publish` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  `agent` varchar(250) NOT NULL,
  `nick` varchar(32) NOT NULL,
  `post` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `parent` (`parent`),
  KEY `user` (`user`),
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`parent`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `comments_ibfk_3` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {count_comments} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `is_comment` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `count` smallint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  CONSTRAINT `count_comments_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {count_ratings} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `sum` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `average` float(4,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  CONSTRAINT `count_ratings_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {files} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `file` varchar(255) NOT NULL,
  `mime` varchar(4) NOT NULL,
  `width` mediumint(4) NOT NULL,
  `height` mediumint(4) NOT NULL,
  `size` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `user` (`user`),
  CONSTRAINT `files_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `files_ibfk_2` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {members} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `role` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `role` (`role`),
  CONSTRAINT `members_ibfk_1` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `members_ibfk_2` FOREIGN KEY (`role`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {polls} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL,
  `publish` int(10) unsigned NOT NULL,
  `expiry` int(10) unsigned NOT NULL,
  `term` varchar(255) NOT NULL,
  `totalvotes` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `user` (`user`),
  CONSTRAINT `polls_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `polls_ibfk_2` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {poll_answers} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll` int(10) unsigned NOT NULL,
  `answer` varchar(128) NOT NULL,
  `votes` smallint(5) unsigned NOT NULL,
  `order` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `poll` (`poll`),
  CONSTRAINT `poll_answers_ibfk_1` FOREIGN KEY (`poll`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {poll_votes} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `poll` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `answer` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `poll` (`poll`),
  KEY `user` (`user`),
  KEY `answer` (`answer`),
  CONSTRAINT `poll_votes_ibfk_1` FOREIGN KEY (`poll`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `poll_votes_ibfk_2` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `poll_votes_ibfk_3` FOREIGN KEY (`answer`) REFERENCES `poll_answers` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {posts} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `post` text COLLATE utf8_polish_ci,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {profiles} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `summary_login` int(10) unsigned NOT NULL DEFAULT '0',
  `joined` int(10) unsigned NOT NULL,
  `is_active` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `is_ban` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `is_avatar` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `gender` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `name` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  CONSTRAINT `profiles_ibfk_1` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {publishes} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `user` int(10) unsigned NOT NULL DEFAULT '1',
  `publish` int(10) unsigned NOT NULL,
  `updated` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `status` (`status`),
  KEY `user` (`user`),
  CONSTRAINT `publishes_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {ratings} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `user` int(10) unsigned NOT NULL,
  `rating` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `user` (`user`),
  CONSTRAINT `ratings_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `ratings_ibfk_2` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {relations} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `relation` int(10) unsigned NOT NULL,
  `module` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  KEY `relation` (`relation`),
  KEY `module` (`module`),
  CONSTRAINT `relations_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE,
  CONSTRAINT `relations_ibfk_2` FOREIGN KEY (`relation`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {sessions} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  `timestamp` int(10) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  `session` char(32) NOT NULL DEFAULT '',
  `agent` varchar(250) NOT NULL,
  `requested` varchar(150) NOT NULL,
  `referrer` varchar(250) NOT NULL,
  `host` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {terms} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `term` varchar(128) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
  CONSTRAINT `terms_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {tokens} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `agent` varchar(128) NOT NULL,
  `token` varchar(64) NOT NULL,
  `created` int(10) unsigned NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token` (`token`),
  KEY `record` (`user`),
  CONSTRAINT `tokens_ibfk_1` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {users} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `email` varchar(128) NOT NULL,
  `pass` varchar(64) NOT NULL,
  `active_key` varchar(64) NOT NULL,
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {recovers} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user` int(10) unsigned NOT NULL,
  `key` varchar(64) NOT NULL,
  `create` int(10) unsigned NOT NULL,
  `update` int(10) unsigned NOT NULL,
  `ip` varchar(15) NOT NULL,
  `agent` varchar(250) DEFAULT NULL,
  `host` varchar(100) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`),
  KEY `user` (`user`)
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {tags} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `term` varchar(64) NOT NULL,
  `slug` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
CONSTRAINT `tags_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS {icons} (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `record` int(10) unsigned NOT NULL,
  `term` varchar(64) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `record` (`record`),
CONSTRAINT `icons_ibfk_1` FOREIGN KEY (`record`) REFERENCES `records` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=1 ;
