<?php defined('SYSPATH') or die('No direct script access.');
class Controller_Default_Setup extends Controller_Template_Default {

	public function before()
	{
		if ( ! $this->request->is_ajax() )
		{
			parent::before();
			$this->model = Model::factory('profil');
			$this->title = $this->title.' - '.__('setup.title');
			$this->breadcrumb->add(__('setup.crumb'),__('setup.title'));

		}
	}

	public function action_index()
	{

		$this->template->title = $this->title;
		$this->template->breadcrumb = $this->breadcrumb->render();
		$this->template->content = Theme::factory('setup/index');

	}

	public function action_part1()
	{

		$this->template->title = $this->title.' - '.__('setup.part1.title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil',__('setup.part1.crumb'),__('setup.part1.title'))->render();
		$this->template->content = Theme::factory('setup/part1');

	}

	public function action_part2()
	{

		$this->template->title = $this->title.' - '.__('setup.part2.title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil',__('setup.part2.crumb'),__('setup.part2.title'))->render();

		$loadbase = Kohana::$config->load('database');
		if($_POST){
	
			$post = Validation::factory($_POST)
				->rule('hostname', 'not_empty')
				->rule('database', 'not_empty')
				->rule('username', 'not_empty')
				->rule('password', 'not_empty');

				if ($post->check()){
				$hostname = html::chars($_POST['hostname']);
				$database = html::chars($_POST['database']);
				$username = html::chars($_POST['username']);
				$password = html::chars($_POST['password']);

				$file = fopen('application/config/database.php', 'w+');
	$fileContent = '<?php defined(\'SYSPATH\') or die(\'No direct access allowed.\');

	return array
	(
	\'default\' => array
		(
			\'type\'       => \'mysql\',
			\'connection\' => array(
					\'hostname\'   => \''.$hostname.'\',
					\'database\'   => \''.$database.'\',
					\'username\'   => \''.$username.'\',
					\'password\'   => \''.$password.'\',
					\'persistent\' => FALSE,
					),
			\'table_prefix\' => \'\',
			\'charset\'      => \'utf8\',
			\'caching\'      => FALSE,
			\'profiling\'    => TRUE,
			)
	);
	';
			
				fwrite($file, $fileContent);
				$this->request->redirect('setup/part2');

				}
				else{
				$warning = 'Aktualnie nie ma utworzonego pliku konfiguracyjnego do połączenia z bazą';
				}
		}
		else{
			if(empty($loadbase)){
			$warning = 'Aktualnie nie ma utworzonego pliku konfiguracyjnego do połączenia z bazą';
			}
			else{
				if(empty($loadbase['default'])){
				$warning = 'Nie ma ustawionego domyślnego połączenia z bazą';
				}
				else{
					$connection = @mysql_connect($loadbase['default']['connection']['hostname'], $loadbase['default']['connection']['username'], $loadbase['default']['connection']['password']);
	
					if(!$connection){
					$warning = 'Nie można połączyć się z serwerem <strong>'.$loadbase['default']['connection']['hostname'].'</strong>. Serwer jest niedostępny lub podane nieprawidłowego użytkownika i hasło.';
					}
					else{
						$db_select =  @mysql_select_db($loadbase['default']['connection']['database'], $connection);
						if(!$db_select){
						$warning = 'Nie można połączyć się z bazą <strong>'.$loadbase['default']['connection']['database'].'</strong>';
						}

					}
				}
			}
		}
	
		$this->template->content = Theme::factory('setup/part2')->bind('warning',$warning)->bind('base',$loadbase);

	}


	public function action_part3()
	{

		$this->template->title = $this->title.' - '.__('setup.part3.title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil',__('setup.part3.crumb'),__('setup.part3.title'))->render();
		$warning = false;
		$loadbase = Kohana::$config->load('core');
		if($_POST){
	
			$post = Validation::factory($_POST)
				->rule('apptitle', 'not_empty')
				->rule('appadmin', 'not_empty')
				->rule('appmail', 'not_empty')
				->rule('appmail', 'email');

				if ($post->check()){
				$apptitle = html::chars($_POST['apptitle']);
				$appadmin = html::chars($_POST['appadmin']);
				$appmail = html::chars($_POST['appmail']);

				$file = fopen('application/config/core.php', 'w+');
	$fileContent = '<?php defined(\'SYSPATH\') or die(\'No direct access allowed.\');

	return array
	(
		\'apptitle\' => \''.$apptitle.'\',
		\'expires\' => 3600,
		\'gmt\' => 3600,
		\'lifetime\' => 1209600,
		\'active\' => 1800,
		\'session_key\' => \'app_user\',
		\'cookie_key\' => \'app_auto_login\',
		\'admin\' => \''.$appadmin.'\',
		\'mail\' => \''.$appmail.'\',
		\'theme\' => \'default\',
	);
	';
			
				fwrite($file, $fileContent);
				$this->request->redirect('setup/part3');

				}
				else{
				$warning = 'Musisz prawidłowo podać tytuł strony, nazwę i email zwrotny dla poczty.';
				}
		}
	
		$this->template->content = Theme::factory('setup/part3')->bind('warning',$warning)->bind('base',$loadbase);

		}

		public function action_part4()
		{

		$this->template->title = $this->title.' - '.__('setup.part4.title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil',__('setup.part4.crumb'),__('setup.part4.title'))->render();

		if($_POST){
	
			$post = Validation::factory($_POST)
				->rule('name', 'not_empty')
				->rule('name', 'min_length', array(':value', '3'))
				->rule('name', 'max_length', array(':value', '32'))
				->rule('pass', 'not_empty')
				->rule('pass', 'min_length', array(':value', '6'))
				->rule('pass', 'max_length', array(':value', '32'))
				->rule('email', 'not_empty')
				->rule('email', 'email');

				if ($post->check()){
				$name = html::chars($_POST['name']);
				$pass = html::chars($_POST['pass']);
				$email = html::chars($_POST['email']);

				Session::instance()->set('name',$name);
				Session::instance()->set('pass',$pass);
				Session::instance()->set('email',$email);

				$this->request->redirect('setup/part4');

				}
				else{
				$warning = 'Musisz prawidłowo podać nazwę użytkownika, hasło i email. Zapamiętaj hasło.';
				}
		}
		$check = Session::instance()->get('email');
		if(empty($check) AND empty($warning)){
		$warning = 'Podaj nazwę użytkownika, hasło i email.';
		}
		$this->template->content = Theme::factory('setup/part4')->bind('warning',$warning);

	}

	public function action_part5()
	{

		$loadbase = Kohana::$config->load('database');
		$this->unpack_sql($loadbase['default']['connection']);
		$this->model->add_fullprofile('default','','',1,1,array());
		$name = Session::instance()->get('name');
		$pass = Session::instance()->get('pass');
		$email = Session::instance()->get('email');
		$this->model->add_fullprofile($name,$email,$pass,1,1,array('3','4'));
		Session::instance()->destroy();
		$this->template->title = $this->title.' - '.__('setup.part5.title');
		$this->template->breadcrumb = $this->breadcrumb->add('profil',__('setup.part5.crumb'),__('setup.part5.title'))->render();
		$this->template->content = Theme::factory('setup/part5')->bind('data',$g);

	}

	private function unpack_sql($config) {
		$prefix = $config["table_prefix"];
		$buf = null;

		mysql_connect($config["hostname"], $config["username"], $config["password"]);
		mysql_select_db($config["database"]);

		foreach (file(MODPATH . "riupresssetup/vendor/setup/dump.sql") as $line) {
			$buf .= trim($line);
			if (preg_match("/;$/", $buf))
			{
				if (!mysql_query($this->prepend_prefix($prefix, $buf)))
				{
					throw new Exception(mysql_error());
				}
				$buf = "";
			}
		}
		return true;
	}

	private function prepend_prefix($prefix, $sql) {
		return  preg_replace("#{([a-zA-Z0-9_]+)}#", "{$prefix}$1", $sql);
	}
}
