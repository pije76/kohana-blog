<?php defined('SYSPATH') or die('No direct script access.');

require SYSPATH.'classes/kohana/core'.EXT;
require SYSPATH.'classes/kohana'.EXT;

date_default_timezone_set('Europe/Warsaw');
setlocale(LC_ALL, 'pl_PL.utf-8');

spl_autoload_register(array('Kohana', 'auto_load'));
ini_set('unserialize_callback_func', 'spl_autoload_call');

I18n::lang('pl-pl');

Kohana::init(array(
'base_url'   => 'http://riupress/',
'index_file' => FALSE,
'errors' => FALSE,
'profile' => FALSE,
'caching' => FALSE,
));
//Kohana::$log->attach(new Log_File(APPPATH.'logs'));
Kohana::$config->attach(new Config_File);
Kohana::modules(array(
	'cache' => MODPATH.'cache',
	'database' => MODPATH.'database',
	'pagination' => MODPATH.'pagination',
	'image' => MODPATH.'image',
	'riudb' => MODPATH.'riudb',
	'theme' => MODPATH.'theme',
	'breadcrumb' => MODPATH.'breadcrumb',
	'sender' => MODPATH.'sender',
	'riupress' => MODPATH.'riupress',
	'riupresssetup' => MODPATH.'riupresssetup',
));

Route::set('default', '(<controller>(/<action>(/<id>)))')
	->defaults(array('directory' => 'default', 'controller' => 'index', 'action' => 'index'));

Cookie::$salt = '!82^f%7&';
Cookie::$expiration = Kohana::$config->load('core')->lifetime;
