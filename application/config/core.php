<?php defined('SYSPATH') or die('No direct access allowed.');

	return array
	(
		'apptitle' => 'Riupress',
		'expires' => 3600,
		'gmt' => 3600,
		'lifetime' => 1209600,
		'active' => 1800,
		'session_key' => 'app_user',
		'cookie_key' => 'app_auto_login',
		'admin' => 'Admin',
		'mail' => 'admin@domena.pl',
		'theme' => 'default',
	);
	