<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(
	'default' => array(
		'dirid' => '1',
		'dirtitle' => __('role_directory_default'),
		'dirsubtitle' => __('role_name_default'),
		),
	'api' => array(
		'dirid' => '2',
		'dirtitle' => __('role_directory_api'),
		'dirsubtitle' => __('role_name_api'),
		),
	'editor' => array(
		'dirid' => '3',
		'dirtitle' => __('role_directory_editor'),
		'dirsubtitle' => __('role_name_editor'),
		),
	'root' => array(
		'dirid' => '4',
		'dirtitle' => __('role_directory_root'),
		'dirsubtitle' => __('role_name_root'),
		),
	'moderator' => array(
		'dirid' => '5',
		'dirtitle' => __('role_directory_moderator'),
		'dirsubtitle' => __('role_name_moderator'),
		),
	'dev' => array(
		'dirid' => '6',
		'dirtitle' => __('role_directory_dev'),
		'dirsubtitle' => __('role_name_dev'),
		),

);

