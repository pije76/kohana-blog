$(document).ready(function(){
	$('#commentform textarea').markItUp(mySettings);
	$('pre').addClass('prettyprint linenums');

	$('.ratme').click(function(ev) {
	ev.preventDefault();
	$('ul.rating li.nocurrent a').each(function()
	{
		$(this).remove();
	});
	var self = $(this);
	var rating = self.attr("id");
	var href = self.attr("href");
	rat(href,rating,self);
	});

	function rat(href,rating,self) {
		$.post(
		    href,
		    { "id": rating },
		    function(data){
		        if (data.success==1){
				var width = data.width;
				var sum = data.sum;
				var count = data.count;
				var msg = data.msg;
				var idrecord = data.idrecord;
				$('ul.rating li.nocurrent').each(function()
				{
				$(this).remove();
				});

				$('ul.rating li.current').replaceWith('<li class="current" style="width: ' + width + 'em;">Średnio głosów: ' + width + '</li>');
				$('#prating').replaceWith('<span class="pull-right">Średnio oceniony na ' + width + ', wszytkich głosów: ' + count + '</span>');
		        }else{
		        return false;}
		    },
		    "json"
		);
	    }
});
!function ($) {

  $(function(){
    // make code pretty
    window.prettyPrint && prettyPrint()
  })
}(window.jQuery)
