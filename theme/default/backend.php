<?php defined('SYSPATH') OR die('No direct access allowed.');
return array(
	'styles' => array(
		'css/bootstrap.css'=>'screen',
		'css/default.css'=>'screen',
		'js/google-code-prettify/prettify.css'=>'screen',
		'js/markitup/sets/riupress/style.css'=>'screen',
	),
	'scripts' => array(
		'js/html5.js',
		'js/jquery-1.7.2.min.js',
		'js/google-code-prettify/prettify.js',
		'js/bootstrap.min.js',
		'js/markitup/jquery.markitup.js',
		'js/markitup/sets/riupress/set.js',
		'js/application.js'
	)
);
