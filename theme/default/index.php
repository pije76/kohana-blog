<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
	<section class="span12">
		<section class="hero-unit">
			<h1>Riupress</h1>
			<p>Jest szybką, lekką, osobistą platformą publikacyjną, koncentrującą się na estetyce, zgodności z internetowymi standardami i użyteczności. Riupress powstaje w oparciu o Framework Kohana.</p>
			<p><a class="btn btn-success btn-large" href="https://github.com/Riu/riupress/zipball/master"><i class="icon-download icon-white"></i> Pobierz Riupress</a> <a class="btn btn-large" href="https://github.com/Riu/riupress"><i class="icon-eye-open"></i> Obserwuj na Github'ie</a></p>
		</section>
	</section>
</section>
<section class="row">
	<section class="span3">
		<h3><i class="icon-thumbs-up mt4"></i> Praktyczny</h3>
		<p>Tagi, kategorie, komentarze, rss, system skórek, optymalizacja dla SEO, ikony, załączniki, oceny i wiele innych.</p>
	</section>
	<section class="span3">
		<h3><i class="icon-eye-open mt4"></i> Nowoczesny</h3>
		<p>Oparty o najlepsze i znane rozwiązania takie jak Kohana Framework, Twitter Boostrap, jQuery i HTML5.</p>
	</section>
	<section class="span3">
		<h3><i class="icon-fire mt4"></i> Szybki i wydajny</h3>
		<p>Zapewnia kilkukrotnie mniejsze obciązenie serwera i baz danych niż Wordpress, czy inne podobne rozwiązania.</p>
	</section>
	<section class="span3">
		<h3><i class="icon-flag mt4"></i> Licencja</h3>
		<p>Riupress jest licencjonowany na zasadach licencji BSD co zapewnia swobodę wykorzystania w dowolnym celu.</p>
	</section>
</section><!--/row-->
