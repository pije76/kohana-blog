<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<section class="row">
	<section class="span12">
			<h1>Riupress</h1>
			<p>Jest szybką, lekką, osobistą platformą publikacyjną, koncentrującą się na estetyce, zgodności z internetowymi standardami i użyteczności. Riupress powstaje w oparciu o Framework Kohana.</p>

			<h3>Wstęp</h3>


			<p>Riupress powstaje jako aplikacja do prowadzenia bloga o programowniu z perspektywą jej rozwoju o dodatkowe funkcjonalności. Bezpośrednią inspiracją było brak system zbliżonego funkcjonalnością do Wordpressa, ale opartego o rozsądne i wydajne rozwiązania programistyczne. </p>

			<h3>Planowane funkcjonalności</h3>
## 

<p>W wersji podstawowej blog zawierać będzie:</p>
<ul>
<li>-silnik aplikacji oparty o Kohana 3.2, wymagane PHP 5.3;</li>
<li>-korzystanie z bazy danych MySQL i mechanizmu RiuDB;</li>
<li>-jQuery jako domyślna biblioteka wbudowana w system;</li>
<li>-prezentacja danych za pomocą HTML5 i Twitter Bootstrap z modułem kolorowania składni;</li>
<li>-edytor postów wykorzystujący Markdown ;</li>
<li>-wykorzystanie ikon The Noun Project;</li>
<li>-kategoryzacja i tagi;</li>
<li>-relacje międzu wpisami;</li>
<li>-załączanie plików do postów;</li>
<li>-kanały rss;</li>
<li>-system skórek;</li>
<li>-komentarze;</li>
<li>-system ocen postów;</li>
<li>-ankiety;</li>
<li>-tworzenie sitemapy;</li>
<li>-optymalizacja pod SEO;</li>
<li>-rejestracja i zarządzenie kontem;</li>
<li>-obsługa wielu autorów;</li>
<li>-avatary.</li>
</ul>

<p>Planowane:</p>
<ul>
<li>-instalator;</li>
<li>-moduł do migracji z Wordpress;</li>
<li>-kopie zapasowe;</li>
<li>-logowanie za pomocą oAuth2.</li>
</ul>
			<h3>Współpraca przy tworzeniu</h3>

<p>
Zapraszam też do dyskusji tutaj: http://forum.kohanaphp.pl/index.php/topic,2526.0.html
</p>
			<h3>Licencja</h3>

<p>
http://www.gnu.org/licenses/gpl-3.0.txt
</p>
			<h3>Kontakt z autorem</h3>

<ul>
<li>-radoslawmuszynski na gmail.com</li>
<li>-https://plus.google.com/116761443278536468195</li>
</ul>
	</section>
</section>
