<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<title><?php echo $title; ?></title>
<?php foreach ($metas as $value) echo '<meta'.Html::attributes($value).' />',"\n" ?>
<?php foreach ($links as $value) echo '<link'.Html::attributes($value).' />',"\n" ?>
<?php foreach ($styles as $file => $type) echo HTML::style($file, array('media' => $type)), "\n" ?>
<?php foreach ($scripts as $file) echo HTML::script($file), "\n" ?>
<?php foreach ($codes as $value) echo '<script'.Html::attributes(array('type' => 'text/javascript')).'>'."\n".$value."\n".'</script>'; ?>
</head>
<body>
<?php
echo $header;
if(!empty($breadcrumb)){
echo $breadcrumb;
}
if(!empty($leftbar)){
echo $leftbar;
}
echo $content;
if(!empty($sidebar)){
echo $sidebar;
}
echo $footer;
?>
</body>
</html>
