<?php defined('SYSPATH') OR die('No direct access allowed.'); ?>
<footer class="row">
	<section class="span12  footer">
		<section class="row">
			<section class="span12">
		      			<p>&copy; 2012 <a href="" title="Riupress">Riupress</a>. Wszystkie prawa zastrzeżone. Stronę dumnie napędza <a href="" title="Riupress">Riupress</a>. Strona zgodna z <a href="" title="HTML5">HTML5</a>. Strona wyrenderowana w czasie <?php echo number_format((microtime(TRUE)-KOHANA_START_TIME), 2) ?> <abbr title="sekundy">s</abbr>, zużywając <?php echo number_format(memory_get_usage() / 1048576, 2) ?> <abbr title="megabajty">mb</abbr> pamięci.</p>
			</section>
		</section>
	</section>
</footer>
<?php
//echo View::factory('profiler/stats');
echo Debug::vars($data);
//echo Request::$user_agent;
?>

</section>
