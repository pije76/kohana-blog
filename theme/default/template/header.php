<?php defined('SYSPATH') OR die('No direct access allowed.'); 
$dirmenu = array();
if(!empty($data['me']['members'])){
	foreach($roles as $r => $k){
		if(in_array($k['dirid'],$data['me']['members'])){
			$dirmenu[] = array('slug'=>$r, 'title'=>$k['dirtitle'], 'class'=>'icon-cog');
		}
	}
}


$d = Request::current()->directory();
if($d==='default'){
	$preslug = '';
}
else{
	$preslug = $d.DIRECTORY_SEPARATOR;
}
$c = Request::current()->controller();
?>

<section class="navbar navbar-fixed-top">
	<section class="navbar-inner">
 		<section class="container">
 		<a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		<span class="icon-bar"></span>
		</a>
		<?php echo html::anchor(url::site(''), 'Riu<span>press</span> <span class="subtitle">'.$subtitle.'</span>', array('title'=>__('app_title'), 'class'=>"brand")); ?>
		<section class="nav-collapse">
			<ul class="nav">
			<?php
			foreach($menu as $m){
				if($m['slug']===$c){
				$class = ' class="active"';
				}
				else{
				$class = '';
				}
			echo '<li'.$class.'>'.html::anchor(url::site($preslug.$m['slug']), '<i class="'.$m['class'].'"></i> '.$m['title'], array('title'=>$m['title'])).'</li>';
			}
			if(!empty($dirmenu)){
			echo '<li class="dropdown">
			<a class="dropdown-toggle" data-toggle="dropdown" href="#">Przełącz <b class="caret"></b></a>
			<ul class="dropdown-menu pull-right">';
			foreach($dirmenu as $m){
				if($m['slug']===$d AND $c==='index'){
				$class = ' class="active"';
				}
				else{
				$class = '';
				}
			echo '<li'.$class.'>'.html::anchor(url::site($m['slug']), '<i class="'.$m['class'].'"></i> '.$m['title'], array('title'=>$m['title'])).'</li>';
			}
			echo '</ul>
			</li>';
			}
			if(!empty($data['me']) AND $data['me']['id']>1){
			echo '<li'.$class.'>'.html::anchor(url::site('profil/logout'), '<i class="icon-off"></i> Wyloguj', array('title'=>'Wyloguj')).'</li>';
			}
			?>
			</ul>
		</section><!--/.nav-collapse -->
		</section>
	</section>
</section>
<section class="container">

