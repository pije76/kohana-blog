riupress
========

System bloga działający w oparciu o Kohana Framework 3.2

## Wstęp

Riupress powstaje jako aplikacja do prowadzenia bloga o programowniu z perspektywą jej rozwoju o dodatkowe funkcjonalności. Bezpośrednią inspiracją było brak system zbliżonego funkcjonalnością do Wordpressa, ale opartego o rozsądne i wydajne rozwiązania programistyczne. 

## Planowane funkcjonalności

W wersji podstawowej blog zawierać będzie:

- silnik aplikacji oparty o Kohana 3.2, wymagane PHP 5.3;
- korzystanie z bazy danych MySQL i mechanizmu RiuDB;
- jQuery jako domyślna biblioteka wbudowana w system;
- prezentacja danych za pomocą HTML5 i Twitter Bootstrap z modułem kolorowania składni;
- edytor wykorzystujący Markdown ;
- kategoryzacja i tagi;
- relacje międzu wpisami;
- załączanie plików do postów;
- kanały rss;
- system skórek;
- komentarze;
- system ocen postów;
- ankiety;
- rejestracja i zarządzenie kontem;
- obsługa wielu autorów;
- instalator;
- moduł do tworzenia dokumentacji;
- avatary.

Planowane:

- tworzenie sitemapy;
- optymalizacja pod SEO;
- moduł do migracji z Wordpress;
- kopie zapasowe;
- logowanie za pomocą oAuth2.

W ramach Riupressa zostaną stworzone również moduły:

- forum dyskusyjnego;
- portfolio;
- stron statycznych;
- bugtracka.

## Współpraca przy tworzeniu

Zapraszam też do dyskusji tutaj: http://forum.kohanaphp.pl/index.php/topic,2526.0.html

## Licencja

http://www.gnu.org/licenses/gpl-3.0.txt

## Kontakt z autorem

- radoslawmuszynski na gmail.com
- https://plus.google.com/116761443278536468195

